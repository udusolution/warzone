<div class="forti-popup">
    <div class="forti-popup__inner">
        <div class="forti-popup__overlay" onclick="closeFortiPopup()"></div>
        <div class="forti-popup__container">

            <!-- Close button -->
            <div class="forti-popup__top">
                <i class="fa fa-times" onclick="closeFortiPopup()"></i>
            </div>

            <div class="row align-items-center">
    
                <!-- Left -->
                <div class="col-xl-8 col-lg-12">
                    <div class="released-game-active">
                        <div class="released-game-carousel">
                            <div class="released-game-item">
    
                                <!-- BG -->
                                <div class="released-game-item-bg"></div>
    
                                <!-- Cover -->
                                <div class="released-game-img">
                                    <img src="img/images/released_game_img01.jpg" alt="" id="fortiCover">
                                </div>
    
                                <!-- Info -->
                                <div class="released-game-content">
                                    <div class="released-game-rating">
                                        <h5>Rarity :<span id="fortiRarity"></span></h5>
                                    </div>
                                    <h4 id="fortiName">Call <span>of Duty</span></h4>
                                    <div class="released-game-list mb-15">
                                        <ul>
                                            <li id="fortiCategory"><span>Category :</span>Virtual Game</li>
                                            <li id="fortiSpecialty"><span>Model :</span>Compete 100 players</li>
                                            <li id="fortiRanking"><span>Controller :</span>Playstation 5 , Xbox , PS4</li>
                                        </ul>
                                    </div>
                                    <p id="fortiDescription">Commodo velit consectetur magna nostrud.</p>
                                    <a href="#" target="_blank" class="btn btn-style-two" id="fortiMarketplace">buy now</a>
                                </div>
    
                            </div>
                        </div>
                    </div>
                </div>
    
                <!-- Right -->
                <div class="col-xl-4 col-lg-12" style="position: unset;">
    
                    <!-- Gallery -->
                    <div class="forti-gallery-nav" id="fortiGallery">
                        <div class="released-game-nav-item">
                            <img src="img/images/release_game_nav01.jpg" alt="">
                        </div>
                        <div class="released-game-nav-item">
                            <img src="img/images/release_game_nav02.jpg" alt="">
                        </div>
                        <div class="released-game-nav-item">
                            <img src="img/images/release_game_nav03.jpg" alt="">
                        </div>
                    </div>
    
                </div>
            </div>

            <!-- Bottom -->
            <div class="forti-popup__bottom">
                <button id="fortiNext">Next <i class="fas fa-arrow-circle-right"></i></button>
            </div>

        </div>
    </div>
</div>


<script>

    // Forti Popup
    const fortiPopup = document.querySelector('.forti-popup');
    const fortiNext = document.querySelector('#fortiNext');

    // Forti Info DOM Elements
    const fortiCover = document.querySelector('#fortiCover');
    const fortiRarity = document.querySelector('#fortiRarity');
    const fortiName = document.querySelector('#fortiName');
    const fortiCategory = document.querySelector('#fortiCategory');
    const fortiSpecialty = document.querySelector('#fortiSpecialty');
    const fortiRanking = document.querySelector('#fortiRanking');
    const fortiDescription = document.querySelector('#fortiDescription');
    const fortiMarketplace = document.querySelector('#fortiMarketplace');
    const fortiGallery = document.querySelector('#fortiGallery');


    function openFortiPopup(id) {

        if(wasDrag) return;

        // Get Forti
        const forti = FORTIS.find(f => f.id == id);

        // ! If no forti found, return
        if(!forti) return;

        // Fill the info
        fortiCover.src                  = forti.cover;
        fortiRarity.innerHTML           = forti.rarity;
        fortiName.innerHTML             = forti.name;
        fortiCategory.innerHTML         = `<span>Category:</span> ${forti.category}`;
        fortiSpecialty.innerHTML        = `<span>Specialty:</span> ${forti.specialty}`;
        fortiRanking.innerHTML          = `<span>Ranking:&nbsp;&nbsp;</span> ${forti.ranking}`;
        fortiDescription.innerHTML      = forti.descriptionLong;
        fortiMarketplace.href           = forti.store;

        // Reset the gallery
        emptyGallery();

        // Fill the gallery
        for(let i = 0; i < forti.gallery.length; i++) {
            let img = forti.gallery[i];
            let htmlSlide = `
                <div class="released-game-nav-item">
                    <img src="${img}" alt="">
                </div>
            `;
            addSlide(htmlSlide);
        }

        // Next Button
        const nextId = id == FORTIS.length ? 1 : id + 1;
        fortiNext.onclick = () => openFortiPopup(nextId);

        // Show the Popup
        showFortiPopup();
    }

    function showFortiPopup() {
        fortiPopup.classList.add('forti-popup--open');
    }
    function closeFortiPopup() {
        fortiPopup.classList.remove('forti-popup--open');
    }

    function addSlide(htmlSlide) {
        console.log('Add Slide:', htmlSlide);
        $('#fortiGallery').slick('slickAdd', htmlSlide);
    }

    function emptyGallery() {
        if(document.querySelector('#fortiGallery.slick-initialized')) {
            console.log('EMPTY THE GALLERY');
            $('#fortiGallery').slick('slickRemove', null, null, true);
        }
    }

    // Init the Gallery
    console.log('INIT SLICK');
    $('#fortiGallery').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        centerMode: false,
        centerPadding: '0px',
        vertical: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    vertical: false,
                    dots: false
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    vertical: false,
                    dots: false
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false,
                    vertical: false,
                    dots: false
                }
            },
            {
                breakpoint: 575,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    vertical: false,
                    dots: false
                }
            },
        ]
    });

</script>