<!doctype html>
<html class="no-js" lang="">
    <head>
        
        <!-- HEAD -->
        <?php include '_head.php'; ?>

        <!-- ReCAPTCHA -->
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

        <style>
            #submitBtn:disabled {
                cursor: not-allowed;
            }
            .form-img {
                width: 100%;
                height: auto;
            }
        </style>

    </head>
    <body>

        <!-- Preloader -->
        <?php include '_preloader.php'; ?>

        <!-- HEADER -->
        <?php include '_header.php'; ?>

        <!-- main-area -->
        <main>

            <!-- breadcrumb-area -->
            <section class="breadcrumb-area breadcrumb-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb-content text-center">
                                <h2>private <span>sale</span></h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.php">Warzone</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">private sale</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- breadcrumb-area-end -->

            <!-- contact-area -->
            <section class="contact-area pt-120 pb-120">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">

                            <img src="img/bg/eth.png" class="form-img" />

                        </div>
                        <div class="col-lg-6 pl-45">
                            <div class="section-title title-style-three mb-20">
                                <h2>PRIVATE <span>SALE</span></h2>
                                <p>Bridge to Ethereum</p>
                            </div>
                            <div class="contact-form">
                                <form id="ethForm">
                                    <input type="text" name="wallet" placeholder="* Your Wallet Address" required />
                                    <input type="number" name="amount" placeholder="* Eth Amount" required />
                                    <input type="text" name="telegram" placeholder="Your Telegram Handle (@JohnDoe)" />
                                    <div class="g-recaptcha" data-sitekey="6LcIxXweAAAAALNeburC_5gaKfY6Z1HgkUD8io7v"></div>
                                    <button type="submit" class="clickable" id="submitBtn">SUBMIT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- contact-area-end -->



        </main>
        <!-- main-area-end -->

        <!-- FOOTER -->
        <?php include '_footer.php'; ?>

		<!-- SCRIPTS -->
        <?php include '_scripts.php'; ?>

        <script>
            /**
             * Eth Form
             */

            const ethForm = document.querySelector('#ethForm');
            const wallet = ethForm.querySelector('input[name="wallet"]');
            const amount = ethForm.querySelector('input[name="amount"]');
            const telegram = ethForm.querySelector('input[name="telegram"]');
            const submitBtn = ethForm.querySelector('#submitBtn');

            ethForm.addEventListener('submit', function(ev) {

                // Prevent Default
                ev.preventDefault();

                // Data
                const data = {
                    wallet: wallet.value,
                    amount: amount.value,
                    telegram: telegram.value,
                    recaptcha: grecaptcha.getResponse()
                }

                // Validate
                console.log(data);
                if(grecaptcha.getResponse() == "") {
                    submitBtn.innerHTML = 'Invalid Captcha';
                    return;
                }

                // Disable Send Button
                submitBtn.disabled = 'disabled';
                submitBtn.innerHTML = 'Sending...';

                // Send
                $.ajax({
                    url: 'sendEthEmail.php',
                    async: true,
                    cache: false,
                    type: 'POST',
                    dataType: "json",
                    data: {
                        data: JSON.stringify(data)
                    },
                    success: function(response) {
                        if(response.status == 'success') {
                            console.log(response);
                            resetEthForm();
                            submitBtn.innerHTML = 'Thank you!';
                        }
                        else if(response.status == 'error') {
                            console.error(response);
                            submitBtn.disabled = false;
                            submitBtn.innerHTML = 'Something went wrong!';
                        }
                        else {
                            console.info(response);
                            submitBtn.disabled = false;
                        }
                    },
                    error: function(err) {
                        console.error(err);
                        submitBtn.disabled = false;
                        submitBtn.innerHTML = 'Something went wrong!!';
                    }
                })

                function resetEthForm() {
                    wallet.value = '';
                    amount.value = '';
                    telegram.value = '';
                }

            })

        </script>

    </body>
</html>
