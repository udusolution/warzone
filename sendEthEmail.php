<?php

    require_once './vendor/autoload.php';
    require './env.php';
    use Mailgun\Mailgun;

    $data = json_decode($_POST['data']);

    // ReCaptcha
    if(isset($data->recaptcha) && $data->recaptcha != "") {
        $secret = "6LcIxXweAAAAANKgIUuboSuMS0tsN6OXHJjHLjFr";
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $data->recaptcha);
        $responseData = json_decode($verifyResponse);
        
        // If recaptcha verified
        if($responseData->success) {

            try {

                
                $message = 'Your message has been sent!';
                $emailSubject = 'BFK Warzone - New Private Sale Submission';
                $emailText = 'You have received a message from the Private Sale form. Wallet Address: "'.$data->wallet.'". Eth Amount: "'.$data->amount.'". Telegram Handle: "'.$data->telegram.'".';

                // EMAIL
                # Instantiate the client.
                $mg = Mailgun::create($MAILGUN_KEY);
                $domain = $MAILGUN_DOMAIN;

                # Make the call to the client.
                $mg->messages()->send($domain, [
                    'from'    => 'BFK Warzone <no-reply@'.$domain.'>',
                    'to'      => $RECIPIENT_EMAIL,
                    'subject' => $emailSubject,
                    'text'    => $emailText
                ]);

            } catch (Error $e) {
                echo json_encode(['status' => 'error', 'message' => $e->getMessage()]);
            }

            try {
                //////////////////// GOOGLE SHEETS ////////////////////////
                $client = new Google_Client();
                $client->setApplicationName('warzone');
                $client->setScopes([Google_Service_Sheets::SPREADSHEETS]);
                $client->setAccessType('offline');
                $client->setAuthConfig(__DIR__ . '/sheet-creds.json');
                $service = new Google_Service_Sheets($client);
                $spreadsheetId = "1ezna5T0c10VE9YLvJhuL2Xei__zRpJsWTtNF6rwV0YM";
                $sheetName = 'BFK WARZONE PRIVATE SALE FORM';
                $body = [
                    $data->wallet,
                    $data->amount,
                    $data->telegram
                ];
                $valueRange = new Google_Service_Sheets_ValueRange();
                $valueRange->setValues([
                    "values" => $body
                ]);
                $params = ["valueInputOption" => "RAW", "insertDataOption" => "INSERT_ROWS"];
                $service->spreadsheets_values->append($spreadsheetId, $sheetName, $valueRange, $params);
                ////////////////////////////////////////////////////////////

            } catch (Google_Exception $e) {
                echo json_encode(['status' => 'error', 'message' => $e->getMessage()]);
            }

            http_response_code(200);
            echo json_encode([
                'status' => 'success',
                'message' => 'Success'
            ]);
        }
    }
?>