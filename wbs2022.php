<?php include 'media/wbs2022.php'; ?>

<!doctype html>
<html class="no-js" lang="">
    <head>

        <!-- Head -->
        <?php include '_head.php'; ?>

    </head>
    <body>

        <!-- Preloader -->
        <?php include '_preloader.php'; ?>

        <!-- Header -->
        <?php include '_header.php'; ?>

        <!-- main-area -->
        <main>

            <!-- Announcements -->
            <?php include '_announcements.php'; ?>

            <!-- breadcrumb-area -->
            <section class="breadcrumb-area breadcrumb-bg team-breadcrumbs">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb-content text-center">
                                <h2>World Blockchain Summit 2022</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                        <li class="breadcrumb-item"><a href="media.php">Media</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">World Blockchain Summit 2022</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- breadcrumb-area-end -->

            <!-- inner-about-area -->
            <section class="game-single-area pt-60 pb-120">
                <div class="container">
                    <div class="pagination-wrap pb-20">
                        <ul>
                            <li><a href="welups2022.php">Back</a></li>
                            <li><a href="expo2022.php">Next</a></li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="game-single-content">
                                <div class="game-single-gallery">
                                    <div class="row">
                                        <div class="col-lg-4 col-sm-6 media-img" style="position: relative;">
                                            <img data-lazy="img/media/wbs2022/video-thumb1.png" src="img/media/wbs2022/video-thumb1.png"  alt="" style="position: absolute;">
                                            <a href="https://www.youtube.com/watch?v=5lVKIW4zFHA" class="popup-video"  style="position: absolute;"><img src="img/icon/video_play_icon.png" alt=""></a>
                                        </div>
                                        <?php foreach($WBS2022 as $key => $val) { ?>
                                            <div class="col-lg-4 col-sm-6 media-img">
                                                <a href="<?php echo $val['img']; ?>" class="popup-image"><img loading="lazy" src="<?php echo $val['img']; ?>" alt=""></a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="game-single-shape"><img loading="lazy" src="img/images/game_section_shape.png" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- inner-about-area-end -->

        </main>
        <!-- main-area-end -->

        <!-- Footer -->
        <?php include '_footer.php'; ?>

        <!-- Scripts -->
        <?php include '_scripts.php'; ?>
        
    </body>
</html>
