<header class="third-header-bg">
    <div class="bg"></div>
    <div class="container custom-container">
        <div class="header-top-area t-header-top-area d-none d-lg-block">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-top-social">
                        <ul>
                            <li>Follow</li>
                            <li><a class="flex-center" href="<?php echo $BFK_FACEBOOK_LINK; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a class="flex-center" href="<?php echo $BFK_TWITTER_LINK; ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <li><a class="flex-center" href="<?php echo $BFK_INSTAGRAM_LINK; ?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
                            <li><a class="flex-center" href="<?php echo $BFK_YT_LINK; ?>" target="_blank"><i class="fab fa-youtube"></i></a></li>
                            <li>
                                <!-- <a class="flex-center" href="<?php echo $BFK_TELEGRAM_OFFICIAL_LINK; ?>" target="_blank"><i class="fab fa-telegram"></i></a> -->

                                <a target="_blank" class="telegram-trigger" tabindex>
                                            
                                    <!-- Telegram Popup -->
                                    <div class="telegram-popup telegram-popup--right">
                                        <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_OFFICIAL_LINK; ?>')"><i class="fab fa-telegram purify"></i>Official</div>
                                        <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_ID_LINK; ?>')"><i class="fab fa-telegram purify"></i>Indonesian Group</div>
                                        <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_JP_LINK; ?>')"><i class="fab fa-telegram purify"></i>Japanese Group</div>
                                        <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_TR_LINK; ?>')"><i class="fab fa-telegram purify"></i>Turkish Group</div>
                                        <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_SHILL_LINK; ?>')"><i class="fab fa-telegram purify"></i>Shill Squad</div>
                                        <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_ANNOUNCEMENTS_LINK; ?>')"><i class="fab fa-telegram purify"></i>Announcements</div>
                                    </div>
                                    
                                    <b class="absolute-center"><i class="fab fa-telegram"></i></b>
                                </a>
                            </li>
                            <li><a class="flex-center" href="<?php echo $BFK_DISCORD_LINK; ?>" target="_blank"><i class="fab fa-discord"></i></a></li>
                            <li><a class="flex-center" href="<?php echo $BFK_TWITCH_LINK; ?>" target="_blank"><i class="fab fa-twitch"></i></a></li>
                            <li><a class="flex-center" href="<?php echo $BFK_TIKTOK_LINK; ?>" target="_blank"><i class="fab fa-tiktok mb-2"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!-- Font Awesome Pro 5.15.4 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) --><path fill="#e4a101" d="M448,209.91a210.06,210.06,0,0,1-122.77-39.25V349.38A162.55,162.55,0,1,1,185,188.31V278.2a74.62,74.62,0,1,0,52.23,71.18V0l88,0a121.18,121.18,0,0,0,1.86,22.17h0A122.18,122.18,0,0,0,381,102.39a121.43,121.43,0,0,0,67,20.14Z"/></svg></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="header-top-login">
                        <ul>
                            <!-- Urgent Announcement -->
                            <?php 
                                if(isset($ANNOUNCEMENTS) && count($ANNOUNCEMENTS) > 0) { 
                                    foreach($ANNOUNCEMENTS as $k => $v) {
                                        if($v['isUrgent'] == 1) {
                            ?>
                                <li><div class="clickable urgent-update-btn" onclick="openAnnouncementPopup(`<?php echo encodeURIComponent($v['description']); ?>`)"><i class="fa fa-exclamation-triangle"></i> &nbsp; Urgent Update &nbsp; <i class="fa fa-caret-right"></i></div></li>
                            <?php 
                                break; }}}
                            ?>
                            
                            <li><a href="" target="_blank"><i class="far fa-user"></i>Register</a></li>
                            <li class="or">or</li>
                            <li><a href="" target="_blank"><i class="far fa-user"></i>Sign In</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="sticky-header">
        <div class="container custom-container desktop-header">
            <div class="row">
                <div class="col-12">
                    <div class="main-menu menu-style-two">
                        <nav>
                            <div class="logo d-block d-lg-none">
                                <a href="index.php"><img src="img/logo/logo_secondary.png" alt="Logo"></a>
                            </div>
                            <div class="navbar-wrap d-none d-lg-flex">
                                <ul class="left">
                                    <li><a href="index.php">Home</a></li>
                                    <li>
                                        <a href="tokenomics.php">
                                            <div class="nav-badge-holder">BFK
                                                <span class="nav-badge badge badge--blue">V3 Contract</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="bootcamp.php">
                                            <div class="nav-badge-holder">Bootcamp 
                                                <span class="nav-badge badge badge--blue">EARLY VIEW</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li><a href="changelog.php">
                                        <div class="nav-badge-holder">Gamelog 
                                            <span class="nav-badge badge badge--blue">UPDATED</span>
                                        </div>
                                    </a></li>
                                    <li><a href="<?php echo $BFK_CONTRIBUTION_LINK; ?>">
                                            <div class="nav-badge-holder">PRIVATE SALE
                                                <span class="nav-badge badge badge--red">LIVE</span>
                                            </div>
                                        </a></li>
                                   
                                </ul>
                                <div class="logo">
                                    <a href="index.php"><img src="img/logo/h3_logo.png" alt="Logo"></a>
                                </div>
                                <ul class="right">
                                    <li>
                                        <a href="<?php echo $BFK_STORE_LINK; ?>">
                                            <div class="nav-badge-holder">MERCH STORE
                                                <span class="nav-badge badge badge--blue">NEW</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $BFK_MARKETPLACE_LINK; ?>" target="_blank">
                                            <div class="nav-badge-holder">Marketplace
                                                <span class="nav-badge badge badge--green">ACTIVE</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li><a href="#">More</a>
                                        <ul class="submenu">
                                            <li style="margin-left:0 !important"><a href="media.php">Media</a></li>
                                            <li style="margin-left:0 !important"><a href="reward.php">Rewards</a></li>
                                            <li style="margin-left:0 !important"><a href="team.php">Team</a></li>
                                            <li style="margin-left:0 !important"><a href="contact.php" >About Us</a></li>
                                            <li style="margin-left:0 !important"><a href="support.php" >Support</a></li>
                                            <li style="margin-left:0 !important"><a href="https://links.bfkwarzone.com/" target="_blank">Multi-Links</a></li>
                                            <li style="margin-left:0 !important"><a href="oldContractV2.php" >Old Contract V2</a></li>
                                        </ul>
                                    </li>
                                    
                                    <?php if(time() > $BFK_GAME_RELEASE) { ?>
                                        <li class="flex-row-center no-hover-effect" style="margin-left: 20px;">
                                            <div class="nav-badge-holder">
                                                <a href="" target="_blank" class="btn-styled clickable">
                                                    Play Beta <i class="fa fa-arrow-right"></i> 
                                                </a>
                                                <span class="nav-badge badge badge--red">INACTIVE</span>
                                            </div>
                                        </li>
                                    <?php } else { ?>
                                        <li class="flex-row-center no-hover-effect" style="margin-left: 20px;"><button onclick="showAlphaPopup()" class="btn-styled clickable">Play Alpha <i class="fa fa-arrow-right"></i></button></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="mobile-menu-wrap d-block d-lg-none">
                        <nav>
                            <!-- <div class="mobile-play-alpha-nav-wrapper">
                                <a href="<?php echo $BFK_ALPHA_GAME_LINK; ?>" class="btn-styled btn-styled--sm" target="_blank">Play Alpha <i class="fa fa-arrow-right"></i></a>
                            </div> -->
                            <div id="mobile-menu" class="navbar-wrap">
                                <ul>
                                    <!-- Urgent Announcement -->
                                    <?php 
                                        if(isset($ANNOUNCEMENTS) && count($ANNOUNCEMENTS) > 0) { 
                                            foreach($ANNOUNCEMENTS as $k => $v) {
                                                if($v['isUrgent'] == 1) {
                                    ?>
                                        <li><a class="clickable urgent-update-btn" onclick="openAnnouncementPopup(`<?php echo encodeURIComponent($v['description']); ?>`)"><i class="fa fa-exclamation-triangle"></i> &nbsp; Urgent Update &nbsp; <i class="fa fa-caret-right"></i></a></li>
                                    <?php 
                                        break; }}}
                                    ?>

                                    <li class="show"><a href="index.php">Home</a></li>
                                    <!-- <li><a href="https://bfkwarzone.com" target="_blank">About</a></li> -->
                                    <li><a href="tokenomics.php">BFK <span class="badge badge--blue">V3 Contract</span></a></li>
                                    <!-- <li><a href="bootcamp.php" target="_blank">Bootcamp</a></li> -->
                                    <li><a href="bootcamp.php">Bootcamp <span class="badge badge--blue">EARLY VIEW</span></a></li>
                                    <li><a href="media.php">Media</a></li>
                                    <li><a href="<?php echo $BFK_CONTRIBUTION_LINK; ?>">PRIVATE SALE <span class="badge badge--red">LIVE</span></a></li>
                                    <li><a href="<?php echo $BFK_STORE_LINK; ?>">MERCH STORE <span class="badge badge--blue">NEW</span></a></li>
                                    <li><a href="changelog.php">Gamelog <span class="badge badge--blue">UPDATED</span></a></li>
                                    <li><a href="reward.php">Rewards <span class="badge badge--red">COMING SOON</span></a></li>
                                    <li><a href="<?php echo $BFK_MARKETPLACE_LINK; ?>" target="_blank">Marketplace <span class="badge badge--green">ACTIVE</span></a></li>
                                    <li><a href="contact.php">About Us</a></li>
                                    <li><a href="oldContractV2.php">Old Contract V2</a></li>
                                    <li><a href="javascript:openAlertPopup(`<img class='copyright-img' src='img/copyright/copyright.png' alt='' height='100%' width='100%'>`)">Copyright</a></li>
                                    <?php if(time() > $BFK_GAME_RELEASE) { ?>
                                        <li><a href="team.php">Team</a></li>
                                    <?php } ?>
                                    <?php if(time() > $BFK_GAME_RELEASE) { ?>
                                        <li><a href="" target="_blank">Play Beta <span class="badge badge--red">INACTIVE</span></a></li>
                                    <?php } else { ?>
                                        <li><a onclick="showAlphaPopup()" class="clickable">Play Alpha</a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="mobile-menu"></div>
                </div>
                <!-- Modal Search -->
                <div class="modal fade" id="search-modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form>
                                <input type="text" placeholder="Search here...">
                                <button><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom-bg"></div>
</header>