<?php include '_media_event.php'; ?>

<!doctype html>
<html class="no-js" lang="">
    <head>

        <!-- Head -->
        <?php include '_head.php'; ?>

    </head>
    <body>

        <!-- Preloader -->
        <?php include '_preloader.php'; ?>

        <!-- Header -->
        <?php include '_header.php'; ?>

        <!-- main-area -->
        <main>

            <!-- Announcements -->
            <?php include '_announcements.php'; ?>

            <!-- breadcrumb-area -->
            <section class="breadcrumb-area breadcrumb-bg team-breadcrumbs">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb-content text-center">
                                <h2>MEDIA</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Media</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- breadcrumb-area-end -->

            <!-- inner-about-area -->
            <section class="upcoming-games-area upcoming-games-bg pt-120 pb-80">
                <div class="container">
                    <div class="row">
                        <?php foreach(array_reverse($EVENT) as $key => $val) { ?>
                            <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                                <div class="upcoming-game-item mb-40" onclick="location.href='<?php echo $val['link']; ?>';">
                                    <div class="upcoming-game-head">
                                        <div class="uc-game-head-title">
                                            <span><?php echo $val['date']; ?></span>
                                            <h4><a href="<?php echo $val['link']; ?>"><?php echo $val['name']; ?></a></h4>
                                        </div>
                                    </div>
                                    <div class="upcoming-game-thumb media-event-img">
                                        <img loading="lazy" src="<?php echo $val['img']; ?>" alt="">
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </section>
            <!-- inner-about-area-end -->

        </main>
        <!-- main-area-end -->

        <!-- Footer -->
        <?php include '_footer.php'; ?>

        <!-- Scripts -->
        <?php include '_scripts.php'; ?>
        
    </body>
</html>
