<?php

    include '../env.php';

    // Create connection
    $conn = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Get form values
    if(isset($_POST['id'])) {

        // Form Data
        $id = $_POST['id'];
        $usrPassword = $_POST['password'];

        if($usrPassword != 'BFK!99') {
            http_response_code(401);
            $msg = urlencode('Incorrect password');
            $pw = urlencode($usrPassword);
            echo '<script>location.href = "../changelog-admin.php?pw='.$pw.'&msg='.$msg.'";</script>';
        }
        else {

            $sql = "UPDATE changelog SET isDeleted = 1 WHERE id = ?";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param("i", $id);
    
            try {
                $stmt->execute();
                http_response_code(200);
                echo '<script>location.href = "../changelog-admin.php?pw=BFK!99";</script>';
            }
            catch(Error $e) {
                http_response_code(500);
                echo 'Something went wrong';
            }
        }

    }
    else {
        http_response_code(400);
        echo 'Missing fields';
    }
    
    $conn->close();

?>