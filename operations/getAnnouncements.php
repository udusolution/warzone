<?php

    include 'env.php';

    // Create connection
    $conn = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Get Announcements
    $res3 = $conn->query("SELECT * FROM announcement WHERE isDeleted = 0 ORDER BY id DESC");
    $ANNOUNCEMENTS = [];
    if($res3){
    while ($row3 = $res3->fetch_assoc()) {
        array_push($ANNOUNCEMENTS, $row3);
    }
    }
    // Close Connection    
    $conn->close();

?>