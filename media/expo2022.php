<?php

$EXPO2022 = [
    array(
        "id" => 1,  
        "img" => "img/media/expo2022/00001.jpeg"
    ),
    array(
        "id" => 2,  
        "img" => "img/media/expo2022/00002.jpeg"
    ),
    array(
        "id" => 3,  
        "img" => "img/media/expo2022/00003.jpeg"
    ),
    array(
        "id" => 4,  
        "img" => "img/media/expo2022/00004.jpeg"
    ),
    array(
        "id" => 5,  
        "img" => "img/media/expo2022/00005.jpeg"
    ),
    array(
        "id" => 6,  
        "img" => "img/media/expo2022/00006.jpeg"
    ),
    array(
        "id" => 7,  
        "img" => "img/media/expo2022/00007.jpeg"
    ),
    array(
        "id" => 8,  
        "img" => "img/media/expo2022/00008.jpeg"
    ),
    array(
        "id" => 9,  
        "img" => "img/media/expo2022/00009.jpeg"
    ),
    array(
        "id" => 10,  
        "img" => "img/media/expo2022/00010.jpeg"
    ),
    array(
        "id" => 11,  
        "img" => "img/media/expo2022/00011.jpeg"
    ),
    array(
        "id" => 12,  
        "img" => "img/media/expo2022/00012.jpeg"
    ),
    array(
        "id" => 13,  
        "img" => "img/media/expo2022/00013.jpeg"
    ),
    array(
        "id" => 14,  
        "img" => "img/media/expo2022/00014.jpeg"
    ),
    array(
        "id" => 15,  
        "img" => "img/media/expo2022/00015.jpeg"
    ),
    array(
        "id" => 16,  
        "img" => "img/media/expo2022/00016.jpeg"
    ),

    array(
        "id" => 17,  
        "img" => "img/media/expo2022/00017.jpeg"
    ),
    array(
        "id" => 18,  
        "img" => "img/media/expo2022/00018.jpeg"
    ),
    array(
        "id" => 19,  
        "img" => "img/media/expo2022/00019.jpeg"
    ),

    array(
        "id" => 20,  
        "img" => "img/media/expo2022/00020.jpeg"
    ),
    array(
        "id" => 21,  
        "img" => "img/media/expo2022/00021.jpeg"
    ),
    array(
        "id" => 22,  
        "img" => "img/media/expo2022/00022.jpeg"
    ),

    array(
        "id" => 23,  
        "img" => "img/media/expo2022/00023.jpeg"
    ),
    array(
        "id" => 24, 
        "img" => "img/media/expo2022/00024.jpeg"
    ),
    array(
        "id" => 25,  
        "img" => "img/media/expo2022/00025.jpeg"
    ),
]

?>

<script>

    const EXPO2022 = <?php echo json_encode($EXPO2022); ?>;

</script>