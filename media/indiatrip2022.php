<?php

$INDIATRIP2022 = [
    array(
        "id" => 1,  
        "img" => "img/media/indiatrip2022/1.jpg"
    ),
    array(
        "id" => 2,  
        "img" => "img/media/indiatrip2022/2.jpg"
    ),
    array(
        "id" => 3,  
        "img" => "img/media/indiatrip2022/3.jpg"
    ),
    array(
        "id" => 4,  
        "img" => "img/media/indiatrip2022/4.jpg"
    ),
    array(
        "id" => 5,  
        "img" => "img/media/indiatrip2022/5.jpg"
    ),
    array(
        "id" => 6,  
        "img" => "img/media/indiatrip2022/6.jpg"
    ),
    array(
        "id" => 7,  
        "img" => "img/media/indiatrip2022/7.jpg"
    ),
    array(
        "id" => 8,  
        "img" => "img/media/indiatrip2022/8.jpg"
    ),
]

?>

<script>

    const INDIATRIP2022 = <?php echo json_encode($INDIATRIP2022); ?>;

</script>