<?php

$WBS2022 = [
    array(
        "id" => 1,  
        "img" => "img/media/wbs2022/00001.jpg"
    ),
    array(
        "id" => 2,  
        "img" => "img/media/wbs2022/00002.jpg"
    ),
    array(
        "id" => 3,  
        "img" => "img/media/wbs2022/00003.jpg"
    ),
    array(
        "id" => 4,  
        "img" => "img/media/wbs2022/00004.jpg"
    ),
    array(
        "id" => 5,  
        "img" => "img/media/wbs2022/00005.jpg"
    ),
    array(
        "id" => 6,  
        "img" => "img/media/wbs2022/00006.jpg"
    ),
    array(
        "id" => 7,  
        "img" => "img/media/wbs2022/00007.jpg"
    ),
    array(
        "id" => 8,  
        "img" => "img/media/wbs2022/00008.jpg"
    ),
    array(
        "id" => 9,  
        "img" => "img/media/wbs2022/00009.jpg"
    ),
    array(
        "id" => 10,  
        "img" => "img/media/wbs2022/00010.jpg"
    ),
    array(
        "id" => 11,  
        "img" => "img/media/wbs2022/00011.jpg"
    ),
    array(
        "id" => 12,  
        "img" => "img/media/wbs2022/00012.jpg"
    ),
    array(
        "id" => 13,  
        "img" => "img/media/wbs2022/00013.jpg"
    ),
    array(
        "id" => 14,  
        "img" => "img/media/wbs2022/00014.jpg"
    ),
    array(
        "id" => 15,  
        "img" => "img/media/wbs2022/00015.jpg"
    ),
    array(
        "id" => 16,  
        "img" => "img/media/wbs2022/00016.jpg"
    ),
    array(
        "id" => 17,  
        "img" => "img/media/wbs2022/00017.jpg"
    ),
    array(
        "id" => 18,  
        "img" => "img/media/wbs2022/00018.jpg"
    ),
    array(
        "id" => 19,  
        "img" => "img/media/wbs2022/00019.jpg"
    ),
    array(
        "id" => 20,  
        "img" => "img/media/wbs2022/00020.jpg"
    ),
    array(
        "id" => 21,  
        "img" => "img/media/wbs2022/00021.jpg"
    ),
    array(
        "id" => 22,  
        "img" => "img/media/wbs2022/00022.jpg"
    ),
    array(
        "id" => 23,  
        "img" => "img/media/wbs2022/00023.jpg"
    ),
    array(
        "id" => 24, 
        "img" => "img/media/wbs2022/00024.jpg"
    ),
    array(
        "id" => 25,  
        "img" => "img/media/wbs2022/00025.jpg"
    ),
    array(
        "id" => 26,  
        "img" => "img/media/wbs2022/00026.jpg"
    ),
]

?>

<script>

    const WBS2022 = <?php echo json_encode($WBS2022); ?>;

</script>