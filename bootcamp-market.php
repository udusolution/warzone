<!-- Bootcamp Page. Note that bootcamp content such as forti information, weapons, etc are stored in the "_fortis.php" file -->

<?php include '_fortis.php'; ?>

<!DOCTYPE html>
<html class="no-js" lang="">
    <head>

        <!-- Head -->
        <?php include '_head.php'; ?>

    </head>
    <body>

        <!-- Preloader -->
        <?php include '_preloader.php'; ?>

        <!-- Header -->
        <header class="third-header-bg market">
            <div id="sticky-header">
                <div class="container custom-container desktop-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="mobile-menu-wrap d-block d-lg-none">
                                <nav>
                                    <div id="mobile-menu" class="navbar-wrap">
                                        <ul>
                                            <li class="show"><a href="">Home</a></li>
                                            <!-- <li><a href="https://bfkwarzone.com" target="_blank">About</a></li> -->
                                            <li><a href="">BFK <span class="badge badge--blue">CONTRACT</span></a></li>
                                            <!-- <li><a href="bootcamp.php" target="_blank">Bootcamp</a></li> -->
                                            <li><a href="">Bootcamp <span class="badge badge--blue">EARLY VIEW</span></a></li>
                                            <li><a href="">Gamelog <span class="badge badge--blue">UPDATED</span></a></li>
                                            <li><a href="<?php echo $BFK_MARKETPLACE_LINK; ?>" target="_blank">Marketplace <span class="badge badge--green">ACTIVE</span></a></li>
                                            <li><a href="">Contact</a></li>
                                            <li><a href="javascript:openAlertPopup(`<img class='copyright-img' src='img/copyright/copyright.png' alt='' height='100%' width='100%'>`)">Copyright</a></li>
                                            <?php if(time() > $BFK_GAME_RELEASE) { ?>
                                                <li><a href="">Team</a></li>
                                            <?php } ?>
                                            <?php if(time() > $BFK_GAME_RELEASE) { ?>
                                                <li><a href="<?php echo $BFK_MARKETPLACE_LINK; ?>" target="_blank">Play Beta <span class="badge badge--red">LIVE</span></a></li>
                                            <?php } else { ?>
                                                <li><a onclick="showAlphaPopup()" class="clickable">Play Alpha</a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- main-area -->
        <main class="bootcamp-page">

            <!-- Page Top -->
            <div class="bootcamp-page__top">

                <!-- Page Title -->
                <div>
                    <div class="bootcamp-page__title">BFK <span class="primary-color">BOOTCAMP</span></div>
                    <div class="bootcamp-page__subtitle">LEARNATORIUM</div>
                </div>

                <!-- Sections Nav -->
                <nav class="bootcamp-page__nav">
                    <div onclick="onTabClicked(1, this)" class="bootcamp-page__nav-item bootcamp-page__nav-item--active">FORTIS</div>
                    <div onclick="onTabClicked(2, this)" class="bootcamp-page__nav-item">ALIENS</div>
                    <div onclick="onTabClicked(3, this)" class="bootcamp-page__nav-item">WEAPONS</div>
                    <div class="bootcamp-page__nav-item bootcamp-page__nav-item--disabled">EQUIPMENTS</div>
                    <div onclick="onTabClicked(4, this)" class="bootcamp-page__nav-item">INFINITY<span class="cut-500"> KNOX</span></div>
                    <div onclick="onTabClicked(5, this)" class="bootcamp-page__nav-item">POWERUP<span class="cut-500"> KNOX</span></div>
                    <div onclick="onTabClicked(6, this)" class="bootcamp-page__nav-item">FORTIS RANKINGS</div>
                    <div onclick="onTabClicked(7, this)" class="bootcamp-page__nav-item">PROFILE RANKINGS</div>
                    <div onclick="onTabClicked(8, this)" class="bootcamp-page__nav-item">FORTI SKILLS</div>
                    <div onclick="onTabClicked(9, this); startP2ECountUp();" class="bootcamp-page__nav-item">P2E Rewards</div>
                    <div onclick="onTabClicked(10, this)" class="bootcamp-page__nav-item">XP POINTS</div>

                    <div onclick="openFortiSupplyPopup()" class="bootcamp-page__nav-item bootcamp-page__nav-item--secondary" style="margin-left: 10px">Forti Supply</div>
                    <div onclick="openRankingSystemPopup()" class="bootcamp-page__nav-item bootcamp-page__nav-item--secondary">Ranking System</div>
                    <div onclick="openShardsPopup()" class="bootcamp-page__nav-item bootcamp-page__nav-item--secondary">Shards</div>
                    <div onclick="openECOSystemPopup()" class="bootcamp-page__nav-item bootcamp-page__nav-item--secondary">ECO System</div>

                </nav>

            </div>

            

            <!-- Sections -->
            <div class="bootcamp-sections-wrapper">
            <div class="bootcamp-sections">

                <!-- FORTIS -->
                <section class="bootcamp-section" data-bc="fortis">
    
                    <!-- Main Menu Pane -->
                    <div class="bootcamp-section__menu--wrapper">
                        <div class="bootcamp-section__menu">
    
                            <!-- Factions Menu -->
                            <div class="menu__nav-wrapper">
                            <nav class="menu__nav">
                                <div class="menu__item clickable menu__item--active" onclick="onCategoryClicked_fortis('all', this)">
                                    <img class="menu__icon" src="img/icon/all-fortis.png" />
                                    <div class="menu__text">All Fortis</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_fortis('army', this)">
                                    <img class="menu__icon" src="img/icon/army.png" />
                                    <div class="menu__text">Army</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_fortis('marine_corps', this)">
                                    <img class="menu__icon" src="img/icon/marine-corps.png" />
                                    <div class="menu__text">Marine Corps</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_fortis('navy', this)">
                                    <img class="menu__icon" src="img/icon/navy.png" />
                                    <div class="menu__text">Navy</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_fortis('air_force', this)">
                                    <img class="menu__icon" src="img/icon/air-force.png" />
                                    <div class="menu__text">Air Force</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_fortis('space_force', this)">
                                    <img class="menu__icon" src="img/icon/space-force.png" />
                                    <div class="menu__text">Space Force</div>
                                </div>
                                <div class="menu__item menu__item--disabled" title="Recruiting Soon">
                                    <img class="menu__icon" src="img/icon/coast-guard.png" />
                                    <div class="menu__text">Coast Guard</div>
                                </div>
                            </nav>
                            </div>
    
                            <!-- Forti Cards -->
                            <div class="bootcamp-section__cards"></div>
    
                        </div>
                    </div>
    
                    <!-- Forti Details Pane -->
                    <div class="bootcamp-section__details--wrapper">
                        <div class="bootcamp-section__details">

                            <!-- Top -->
                            <div class="bootcamp-section__details__top">

                                <!-- Back Button -->
                                <button class="details__back-btn" onclick="unshiftPane_fortis()"><i class="fa fa-chevron-left"></i> <span class="details__back-btn-text">Forti Name</span></button>

                                <div class="bootcamp-section__details__top__right">
                                    <!-- Watch 3D Video -->
                                    <a class="popup-video bootcamp-section__forti-video-btn" href="img/videos/armis.mp4"><i class="fa fa-video"></i> watch 3D</a>

                                    <!-- Share Button -->
                                    <div class="bootcamp-section__share-forti" onclick="shareForti()"><i class="fa fa-share"></i><i class="fa fa-check"></i> <span>share</span></div>
                                </div>

                            </div>
    
                            <div class="details__flex">
    
                                <div class="details__left">

                                    <!-- Forti Description -->
                                    <div class="details__description details-box clickable" onclick="openFortiDescriptionPopup()">
                                        <h4>Story</h4>
                                        <div class="details__description-text-wrapper">
                                            <span class="details__description-text"></span>
                                            <span class="read-more-btn">read more <i class="fa fa-caret-right"></i></span>
                                        </div>
                                        <div class="details__description__icons"></div>
                                    </div>

                                    <!-- Forti Specs -->
                                    <div class="details__specs details-box">
                                        <!-- <h4>Story</h4> -->
                                        <div class="details__specs__rarity">Supply <span class="details__specs__rarity-label product-rarity product-rarity--rounded"></span></div>
                                        <ul class="details__specs__list"></ul>
                                    </div>

                                    <!-- Price -->
                                    <div class="details__price details__price--available details-box details-box--alt">
                                        <div class="details__price__amount"></div>
                                        <div class="details__price__note">Will be transferred for the value of BFK</div>
                                        <div class="details__price__available">Available at Marketplace</div>
                                    </div>

                                </div>
    
                                <!-- Forti Cover -->
                                <a class="popup-image details__cover-wrapper">
                                    <div class="img-loader">
                                        <img class="details__cover" />
                                    </div>
                                </a>
    
                                <!-- Forti Stats -->
                                <div class="details__stats">
    
                                    <!-- Forti Stats: Customizations -->
                                    <div class="stats__customizations details-box">

                                        
                                        <!-- Stats Tab Nav -->
                                        <div class="stats__categories">
                                            <!-- Weapons -->
                                            <div class="stats__category" title="Battle equipment" data-stat-tab="weapons" onclick="onStatTabClicked('weapons')">
                                                <div class="stats__category-icon-wrapper">
                                                    <img class="stats__category-icon" src="img/icon/p1.png" />
                                                </div>
                                                <div class="stats__category-title">Weapons</div>
                                            </div>
                                            <!-- Skins -->
                                            <div class="stats__category stats__category--locked" title="Coming soon" data-stat-tab="skins">
                                                <div class="stats__category-icon-wrapper">
                                                    <img class="stats__category-icon" src="img/icon/p3.png" />
                                                </div>
                                                <div class="stats__category-title">Skins</div>
                                            </div>
                                            <!-- Infinity Knox -->
                                            <div class="stats__category stats__category--check" title="Affected by Knox Power">
                                                <div class="stats__category-icon-wrapper">
                                                    <img class="stats__category-icon" src="img/icon/p2.png" />
                                                </div>
                                                <div class="stats__category-title">Infinity Knox</div>
                                            </div>
                                            <!-- Power Ups -->
                                            <div class="stats__category stats__category--check" title="Power Ups can be activated">
                                                <div class="stats__category-icon-wrapper">
                                                    <img class="stats__category-icon" src="img/icon/p4.png" />
                                                </div>
                                                <div class="stats__category-title">Power Ups</div>
                                            </div>
                                            <!-- Special Ability -->
                                            <div class="stats__category stats__category--star" title="Special ability" id="fortiAbility">
                                                <div class="stats__category-icon-wrapper">
                                                    <img class="stats__category-icon" src="img/icon/arrow.png" />
                                                </div>
                                                <div class="stats__category-title">Special Ability</div>
                                            </div>
                                        </div>

                                        <!-- Stats Item: Weapons -->
                                        <div class="stats__weapons" data-stat="weapons">
                                            <div class="stats__weapon img-loader" onclick="onFortiWeaponClicked(0)">
                                                <img class="stats__weapon-img" />
                                                <div class="stats__weapon-name"></div>
                                            </div>
                                            <div class="stats__weapon img-loader" onclick="onFortiWeaponClicked(1)">
                                                <div class="stats__weapon-name"></div>
                                                <img class="stats__weapon-img" />
                                            </div>
                                        </div>

                                        <!-- Stats Item: Skins -->
                                        <div class="stats__weapons" data-stat="skins"></div>

                                    </div>
    
                                    <!-- Forti Stats: Perks -->
                                    <div class="details-box">
                                        <div class="stats__perks"></div>
                                    </div>
    
                                </div>
    
                            </div>
                        </div>
                    </div>
    
                </section>

                <!-- ALIENS -->
                <section class="bootcamp-section" data-bc="aliens">
    
                    <!-- Main Menu Pane -->
                    <div class="bootcamp-section__menu--wrapper">
                        <div class="bootcamp-section__menu">
    
                            <!-- Factions Menu -->
                            <div class="menu__nav-wrapper">
                            <nav class="menu__nav">
                                <div class="menu__item clickable menu__item--active" onclick="onCategoryClicked_aliens('all', this)">
                                    <img class="menu__icon" src="img/icon/all-fortis.png" />
                                    <div class="menu__text">All Aliens</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_aliens('alien_chain', this)">
                                    <img class="menu__icon" src="img/icon/army.png" />
                                    <div class="menu__text">Ethenum AS-ST</div>
                                </div>
                            </nav>
                            </div>
    
                            <!-- Alien Cards -->
                            <div class="bootcamp-section__cards"></div>
    
                        </div>
                    </div>
    
                    <!-- Alien Details Pane -->
                    <div class="bootcamp-section__details--wrapper">
                        <div class="bootcamp-section__details">

                            <!-- Top -->
                            <div class="bootcamp-section__details__top">

                                <!-- Back Button -->
                                <button class="details__back-btn" onclick="unshiftPane_aliens()"><i class="fa fa-chevron-left"></i> <span class="details__back-btn-text">Alien Name</span></button>

                                <div class="bootcamp-section__details__top__right">
                                    <!-- Watch 3D Video -->
                                    <a class="popup-video bootcamp-section__alien-video-btn" href="img/videos/armis.mp4"><i class="fa fa-video"></i> watch 3D</a>

                                    <!-- Share Button -->
                                    <div class="bootcamp-section__share-alien" onclick="shareAlien()"><i class="fa fa-share"></i><i class="fa fa-check"></i> <span>share</span></div>
                                </div>

                            </div>
    
                            <div class="details__flex">
    
                                <div class="details__left">

                                    <!-- Alien Description -->
                                    <div class="details__description details-box clickable" onclick="openFortiDescriptionPopup()">
                                        <h4>Story</h4>
                                        <div class="details__description-text-wrapper">
                                            <span class="details__description-text"></span>
                                            <span class="read-more-btn">read more <i class="fa fa-caret-right"></i></span>
                                        </div>
                                        <div class="details__description__icons"></div>
                                    </div>

                                    <!-- Alien Specs -->
                                    <div class="details__specs details-box">
                                        <!-- <h4>Story</h4> -->
                                        <div class="details__specs__rarity">Supply <span class="details__specs__rarity-label product-rarity product-rarity--rounded"></span></div>
                                        <ul class="details__specs__list"></ul>
                                    </div>

                                    <!-- Price -->
                                    <div class="details__price details__price--available details-box details-box--alt">
                                        <div class="details__price__amount"></div>
                                        <div class="details__price__note">killing the alien in events will drop rare loot</div>
                                        <div class="details__price__available">Events Only Fights</div>
                                    </div>

                                </div>
    
                                <!-- Alien Cover -->
                                <a class="popup-image details__cover-wrapper">
                                    <div class="img-loader">
                                        <img class="details__cover" />
                                    </div>
                                </a>
    
                                <!-- Alien Stats -->
                                <div class="details__stats">
    
                                    <!-- Alien Stats: Customizations -->
                                    <div class="stats__customizations details-box">

                                        

                                        <div class="stats__categories">
                                            <div class="stats__category" title="Battle equipment">
                                                <div class="stats__category-icon-wrapper">
                                                    <img class="stats__category-icon" src="img/icon/p1.png" />
                                                </div>
                                                <div class="stats__category-title">Weapons</div>
                                            </div>
                                            <div class="stats__category stats__category--locked">
                                                <div class="stats__category-icon-wrapper">
                                                    <img class="stats__category-icon" src="img/icon/p2.png" />
                                                </div>
                                                <div class="stats__category-title">Skins</div>
                                            </div>
                                            <div class="stats__category" title="XAR Element">
                                                <div class="stats__category-icon-wrapper stats__category--check">
                                                    <img class="stats__category-icon" src="img/icon/p3.png" />
                                                </div>
                                                <div class="stats__category-title">Core Shards</div>
                                            </div>
                                            <div class="stats__category" title="Auto detect Enemies">
                                                <div class="stats__category-icon-wrapper stats__category--check">
                                                    <img class="stats__category-icon" src="img/icon/p4.png" />
                                                </div>
                                                <div class="stats__category-title">Auto Assault</div>
                                            </div>
                                            <div class="stats__category" title="Special ability" id="alienAbility">
                                                <div class="stats__category-icon-wrapper stats__category--star">
                                                    <img class="stats__category-icon" src="img/icon/arrow.png" />
                                                </div>
                                                <div class="stats__category-title">Special Ability</div>
                                            </div>
                                        </div>
                                        <div class="stats__weapons">
                                            <div class="stats__weapon img-loader" onclick="onAlienWeaponClicked(0)">
                                                <img class="stats__weapon-img" />
                                                <div class="stats__weapon-name"></div>
                                            </div>
                                            <div class="stats__weapon img-loader" onclick="onAlienWeaponClicked(1)">
                                                <div class="stats__weapon-name"></div>
                                                <img class="stats__weapon-img" />
                                            </div>
                                        </div>
                                    </div>
    
                                    <!-- Alien Stats: Perks -->
                                    <div class="details-box">
                                        <div class="stats__perks"></div>
                                    </div>
    
                                </div>
    
                            </div>
                        </div>
                    </div>
    
                </section>

                <!-- WEAPONS -->
                <section class="bootcamp-section" data-bc="weapons">
    
                    <!-- Main Menu Pane -->
                    <div class="bootcamp-section__menu--wrapper">
                        <div class="bootcamp-section__menu">
    
                            <!-- Weapon Classes Menu -->
                            <div class="menu__nav-wrapper">
                            <nav class="menu__nav">
                                <!-- All Weapons, Hand Guns, Shotgun, SMG, Automatic Rifle, LMG, HMG, Sniper, Heavy Sniper -->
                                <div class="menu__item clickable menu__item--active" onclick="onCategoryClicked_weapons('all', this)">
                                    <img class="menu__icon" src="img/favicon.png" />
                                    <div class="menu__text">All Weapons</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_weapons('hand-gun', this)">
                                    <img class="menu__icon menu__icon--weapon" src="img/weapons/sm/Pistol.png" />
                                    <div class="menu__text">Hand Guns</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_weapons('shotgun', this)">
                                    <img class="menu__icon menu__icon--weapon" src="img/weapons/sm/Shotgun.png" />
                                    <div class="menu__text">Shotgun</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_weapons('smg', this)">
                                    <img class="menu__icon menu__icon--weapon" src="img/weapons/sm/MP5K.png" />
                                    <div class="menu__text">SMG</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_weapons('automatic-rifle', this)">
                                    <img class="menu__icon menu__icon--weapon" src="img/weapons/sm/HK416.png" />
                                    <div class="menu__text">Assault Rifle</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_weapons('lmg', this)">
                                    <img class="menu__icon menu__icon--weapon" src="img/weapons/sm/Gun M249.png" />
                                    <div class="menu__text">LMG</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_weapons('hmg', this)">
                                    <img class="menu__icon menu__icon--weapon" src="img/weapons/sm/Gun 240.png" />
                                    <div class="menu__text">HMG</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_weapons('sniper', this)">
                                    <img class="menu__icon menu__icon--weapon" src="img/weapons/sm/gun m110.png" />
                                    <div class="menu__text">Sniper</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_weapons('heavy-sniper', this)">
                                    <img class="menu__icon menu__icon--weapon" src="img/weapons/sm/50 CAL.png" />
                                    <div class="menu__text">Heavy Sniper</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_weapons('rocket-launcher', this)">
                                    <img class="menu__icon menu__icon--weapon" src="img/weapons/sm/M136_AT4.png" />
                                    <div class="menu__text">Rocket Launcher</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_weapons('grenade-launcher', this)">
                                    <img class="menu__icon menu__icon--weapon" src="img/weapons/sm/M32.png" />
                                    <div class="menu__text">Grenade Launcher</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_weapons('flame-thrower', this)">
                                    <img class="menu__icon menu__icon--weapon" src="img/weapons/sm/Boring_F-Thrower.png" />
                                    <div class="menu__text">Flame Thrower</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_weapons('pulse', this)">
                                    <img class="menu__icon menu__icon--weapon" src="img/weapons/sm/LRB_Pulse.png" />
                                    <div class="menu__text">Pulse</div>
                                </div>
                            </nav>
                            </div>
    
                            <!-- Weapon Cards -->
                            <div class="bootcamp-section__cards"></div>
    
                        </div>
                    </div>
    
                    <!-- Weapon Details Pane -->
                    <div class="bootcamp-section__details--wrapper">
                        <div class="bootcamp-section__details">
    
                            <!-- Top -->
                            <div class="bootcamp-section__details__top">

                                <!-- Back Button -->
                                <button class="details__back-btn" onclick="unshiftPane_weapons()"><i class="fa fa-chevron-left"></i> <span class="details__back-btn-text">Forti Name</span></button>
                                
                                <!-- Emphasized Labels -->
                                <div class="bootcamp-section__details__top__labels">

                                </div>

                            </div>
    
                            <div class="details__flex">
    
                                
                                <div class="details__left">

                                    <!-- Weapon Description -->
                                    <div class="details__description details-box">
                                        <h4>Story</h4>
                                        <div class="details__description-text"></div>
                                    </div>

                                    <!-- Weapon Efficiency -->
                                    <div class="details__efficiency details-box">
                                        <h4>Efficiency</h4>
                                        <div class="details__efficiency-text"></div>
                                    </div>

                                    <!-- Price -->
                                    <div class="details__price details__price--available details-box details-box--alt">
                                        <div class="details__price__amount"></div>
                                        <div class="details__price__note">Will be transferred for the value of BFK</div>
                                        <div class="details__price__available">Available at Marketplace</div>
                                    </div>

                                </div>
    
                                <!-- Weapon Cover -->
                                <a class="popup-image details__cover-wrapper img-loader">
                                    <img class="details__cover details__cover--lg" style="align-self: center;" />
                                </a>
    
                                <!-- Weapon Stats -->
                                <div class="details__stats">
                                    
                                    <!-- Weapon Stats: Perks -->
                                    <div class="stats__perks details-box block">
                                        <h4>Specifications</h4>
                                        <table class="stats__perks-table"></table>
                                    </div>
    
                                </div>
    
                            </div>
                        </div>
                    </div>
    
                </section>

                <!-- INFINITY KNOX -->
                <section class="bootcamp-section" data-bc="infinity-knox">
    
                    <!-- Main Menu Pane -->
                    <div class="bootcamp-section__menu--wrapper">
                        <div class="bootcamp-section__menu">
    
                            <!-- Infinity Knox Classes Menu -->
                            <div class="menu__nav-wrapper">
                            <nav class="menu__nav">
                                <!-- All, Quasar, Supernova, Star, Horizon, Comet -->
                                <div class="menu__item clickable menu__item--active" onclick="onCategoryClicked_iKnox('all', this)">
                                    <img class="menu__icon" src="img/favicon.png" />
                                    <div class="menu__text">All</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_iKnox('gold', this)">
                                    <img class="menu__icon" src="img/favicon.png" />
                                    <div class="menu__text">Gold</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_iKnox('emerald', this)">
                                    <img class="menu__icon" src="img/favicon.png" />
                                    <div class="menu__text">Emerald</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_iKnox('quasar', this)">
                                    <img class="menu__icon" src="img/icon/quasar1.png" />
                                    <div class="menu__text">Quasar</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_iKnox('supernova', this)">
                                    <img class="menu__icon" src="img/icon/supernova1.png" />
                                    <div class="menu__text">Supernova</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_iKnox('star', this)">
                                    <img class="menu__icon" src="img/icon/star1.png" />
                                    <div class="menu__text">Star</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_iKnox('horizon', this)">
                                    <img class="menu__icon" src="img/icon/horizon.png" />
                                    <div class="menu__text">Horizon</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_iKnox('comet', this)">
                                    <img class="menu__icon" src="img/icon/comet1.png" />
                                    <div class="menu__text">Comet</div>
                                </div>
                            </nav>
                            </div>
    
                            <!-- Infinity Knox Cards -->
                            <div class="bootcamp-section__cards"></div>
    
                        </div>
                    </div>
    
                    <!-- Infinity Knox Details Pane -->
                    <div class="bootcamp-section__details--wrapper">
                        <div class="bootcamp-section__details">

                            <!-- Top -->
                            <div class="bootcamp-section__details__top">

                                <!-- Back Button -->
                                <button class="details__back-btn" onclick="unshiftPane_iKnox()"><i class="fa fa-chevron-left"></i> <span class="details__back-btn-text">Forti Name</span></button>

                                <div class="bootcamp-section__details__top__right">

                                    <!-- Watch Animation Video -->
                                    <a class="popup-video bootcamp-section__iKnox-video-btn" href=""><i class="fa fa-video"></i> watch animation</a>

                                    <!-- Share Button -->
                                    <div class="bootcamp-section__share-btn" onclick="shareIKnox()"><i class="fa fa-share"></i><i class="fa fa-check"></i> <span>share</span></div>

                                </div>

                            </div>
    
                            <div class="details__flex">
    
                                
                                <div class="details__left">

                                    <!-- Infinity Knox Description -->
                                    <div class="details__description details-box">
                                        <h4>Story</h4>
                                        <div class="details__description-text"></div>
                                    </div>

                                    <!-- Price -->
                                    <div class="details__price details__price--available details-box details-box--alt">
                                        <div class="details__price__amount"></div>
                                        <div class="details__price__note">Will be transferred for the value of BFK</div>
                                        <div class="details__price__available">Available at Marketplace</div>
                                    </div>

                                </div>
    
                                <!-- Infinity Knox Cover -->
                                <a class="popup-image details__cover-wrapper img-loader">
                                    <img class="details__cover details__cover--md" style="align-self: center;" />
                                </a>
    
                                <!-- Infinity Knox Stats -->
                                <div class="details__stats">
    
                                    <!-- Infinity Knox Stats: Perks -->
                                    <div class="stats__perks details-box block">
                                        <h4>Specifications</h4>
                                        <table class="stats__perks-table"></table>
                                    </div>
    
                                </div>
    
                            </div>
                        </div>
                    </div>
    
                </section>

                <!-- POWER UP KNOX -->
                <section class="bootcamp-section" data-bc="power-up-knox">
    
                    <!-- Main Menu Pane -->
                    <div class="bootcamp-section__menu--wrapper">
                        <div class="bootcamp-section__menu">
    
                            <!-- POWER UP Knox Classes Menu -->
                            <div class="menu__nav-wrapper">
                            <nav class="menu__nav">
                                <!-- All, Quasar, Supernova, Star, Horizon, Comet -->
                                <div class="menu__item clickable menu__item--active" onclick="onCategoryClicked_pKnox('all', this)">
                                    <img class="menu__icon" src="img/favicon.png" />
                                    <div class="menu__text">All</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_pKnox('quasar', this)">
                                    <img class="menu__icon" src="img/icon/quasar1_p.png" />
                                    <div class="menu__text">Quasar</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_pKnox('supernova', this)">
                                    <img class="menu__icon" src="img/icon/supernova1_p.png" />
                                    <div class="menu__text">Supernova</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_pKnox('star', this)">
                                    <img class="menu__icon" src="img/icon/star1_p.png" />
                                    <div class="menu__text">Star</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_pKnox('horizon', this)">
                                    <img class="menu__icon" src="img/icon/horizon_p.png" />
                                    <div class="menu__text">Horizon</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_pKnox('comet', this)">
                                    <img class="menu__icon" src="img/icon/comet1_p.png" />
                                    <div class="menu__text">Comet</div>
                                </div>
                            </nav>
                            </div>
    
                            <!-- POWER UP Knox Cards -->
                            <div class="bootcamp-section__cards"></div>
    
                        </div>
                    </div>
    
                    <!-- POWER UP Knox Details Pane -->
                    <div class="bootcamp-section__details--wrapper">
                        <div class="bootcamp-section__details">

                            <!-- Top -->
                            <div class="bootcamp-section__details__top">

                                <!-- Back Button -->
                                <button class="details__back-btn" onclick="unshiftPane_pKnox()"><i class="fa fa-chevron-left"></i> <span class="details__back-btn-text">Forti Name</span></button>

                                <div class="bootcamp-section__details__top__right">

                                    <!-- Share Button -->
                                    <div class="bootcamp-section__share-btn" onclick="sharePKnox()"><i class="fa fa-share"></i><i class="fa fa-check"></i> <span>share</span></div>

                                </div>

                            </div>
                            
    
                            <div class="details__flex">
    
                                
                                <div class="details__left">

                                    <!-- POWER UP Knox Description -->
                                    <div class="details__description details-box">
                                        <h4>Effect</h4>
                                        <div class="details__description-text"></div>
                                    </div>

                                    <!-- Price -->
                                    <div class="details__price details__price--available details-box details-box--alt">
                                        <div class="details__price__amount"></div>
                                        <div class="details__price__note">Sold only in marketplace</div>
                                        <div class="details__price__available">Available at Marketplace</div>
                                    </div>

                                </div>
    
                                <!-- POWER UP Knox Cover -->
                                <a class="popup-image details__cover-wrapper img-loader">
                                    <img class="details__cover details__cover--md" style="align-self: center;" />
                                </a>
    
                                <!-- POWER UP Knox Stats -->
                                <div class="details__stats">
    
                                    <!-- POWER UP Knox Stats: Perks -->
                                    <div class="stats__perks details-box block">
                                        <h4>Specifications</h4>
                                        <table class="stats__perks-table"></table>
                                    </div>
    
                                </div>
    
                            </div>
                        </div>
                    </div>
    
                </section>

                <!-- RANKING -->
                <section class="bootcamp-section" data-bc="rankings">
    
                    <!-- Main Menu Pane -->
                    <div class="bootcamp-section__menu--wrapper">
                        <div class="bootcamp-section__menu">
    
                            <!-- Ranking Classes Menu -->
                            <div class="menu__nav-wrapper">
                            <nav class="menu__nav">
                                <!-- All, Army, Navy, Air Force, Marine Corps, Space Force, Coast Guard -->
                                <div class="menu__item clickable menu__item--active" onclick="onCategoryClicked_rankings('all', this)">
                                    <img class="menu__icon" src="img/icon/all-fortis.png" />
                                    <div class="menu__text">All</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_rankings('army', this)">
                                    <img class="menu__icon" src="img/icon/army.png" />
                                    <div class="menu__text">Army</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_rankings('marine_corps', this)">
                                    <img class="menu__icon" src="img/icon/marine-corps.png" />
                                    <div class="menu__text">Marine Corps</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_rankings('navy', this)">
                                    <img class="menu__icon" src="img/icon/navy.png" />
                                    <div class="menu__text">Navy</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_rankings('air_force', this)">
                                    <img class="menu__icon" src="img/icon/air-force.png" />
                                    <div class="menu__text">Air Force</div>
                                </div>
                                <div class="menu__item menu__item--disabled" title="Recruiting Soon">
                                    <img class="menu__icon" src="img/icon/space-force.png" />
                                    <div class="menu__text">Space Force</div>
                                </div>
                                <div class="menu__item menu__item--disabled" title="Recruiting Soon">
                                    <img class="menu__icon" src="img/icon/coast-guard.png" />
                                    <div class="menu__text">Coast Guard</div>
                                </div>
                            </nav>
                            </div>
    
                            <!-- Ranking Cards -->
                            <div class="bootcamp-section__cards"></div>
    
                        </div>
                    </div>
    
                    <!-- Ranking Details Pane -->
                    <div class="bootcamp-section__details--wrapper">
                        <div class="bootcamp-section__details">
    
                            <!-- Back Button -->
                            <button class="details__back-btn" onclick="unshiftPane_rankings()"><i class="fa fa-chevron-left"></i> <span class="details__back-btn-text">Forti Name</span></button>
    
                            <div class="details__flex" style="margin-top: 20px;">
    
                                <!-- Ranking Cover -->
                                <a class="popup-image details__cover-wrapper img-loader">
                                    <img class="details__cover details__cover--sm" style="align-self: center;" />
                                </a>
    
                                <!-- Ranking Stats -->
                                <div class="details__stats details__stats--wide">
    
                                    <!-- Ranking Stats: Perks -->
                                    <div class="stats__perks details-box block">
                                        <table class="stats__perks-table"></table>
                                        <br />
                                        <span>
                                            <span class="small-note">XP points are earned by in-game activity</span>
                                            <br />
                                            <a class="read-more-btn clickable white bold" onclick="onTabClicked(10, document.querySelector('.bootcamp-page__nav > div:nth-child(10)'))">Learn More <i class="fa fa-caret-right"></i></a>
                                        </span>
                                    </div>
    
                                </div>
    
                            </div>
                        </div>
                    </div>
    
                </section>

                <!-- PLAYER RANKINGS -->
                <section class="bootcamp-section" data-bc="player-rankings">
    
                    <!-- Main Menu Pane -->
                    <div class="bootcamp-section__menu--wrapper">
                        <div class="bootcamp-section__menu">
    
                            <!-- Player Rankings Menu -->
                            <div class="menu__nav-wrapper">
                            <nav class="menu__nav">
                                <!-- <div class="">
                                    <div class="menu__text">Player Rankings</div>
                                </div> -->
                                <div class="menu__item clickable" onclick="onCategoryClicked_pRankings(1, this)">
                                    <img class="menu__icon" src="img/ranks/profile-1.png" />
                                    <div class="menu__text">Private</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_pRankings(2, this)">
                                    <img class="menu__icon" src="img/ranks/profile-2.png" />
                                    <div class="menu__text">Corporal</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_pRankings(3, this)">
                                    <img class="menu__icon" src="img/ranks/profile-3.png" />
                                    <div class="menu__text">Seargeant</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_pRankings(4, this)">
                                    <img class="menu__icon" src="img/ranks/profile-4.png" />
                                    <div class="menu__text">Seargeant Major</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_pRankings(5, this)">
                                    <img class="menu__icon" src="img/ranks/profile-5.png" />
                                    <div class="menu__text">Second Lieutenant</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_pRankings(6, this)">
                                    <img class="menu__icon" src="img/ranks/profile-6.png" />
                                    <div class="menu__text">First Lieutenant</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_pRankings(7, this)">
                                    <img class="menu__icon" src="img/ranks/profile-7.png" />
                                    <div class="menu__text">Captain</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_pRankings(8, this)">
                                    <img class="menu__icon" src="img/ranks/profile-8.png" />
                                    <div class="menu__text">Major</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_pRankings(9, this)">
                                    <img class="menu__icon" src="img/ranks/profile-9.png" />
                                    <div class="menu__text">Major General</div>
                                </div>
                                <div class="menu__item clickable" onclick="onCategoryClicked_pRankings(10, this)">
                                    <img class="menu__icon" src="img/ranks/profile-10.png" />
                                    <div class="menu__text">General of the Army</div>
                                </div>
                            </nav>
                            </div>
    
                            <!-- Player Rankings Details -->
                            <div class="bootcamp-section__cards bootcamp-section__cards--pane">
                                <div class="bootcamp-section__details" style="height: 100%;">
                        
                                    <!-- Player Rankings Cover [Mobile Only] -->
                                    <a class="popup-image details__cover-wrapper details__cover-wrapper--mobile img-loader" separate-mobile>
                                        <img class="details__cover" />
                                    </a>

                                    <div class="details__flex-center">

                                        <div class="details__flex-pr details__flex-pr--align-center">


                                            <!-- Player Rankings Description -->
                                            <div class="details__description details-box">
                                                <h4>Profile Rankings <span class="details__description__profile-rank"></span></h4>
                                                <div class="details__description-text-wrapper">
                                                    <span class="details__description-text"></span>
                                                </div>
                                                <div class="details__description__icons"></div>
                                            </div>

                                            <!-- Player Rankings Cover [Desktop Only] -->
                                            <a class="popup-image details__cover-wrapper img-loader" separate-mobile>
                                                <img class="details__cover" />
                                            </a>

                                        </div>

                                        <div class="details__flex-pr">

                                            <!-- Starting Rank -->
                                            <div class="details__price details__price--available details-box details-box--alt" style="margin-top: 23px !important;">
                                                <div class="details__price__amount"></div>
                                                <div class="details__price__note"></div>
                                                <div class="details__price__available">Starting Rank</div>
                                            </div>

                                            <!-- Player Rankings Specs -->
                                            <div class="details__specs details-box">
                                                <h4>Rank Details</h4>
                                                <ul class="details__specs__list"></ul>
                                            </div>
                
                                        </div>

                                    </div>

                                </div>
                            </div>
    
                        </div>
                    </div>
    
                </section>

                <!-- Forti Skills -->
                <section class="bootcamp-section" data-bc="xp-points">
    
                    <!-- Main Pane -->
                    <div class="bootcamp-section__menu--wrapper">
                        <div class="bootcamp-section__menu min-width-75">

                            <div class="bootcamp-section__forti-skills">

                                <div class="row" style="margin: 0;">
                                    <div class="col-xl-5 col-lg-6 col-md-10">
                                        <div class="section-title title-style-three mb-70">
                                            <h2>RANK YOUR <span>FORTI'S</span></h2>
                                            <p>Your Forti's Hold special ability's, Power-ups effects and Rank's that will unlock the limits and power of each!</p>
                                        </div>
                                        <div class="soldier-mobile img-loader">
                                            <img loading="lazy" src="img/fortis/boomer.png" />
                                        </div>
                                        <div class="just-gamers-list">
                                            <ul>
                                                <li>
                                                    <div class="just-gamers-list-icon">
                                                        <img loading="lazy" src="img/icon/skills.png" alt="">
                                                    </div>
                                                    <div class="just-gamers-list-content fix">
                                                        <h5>Skills</h5>
                                                        <p>Fortis earn skills by fighting in the warzone. Each skill is equal 200 collected experience points. For more information about points allocation, please visit here (scrolls to XP Points)</p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="just-gamers-list-icon">
                                                        <img loading="lazy" src="img/icon/extendable.png" alt="">
                                                    </div>
                                                    <div class="just-gamers-list-content fix">
                                                        <h5>Multiplier</h5>
                                                        <p>Based on the action and performance you do in-game, your multiplier will be affected, specially with the acquiring of an Infinity Knox. Multipliers allow you to rank faster.</p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="just-gamers-list-icon">
                                                        <img loading="lazy" src="img/icon/premium-quality.png" alt="">
                                                    </div>
                                                    <div class="just-gamers-list-content fix">
                                                        <h5>Collected XP</h5>
                                                        <p>Each Forti rank consists of 20 total skill points based on the rarity of the Forti. Reaching 20 skill points is equal to 4000 XP points which will unlock the next rank and reset your counter.</p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xl-7 col-lg-6 d-none d-lg-block">
                                        <div class="just-gamers-img img-loader">
                                            <img loading="lazy" src="img/fortis/boomer.png" alt="" class="just-gamers-character">
                                            <div class="just-gamers-circle-shape">
                                                <img loading="lazy" src="img/images/gamers_circle_line.png" alt="">
                                                <img loading="lazy" src="img/images/gamers_circle_shape.png" alt="" class="rotateme">
                                                <img loading="lazy" src="img/icon/skill.png" alt="">
                                            </div>
                                            <img loading="lazy" src="img/images/just_gamers_chart.png" alt="" class="gamers-chart-shape">
                                            <div class="section-title title-style-three mb-70 gamers-bottom-right-title">
                                                <h2>PLAY & <span>RANK</span></h2>
                                                <p>Earn skill points by collecting experience in the warzone!</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
    
                </section>

                <!-- P2E -->
                <section class="bootcamp-section" data-bc="p2e">
    
                    <!-- Main Pane -->
                    <div class="bootcamp-section__menu--wrapper">
                        <div class="bootcamp-section__menu min-width-75">

                            <div class="bootcamp-section__forti-skills">

                                <div class="row" style="margin: 0;">
                                    <div class="col-xl-5 col-lg-6 col-md-10">
                                        <div class="section-title title-style-three mb-70">
                                            <h2>RANK & EARN <span>BFK</span></h2>
                                            <p>Your Fortis earn while fighting. Unlock ranks and get calculated air drops based on your efforts with your BFK profile.</p>
                                        </div>
                                        <div class="soldier-mobile img-loader">
                                            <img loading="lazy" src="img/images/p2e-forti.png" />
                                        </div>
                                        <div class="just-gamers-list">
                                            <ul>
                                                <li>
                                                    <div class="just-gamers-list-icon">
                                                        <img loading="lazy" src="img/icon/earn-money.png" alt="">
                                                    </div>
                                                    <div class="just-gamers-list-content fix">
                                                        <h5>Earn</h5>
                                                        <p>Fortis earn Points by fighting in the warzone. Each Kill will raise your point allocation and XP, you can raise your points to unlock a new rank and earn BFK airdrops as you go! Kills, Assists, and Marketplace activities will help you rank and allocate your BFK earnings for the next rank.</p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="just-gamers-list-icon">
                                                        <img loading="lazy" src="img/icon/packing.png" alt="">
                                                    </div>
                                                    <div class="just-gamers-list-content fix">
                                                        <h5>Collect</h5>
                                                        <p>Collecting Points in your profile by doing Purchases, Auctions, and Sales will help you earn more Profile points to help you rank faster, ranking is affected by Forti’s battle efforts, and your actions in the marketplace help you collect more points to get your airdrop faster on the next rank.</p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="just-gamers-list-icon">
                                                        <img loading="lazy" src="img/icon/airdrop.png" alt="">
                                                    </div>
                                                    <div class="just-gamers-list-content fix">
                                                        <h5>Airdrop</h5>
                                                        <p>On each Rank Unlock, you will receive Airdrop based on the Rank you are moving to, Airdrops are automated and will appear in your active wallet after some time related to the network processing on the blockchain, network fees are covered by BFK so no worries your earning will arrive intact.</p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xl-7 col-lg-6 d-none d-lg-block">
                                        <div class="just-gamers-img img-loader">
                                            <img loading="lazy" src="img/images/p2e-forti.png" alt="" class="just-gamers-character">
                                            <div class="just-gamers-circle-shape">
                                                <img loading="lazy" src="img/images/gamers_circle_line.png" alt="">
                                                <img loading="lazy" src="img/images/gamers_circle_shape.png" alt="" class="rotateme">
                                                <img loading="lazy" src="img/icon/earn_pnf2.png" class="p2e-badge" alt="">
                                                <div class="p2e-counter">
                                                    Up to <span id="p2eCountUpWrapper"><span id="p2eCountUp">0</span> BFK</span> per Rank
                                                </div>
                                            </div>
                                            <img loading="lazy" src="img/images/just_gamers_chart.png" alt="" class="gamers-chart-shape">
                                            <div class="section-title title-style-three mb-70 gamers-bottom-right-title">
                                                <h2>PLAY & <span>EARN</span></h2>
                                                <p>Earn BFK by unlocking ranks with your profile!</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
    
                </section>

                <!-- XP Points -->
                <section class="bootcamp-section" data-bc="xp-points">
    
                    <!-- Main Pane -->
                    <div class="bootcamp-section__menu--wrapper">
                        <div class="bootcamp-section__menu">
                            <div class="bootcamp-section__xp">
                                <div class="xp__title">Experience & Points Distribution System</div>
                                <div class="xp__table-wrapper">
                                    <i class="xp__table-wrapper-icon fa fa-caret-down"></i>
                                    <table class="xp__table">
                                        <tr>
                                            <td>Buying 1 NFT</td>
                                            <td>25 Points (25 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Selling 1 NFT</td>
                                            <td>25 Points (25 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Buying Weapon</td>
                                            <td>25 Points (25 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Selling Weapon</td>
                                            <td>25 Points (25 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Leveling up Forti</td>
                                            <td>50 Points (50 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Event Win</td>
                                            <td>100 Points (100 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Event Lose</td>
                                            <td>50 Points (50 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Ranked Game Join</td>
                                            <td>10 Points (10 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Ranked Game 1 v 1 Win</td>
                                            <td>50 Points (50 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Ranked Game 1 v 1 Lose</td>
                                            <td>10 Points (10 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Free for All or Deathmatch Join</td>
                                            <td>5 Points (5 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Infinity Cards Holding</td>
                                            <td>1 Point per day while holding only (1 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Forti Kill</td>
                                            <td>5 Points (5 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Coop Game (2 v 2) on join</td>
                                            <td>10 Points (10 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Coop Game (3 v 3) on join</td>
                                            <td>15 Points (15 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Coop Game (4 v 4) on join</td>
                                            <td>20 Points (20 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Coop Game (5 v 5) on join</td>
                                            <td>25 Points (25 XP)</td>
                                        </tr>
                                        <tr>
                                            <td>Auctions</td>
                                            <td>10 Points (10 XP)</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
    
                </section>

            </div>
            </div>

            <!-- Forti Description Read More Popup -->
            <div class="fortiDescriptionReadMorePopup">
                <div class="fortiDescriptionReadMorePopup__relative-wrapper">
                    <div class="fortiDescriptionReadMorePopup__overlay" onclick="closeFortiDescriptionPopup()"></div>
                    <div class="fortiDescriptionReadMorePopup__container">
                        <div class="fortiDescriptionReadMorePopup__close">
                            <i class="fortiDescriptionReadMorePopup__close-btn fa fa-times" onclick="closeFortiDescriptionPopup()"></i>
                        </div>
                        <p class="fortiDescriptionReadMorePopup__description">asfasfasfasf</p>
                    </div>
                </div>
            </div>

            <!-- Forti Supply SVG Popup -->
            <div class="svg-popup" id="fortiSupplyPopup">
                <div class="svg-popup__relative-wrapper">
                    <div class="svg-popup__overlay" onclick="closeSVGPopup()"></div>
                    <div class="svg-popup__container">
                        <div class="svg-popup__close">
                            <a class="download-btn" href="files/supplyfortis.pdf" target="_blank" download>download pdf</a>
                            <i class="svg-popup__close-btn fa fa-times" onclick="closeSVGPopup()"></i>
                        </div>
                        <?php include 'img/svg/supplyfortis.svg'; ?>
                    </div>
                </div>
            </div>

            <!-- Ranking System SVG Popup -->
            <div class="svg-popup" id="rankingSystemPopup">
                <div class="svg-popup__relative-wrapper">
                    <div class="svg-popup__overlay" onclick="closeSVGPopup()"></div>
                    <div class="svg-popup__container">
                        <div class="svg-popup__close">
                            <a class="download-btn" href="files/rankingsystem.pdf" target="_blank" download>download pdf</a>
                            <i class="svg-popup__close-btn fa fa-times" onclick="closeSVGPopup()"></i>
                        </div>
                        <?php include 'img/svg/rankingsystem.svg'; ?>
                    </div>
                </div>
            </div>

            <!-- Shards SVG Popup -->
            <div class="svg-popup" id="shardsPopup">
                <div class="svg-popup__relative-wrapper">
                    <div class="svg-popup__overlay" onclick="closeSVGPopup()"></div>
                    <div class="svg-popup__container">
                        <div class="svg-popup__close">
                            <a class="download-btn" href="files/Shards.pdf" target="_blank" download>download pdf</a>
                            <i class="svg-popup__close-btn fa fa-times" onclick="closeSVGPopup()"></i>
                        </div>
                        <?php include 'img/svg/Shards.svg'; ?>
                    </div>
                </div>
            </div>

            <!-- Blockchain ECO System SVG Popup -->
            <div class="svg-popup" id="ecoSystemPopup">
                <div class="svg-popup__relative-wrapper">
                    <div class="svg-popup__overlay" onclick="closeSVGPopup()"></div>
                    <div class="svg-popup__container">
                        <div class="svg-popup__close">
                            <a class="download-btn" href="files/Ecosystem.pdf" target="_blank" download>download pdf</a>
                            <i class="svg-popup__close-btn fa fa-times" onclick="closeSVGPopup()"></i>
                        </div>
                        <?php include 'img/svg/Ecosystem.svg'; ?>
                    </div>
                </div>
            </div>


        </main>
        <!-- main-area-end -->

        <!-- Footer -->
       
        <!-- Scripts -->
        <?php include '_scripts.php'; ?>
        
    </body>
</html>

<script>

    /**
     * Table of Content
     * 1. State
     * 2. Fixed Configs
     * 3. DOM References
     * 4. Helper Methods
     * 5. Navigation
     * 6. Init
     */

    /********************************** */
    /************* 1. State *********** */
    /********************************** */
    let selectedForti = null;
    let selectedAlien = null;
    let selectedWeapon = null;
    let selectedIKnox = null;
    let selectedPKnox = null;

    /********************************** */
    /********** 2. FIXED CONFIG ******* */
    /********************************** */
    const TABS_COUNT = 10;
    const DEFAULT_PRICE_NOTE = "Will be transferred for the value of BFK";
    const DEFAULT_FORTI_VIDEO_HREF = "img/videos/armis.mp4";
    const DEFAULT_ALIEN_VIDEO_HREF = "img/videos/armis.mp4";
    const P2E_COUNT_UP_TO = 5000;
    const P2E_COUNT_UP_DURATION_MS = 3000;
    const P2E_COUNT_UP_RATE_MS = 20;

    /********************************** */
    /********* 3. DOM References ****** */
    /********************************** */
    const mainDOM                               = document.querySelector('main');
    const mainNav                               = mainDOM.querySelector('.bootcamp-page__nav');
    const bootcampRoller                        = document.querySelector('.bootcamp-sections');
    const fortiSupplyPopup                      = document.querySelector('#fortiSupplyPopup');
    const rankingSystemPopup                    = document.querySelector('#rankingSystemPopup');
    const shardsPopup                           = document.querySelector('#shardsPopup');
    const ecoSystemPopup                        = document.querySelector('#ecoSystemPopup');    
    const p2eCountUp                            = document.querySelector('#p2eCountUp');

    // Bootcamps
    const bootcamp_fortis                       = document.querySelector('.bootcamp-section[data-bc="fortis"]');
    const bootcamp_aliens                       = document.querySelector('.bootcamp-section[data-bc="aliens"]');
    const bootcamp_weapons                      = document.querySelector('.bootcamp-section[data-bc="weapons"]');
    const bootcamp_iKnox                        = document.querySelector('.bootcamp-section[data-bc="infinity-knox"]');
    const bootcamp_pKnox                        = document.querySelector('.bootcamp-section[data-bc="power-up-knox"]');
    const bootcamp_rankings                     = document.querySelector('.bootcamp-section[data-bc="rankings"]');
    const bootcamp_pRankings                    = document.querySelector('.bootcamp-section[data-bc="player-rankings"]');

    // Menu Pane DOM
    const categorySelector_fortis               = bootcamp_fortis.querySelector('.bootcamp-section__menu .menu__nav');
    const cards_fortis                          = bootcamp_fortis.querySelector('.bootcamp-section__cards');
    const categorySelector_aliens               = bootcamp_aliens.querySelector('.bootcamp-section__menu .menu__nav');
    const cards_aliens                          = bootcamp_aliens.querySelector('.bootcamp-section__cards');
    const categorySelector_weapons              = bootcamp_weapons.querySelector('.bootcamp-section__menu .menu__nav');
    const cards_weapons                         = bootcamp_weapons.querySelector('.bootcamp-section__cards');
    const categorySelector_iKnox                = bootcamp_iKnox.querySelector('.bootcamp-section__menu .menu__nav');
    const cards_iKnox                           = bootcamp_iKnox.querySelector('.bootcamp-section__cards');
    const categorySelector_pKnox                = bootcamp_pKnox.querySelector('.bootcamp-section__menu .menu__nav');
    const cards_pKnox                           = bootcamp_pKnox.querySelector('.bootcamp-section__cards');
    const categorySelector_rankings             = bootcamp_rankings.querySelector('.bootcamp-section__menu .menu__nav');
    const cards_rankings                        = bootcamp_rankings.querySelector('.bootcamp-section__cards');
    const categorySelector_pRankings            = bootcamp_pRankings.querySelector('.bootcamp-section__menu .menu__nav');
    const cards_pRankings                       = bootcamp_pRankings.querySelector('.bootcamp-section__cards');

    // Details Pane DOM
    const backBtnText_fortis                          = bootcamp_fortis.querySelector('.details__back-btn-text');
    const description_fortis                          = bootcamp_fortis.querySelector('.details__description-text');
    const descriptionIcons_fortis                     = bootcamp_fortis.querySelector('.details__description__icons');
    const price_fortis                                = bootcamp_fortis.querySelector('.details__price__amount');
    const priceNote_fortis                            = bootcamp_fortis.querySelector('.details__price__note');
    const cover_fortis                                = bootcamp_fortis.querySelector('.details__cover');
    const coverWrapper_fortis                         = bootcamp_fortis.querySelector('.details__cover-wrapper');
    const perks_fortis                                = bootcamp_fortis.querySelector('.stats__perks');
    const [weapon1, weapon2]                          = bootcamp_fortis.querySelectorAll('.stats__weapon');
    const weapon1Name                                 = weapon1.querySelector('.stats__weapon-name');
    const weapon1Image                                = weapon1.querySelector('.stats__weapon-img');
    const weapon2Name                                 = weapon2.querySelector('.stats__weapon-name');
    const weapon2Image                                = weapon2.querySelector('.stats__weapon-img');
    const fortiAbility                                = bootcamp_fortis.querySelector('#fortiAbility');
    const fortiRarity                                 = bootcamp_fortis.querySelector('.details__specs__rarity-label');
    const fortiSpecsList                              = bootcamp_fortis.querySelector('.details__specs__list');
    const fortiDescriptionPopup                       = document.querySelector('.fortiDescriptionReadMorePopup');
    const fortiDescriptionPopupText                   = document.querySelector('.fortiDescriptionReadMorePopup__description');
    const fortiShareBtn                               = bootcamp_fortis.querySelector('.bootcamp-section__share-forti');
    const fortiVideoBtn                               = bootcamp_fortis.querySelector('.bootcamp-section__forti-video-btn');
    const fortiStatTabs                               = bootcamp_fortis.querySelectorAll('[data-stat-tab]');
    const fortiStatPanes                              = bootcamp_fortis.querySelectorAll('[data-stat]');

    const backBtnText_aliens                          = bootcamp_aliens.querySelector('.details__back-btn-text');
    const description_aliens                          = bootcamp_aliens.querySelector('.details__description-text');
    const descriptionIcons_aliens                     = bootcamp_aliens.querySelector('.details__description__icons');
    const price_aliens                                = bootcamp_aliens.querySelector('.details__price__amount');
    const priceNote_aliens                            = bootcamp_aliens.querySelector('.details__price__note');
    const cover_aliens                                = bootcamp_aliens.querySelector('.details__cover');
    const coverWrapper_aliens                         = bootcamp_aliens.querySelector('.details__cover-wrapper');
    const perks_aliens                                = bootcamp_aliens.querySelector('.stats__perks');
    const [weapon1A, weapon2A]                        = bootcamp_aliens.querySelectorAll('.stats__weapon');
    const weapon1AName                                = weapon1A.querySelector('.stats__weapon-name');
    const weapon1AImage                               = weapon1A.querySelector('.stats__weapon-img');
    const weapon2AName                                = weapon2A.querySelector('.stats__weapon-name');
    const weapon2AImage                               = weapon2A.querySelector('.stats__weapon-img');
    const alienAbility                                = bootcamp_aliens.querySelector('#alienAbility');
    const alienRarity                                 = bootcamp_aliens.querySelector('.details__specs__rarity-label');
    const alienSpecsList                              = bootcamp_aliens.querySelector('.details__specs__list');
    const alienShareBtn                               = bootcamp_aliens.querySelector('.bootcamp-section__share-alien');
    const alienVideoBtn                               = bootcamp_aliens.querySelector('.bootcamp-section__alien-video-btn');
 
    const backBtnText_weapons                         = bootcamp_weapons.querySelector('.details__back-btn-text');
    const description_weapons                         = bootcamp_weapons.querySelector('.details__description-text');
    const efficiency_weapons                          = bootcamp_weapons.querySelector('.details__efficiency-text');
    const price_weapons                               = bootcamp_weapons.querySelector('.details__price__amount');
    const cover_weapons                               = bootcamp_weapons.querySelector('.details__cover');
    const coverWrapper_weapons                        = bootcamp_weapons.querySelector('.details__cover-wrapper');
    const perks_weapons                               = bootcamp_weapons.querySelector('.stats__perks-table');
    const labels_weapons                              = bootcamp_weapons.querySelector('.bootcamp-section__details__top__labels');
 
    const backBtnText_iKnox                           = bootcamp_iKnox.querySelector('.details__back-btn-text');
    const description_iKnox                           = bootcamp_iKnox.querySelector('.details__description-text');
    const price_iKnox                                 = bootcamp_iKnox.querySelector('.details__price__amount');
    const priceNote_iKnox                             = bootcamp_iKnox.querySelector('.details__price__note');
    const cover_iKnox                                 = bootcamp_iKnox.querySelector('.details__cover');
    const coverWrapper_iKnox                          = bootcamp_iKnox.querySelector('.details__cover-wrapper');
    const perks_iKnox                                 = bootcamp_iKnox.querySelector('.stats__perks-table');
    const videoBtn_iKnox                              = bootcamp_iKnox.querySelector('.bootcamp-section__iKnox-video-btn');
    const shareBtn_iKnox                              = bootcamp_iKnox.querySelector('.bootcamp-section__share-btn');
 
    const backBtnText_pKnox                           = bootcamp_pKnox.querySelector('.details__back-btn-text');
    const description_pKnox                           = bootcamp_pKnox.querySelector('.details__description-text');
    const price_pKnox                                 = bootcamp_pKnox.querySelector('.details__price__amount');
    const cover_pKnox                                 = bootcamp_pKnox.querySelector('.details__cover');
    const coverWrapper_pKnox                          = bootcamp_pKnox.querySelector('.details__cover-wrapper');
    const perks_pKnox                                 = bootcamp_pKnox.querySelector('.stats__perks-table');
    const shareBtn_pKnox                              = bootcamp_pKnox.querySelector('.bootcamp-section__share-btn');
 
    const backBtnText_rankings                        = bootcamp_rankings.querySelector('.details__back-btn-text');
    const description_rankings                        = bootcamp_rankings.querySelector('.details__description-text');
    const cover_rankings                              = bootcamp_rankings.querySelector('.details__cover');
    const coverWrapper_rankings                       = bootcamp_rankings.querySelector('.details__cover-wrapper');
    const perks_rankings                              = bootcamp_rankings.querySelector('.stats__perks-table');
 
    const description_pRankings                          = bootcamp_pRankings.querySelector('.details__description-text');
    const descriptionIcons_pRankings                     = bootcamp_pRankings.querySelector('.details__description__icons');
    const profileRank__pRankings                         = bootcamp_pRankings.querySelector('.details__description__profile-rank');
    const price_pRankings                                = bootcamp_pRankings.querySelector('.details__price__amount');
    const priceNote_pRankings                            = bootcamp_pRankings.querySelector('.details__price__note');
    const coverWrapper_pRankings                         = bootcamp_pRankings.querySelector('.details__cover-wrapper:not(.details__cover-wrapper--mobile)');
    const cover_pRankings                                = coverWrapper_pRankings.querySelector('.details__cover');
    const coverWrapperM_pRankings                        = bootcamp_pRankings.querySelector('.details__cover-wrapper--mobile');
    const coverM_pRankings                               = coverWrapperM_pRankings.querySelector('.details__cover-wrapper--mobile .details__cover');
    const specsList__pRankings                           = bootcamp_pRankings.querySelector('.details__specs__list');

    /********************************** */
    /********* 4. Helper Methods ****** */
    /********************************** */

    /**
     * Fix the bootcamp's height (when page is resized especially)
     */
    function fixBootcampHeight() {
        const headerHeight = document.querySelector('header').clientHeight;
        const bootcampHeight = window.innerHeight - headerHeight;
        mainDOM.style.height = `${bootcampHeight}px`;
        console.log('Fixed bootcamp height to become: ' + bootcampHeight);
    }

    /**
     * Init the Image Popup plugin, used after adding an image dynamically to the DOM
     */
    function initImagePopups() {
        document.querySelectorAll('.popup-image').forEach(popupImage => {
            $(popupImage).magnificPopup({
                type: 'image',
                gallery: {
                    enabled: false
                },
                zoom: {
                    enabled: true
                }
            });
        });
    }

    /**
     * Get the icon of the forti's faction
     */
    function getFortiFactionIcon(category) {
        switch(category) {
            case 'army':
                return 'img/icon/army.png';
            case 'navy':
                return 'img/icon/navy.png';
            case 'air_force':
                return 'img/icon/air-force.png';
            case 'marine_corps':
                return 'img/icon/marine-corps.png';
            case 'space_force':
                return 'img/icon/space-force.png';
            case 'coast_guard':
                return 'img/icon/coast-guard.png';
            default:
                return 'img/icon/all-fortis.png';
        }
    }

    /**
     * Get the icon of the alien's faction
     */
    function getAlienFactionIcon(category) {
        switch(category) {
            case 'alien_chain':
                return 'img/icon/alien-chain.png';
            default:
                return 'img/icon/all-fortis.png';
        }
    }

    /**
     * Get the icon of a perk based on its name
     */
    function getPerkIcon(name) {
        switch(name) {
            case "DPR": return "img/icon/pw1.png";
            case "HP": return "img/icon/pw2.png";
            case "Armor": return "img/icon/pw3.png";
            case "Bullet Speed": return "img/icon/pw5.png";
            case "Fire Rate": return "img/icon/pw4.png";
            case "Skill": return "img/icon/pw6.png";
            case "Level": return "img/icon/pw6.png";
        }
    }

    /**
     * handle when a forti weapon is clicked inside the Forti Details Pane
     */
    function onFortiWeaponClicked(weaponIndex) {

        // Get selected weapon stats
        const selectedWeapon = selectedForti.stats[weaponIndex];

        // Set selected weapon class
        if(weaponIndex == 0) {
            weapon1.classList.add('stats__weapon--selected');
            weapon2.classList.remove('stats__weapon--selected');
        }
        else {
            weapon1.classList.remove('stats__weapon--selected');
            weapon2.classList.add('stats__weapon--selected');
        }

        // Show respective Forti cover and weapon stats
        cover_fortis.style.opacity = 0;
        perks_fortis.style.opacity = 0;
        setTimeout(() => {

            // Show correct cover image
            cover_fortis.src = '';
            if(weaponIndex == 0) {
                cover_fortis.src = selectedForti.cover;
                coverWrapper_fortis.href = selectedForti.cover;
            }
            else {
                cover_fortis.src = selectedForti.cover2;
                coverWrapper_fortis.href = selectedForti.cover2;
            }

            // Show selected weapon stats
            perks_fortis.innerHTML = selectedWeapon.map(perk => `
                <div class="stats__perk">
                    <span class="stats__perk-title">${perk.title}</span>
                    <div class="stats__perk-flex">
                        <div class="stats__perk-icon-wrapper">
                            <img class="stats__perk-icon" src="${getPerkIcon(perk.title)}" />
                        </div>
                        <div class="stats__perk-value">${perk.value}</div>
                    </div>
                </div>
            `).join('');

            // Set component opacities to 100%
            setTimeout(() => {
                cover_fortis.style.opacity = 1;
                perks_fortis.style.opacity = 1;
            }, 100)
        }, 300)

    }

    /**
     * handle when an alien weapon is clicked inside the Alien Details Pane
     */
    function onAlienWeaponClicked(weaponIndex) {

        // Get selected weapon stats
        const selectedWeapon = selectedAlien.stats[weaponIndex];

        // Set selected weapon class
        if(weaponIndex == 0) {
            weapon1A.classList.add('stats__weapon--selected');
            weapon2A.classList.remove('stats__weapon--selected');
        }
        else {
            weapon1A.classList.remove('stats__weapon--selected');
            weapon2A.classList.add('stats__weapon--selected');
        }

        // Show respective Forti cover and weapon stats
        cover_aliens.style.opacity = 0;
        perks_aliens.style.opacity = 0;
        setTimeout(() => {

            // Show correct cover image
            cover_aliens.src = '';
            if(weaponIndex == 0) {
                cover_aliens.src = selectedAlien.cover;
                coverWrapper_aliens.href = selectedAlien.cover;
            }
            else {
                cover_aliens.src = selectedAlien.cover2;
                coverWrapper_aliens.href = selectedAlien.cover2;
            }

            // Show selected weapon stats
            perks_aliens.innerHTML = selectedWeapon.map(perk => `
                <div class="stats__perk">
                    <span class="stats__perk-title">${perk.title}</span>
                    <div class="stats__perk-flex">
                        <div class="stats__perk-icon-wrapper">
                            <img class="stats__perk-icon" src="${getPerkIcon(perk.title)}" />
                        </div>
                        <div class="stats__perk-value ${perk.value.length > 5 && perk.value.length < 10 ? 'stats__perk-value--sm' : perk.value.length >= 10 ? 'stats__perk-value--xsm' : ''}">${perk.value}</div>
                    </div>
                </div>
            `).join('');

            // Set component opacities to 100%
            setTimeout(() => {
                cover_aliens.style.opacity = 1;
                perks_aliens.style.opacity = 1;
            }, 100)
        }, 300)

    }

    /**
     * open the forti's "read more" popup when clicked inside the Forti Details Pane
     */
    function openFortiDescriptionPopup() {
        fortiDescriptionPopup.classList.add('fortiDescriptionPopup--open');
    }

    /**
     * close the forti's "read more" popup
     */
    function closeFortiDescriptionPopup() {
        fortiDescriptionPopup.classList.remove('fortiDescriptionPopup--open');
    }

    /**
     * open the popup that shows the forti supply diagram (svg)
     */
    function openFortiSupplyPopup() {
        fortiSupplyPopup.classList.add('svg-popup--open');
    }

    /**
     * open the popup that shows the ranking system diagram (svg)
     */
    function openRankingSystemPopup() {
        rankingSystemPopup.classList.add('svg-popup--open');
    }

    /**
     * open the popup that shows the Shards diagram (svg)
     */
    function openShardsPopup() {
        shardsPopup.classList.add('svg-popup--open');
    }

    /**
     * open the popup that shows the Blockchain ECO System diagram (svg)
     */
    function openECOSystemPopup() {
        ecoSystemPopup.classList.add('svg-popup--open');
    }    

    /**
     * close the diagram popups ("forti supply" and "ranking system")
     */
    function closeSVGPopup() {
        fortiSupplyPopup.classList.remove('svg-popup--open');
        rankingSystemPopup.classList.remove('svg-popup--open');
        shardsPopup.classList.remove('svg-popup--open');
        ecoSystemPopup.classList.remove('svg-popup--open');
    }
    
    /**
     * get power up knox's letter color based on the first letter of its name
     */
    function getPKnoxColor(cardName) {
        const cardLetter = cardName.substr(0, 1);
        switch(cardLetter) {
            case 'H': return 'legacy';
            case 'G': return 'rare';
            case 'M': return 'ultra-rare';
            case 'X': return 'ultimate';
            case 'I': return 'supreme-rare';
            default: return 'legacy';
        }
    }

    /**
     * copy the share link of the selected forti into clipboard
     */
    function shareForti() {
        const fortiId = selectedForti?.id || null;
        if(!fortiId) return;
        copyToClipboard(`https://warzone.bfkwarzone.com/bootcamp.php?fortiId=${fortiId}`);
        informFortiShared();
    }

    /**
     * update the share forti button after click
     */
    function informFortiShared() {
        fortiShareBtn.querySelector('span').innerHTML = `link copied!`;
        fortiShareBtn.classList.add('bootcamp-section__share-forti--shared');
    }

    /**
     * reset the share forti button
     */
    function resetFortiShareBtn() {
        fortiShareBtn.querySelector('span').innerHTML = `share`;
        fortiShareBtn.classList.remove('bootcamp-section__share-forti--shared');
    }

    /**
     * copy the share link of the selected alien into clipboard
     */
    function shareAlien() {
        const alienId = selectedAlien?.id || null;
        if(!alienId) return;
        copyToClipboard(`https://warzone.bfkwarzone.com/bootcamp.php?alienId=${alienId}`);
        informAlienShared();
    }

    /**
     * update the share alien button after click
     */
    function informAlienShared() {
        alienShareBtn.querySelector('span').innerHTML = `link copied!`;
        alienShareBtn.classList.add('bootcamp-section__share-alien--shared');
    }

    /**
     * reset the share alien button
     */
    function resetAlienShareBtn() {
        alienShareBtn.querySelector('span').innerHTML = `share`;
        alienShareBtn.classList.remove('bootcamp-section__share-alien--shared');
    }

    /**
     * copy the share link of the selected infinity knox into clipboard
     */
    function shareIKnox() {
        const iKnoxId = selectedIKnox?.id || null;
        if(!iKnoxId) return;
        copyToClipboard(`https://warzone.bfkwarzone.com/bootcamp.php?iKnoxId=${iKnoxId}`);
        informIKnoxShared();
    }

    /**
     * update the share infinity knox button after click
     */
    function informIKnoxShared() {
        shareBtn_iKnox.querySelector('span').innerHTML = `link copied!`;
        shareBtn_iKnox.classList.add('bootcamp-section__share-btn--shared');
    }

    /**
     * reset the share infinity button
     */
    function resetIKnoxShareBtn() {
        shareBtn_iKnox.querySelector('span').innerHTML = `share`;
        shareBtn_iKnox.classList.remove('bootcamp-section__share-btn--shared');
    }

    /**
     * copy the share link of the selected power up knox into clipboard
     */
    function sharePKnox() {
        const pKnoxId = selectedPKnox?.id || null;
        if(!pKnoxId) return;
        copyToClipboard(`https://warzone.bfkwarzone.com/bootcamp.php?pKnoxId=${pKnoxId}`);
        informPKnoxShared();
    }

    /**
     * update the share power up knox button after click
     */
    function informPKnoxShared() {
        shareBtn_pKnox.querySelector('span').innerHTML = `link copied!`;
        shareBtn_pKnox.classList.add('bootcamp-section__share-btn--shared');
    }

    /**
     * reset the share power up knox button
     */
    function resetPKnoxShareBtn() {
        shareBtn_pKnox.querySelector('span').innerHTML = `share`;
        shareBtn_pKnox.classList.remove('bootcamp-section__share-btn--shared');
    }

    /**
     * start the P2E count-up
     * Example of Logic:
     *      If the number to count to is 10,000, and counter update rate is 2s (2000ms), and total animation duration is 10s (1000ms)
     *      Then the incrementPerInterval (amount to increment the number at each refresh rate) is (10000 / (10 / 2)) = 2000
     */
    function startP2ECountUp() {

        // Calculate the amount to increment on each interval
        const incrementPerInterval = Math.floor(P2E_COUNT_UP_TO / (P2E_COUNT_UP_DURATION_MS / (P2E_COUNT_UP_RATE_MS)));

        let i = 0;
        const intervalDebounce = setInterval(() => {

            // Increment
            i += incrementPerInterval;
            p2eCountUp.innerHTML = i;

            // If desired number reached, correct the number and stop the interval
            if(i >= P2E_COUNT_UP_TO) {
                p2eCountUp.innerHTML = P2E_COUNT_UP_TO;
                clearInterval(intervalDebounce);
            }

        }, P2E_COUNT_UP_RATE_MS);
    }

    /**
     * switch the selected weapons's skins in the weapons details pane if it has any
     */
    function switchWeaponSkin() {

        // ! Checks
        if(!selectedWeapon) return;
        if(!selectedWeapon.skin) return;

        // Switch & Update State
        if(selectedWeapon.skinShown) {
            cover_weapons.src = selectedWeapon.coverHD;
            coverWrapper_weapons.href = selectedWeapon.coverHD;
            selectedWeapon.skinShown = false;
        }
        else {
            cover_weapons.src = selectedWeapon.skin;
            coverWrapper_weapons.href = selectedWeapon.skin;
            selectedWeapon.skinShown = true;
        }

        // Re-Init Image Loaders
        initImageLoaders();

    }

    /******************************************** */
    /*************** 5. Navigation ************** */
    /******************************************** */
    // The shift functions are responsible for the horizontal shift of the layout when a card is clicked
    function shiftPane_fortis() {
        bootcamp_fortis.classList.add('bootcamp-section--shift');
        resetFortiShareBtn();
    }
    function unshiftPane_fortis() {
        bootcamp_fortis.classList.remove('bootcamp-section--shift');
        resetFortiShareBtn();
    }
    function shiftPane_aliens() {
        bootcamp_aliens.classList.add('bootcamp-section--shift');
        resetAlienShareBtn();
    }
    function unshiftPane_aliens() {
        bootcamp_aliens.classList.remove('bootcamp-section--shift');
        resetAlienShareBtn();
    }
    function shiftPane_weapons() {
        bootcamp_weapons.classList.add('bootcamp-section--shift');
    }
    function unshiftPane_weapons() {
        bootcamp_weapons.classList.remove('bootcamp-section--shift');
    }
    function shiftPane_iKnox() {
        bootcamp_iKnox.classList.add('bootcamp-section--shift');
        resetIKnoxShareBtn();
    }
    function unshiftPane_iKnox() {
        bootcamp_iKnox.classList.remove('bootcamp-section--shift');
        resetIKnoxShareBtn();
    }
    function shiftPane_pKnox() {
        bootcamp_pKnox.classList.add('bootcamp-section--shift');
        resetPKnoxShareBtn();
    }
    function unshiftPane_pKnox() {
        bootcamp_pKnox.classList.remove('bootcamp-section--shift');
        resetPKnoxShareBtn();
    }
    function shiftPane_rankings() {
        bootcamp_rankings.classList.add('bootcamp-section--shift');
    }
    function unshiftPane_rankings() {
        bootcamp_rankings.classList.remove('bootcamp-section--shift');
    }
    function unshiftPane_all() {
        unshiftPane_fortis();
        unshiftPane_aliens();
        unshiftPane_weapons();
        unshiftPane_iKnox();
        unshiftPane_pKnox();
        unshiftPane_rankings();
    }
    
    /**
     * select the first category of a section (eg "All Fortis" in the fortis section)
     */
    function selectFirstCategoryOfSection(sectionIndex) {
        const selectFirstCategoriesFns = [
            // 1. Fortis
            () => onCategoryClicked_fortis('all', categorySelector_fortis.querySelectorAll('.menu__item')[0]),
            // 2. Aliens
            () => onCategoryClicked_aliens('all', categorySelector_aliens.querySelectorAll('.menu__item')[0]),
            // 3. Weapons
            () => onCategoryClicked_weapons('all', categorySelector_weapons.querySelectorAll('.menu__item')[0]),
            // 4. Infinity Knox
            () => onCategoryClicked_iKnox('all', categorySelector_iKnox.querySelectorAll('.menu__item')[0]),
            // 5. Power Up Knox
            () => onCategoryClicked_pKnox('all', categorySelector_pKnox.querySelectorAll('.menu__item')[0]),
            // 6. Forti Rankings
            () => onCategoryClicked_rankings('all', categorySelector_rankings.querySelectorAll('.menu__item')[0]),
            // 7. Profile Rankings
            () => onCategoryClicked_pRankings(1, categorySelector_pRankings.querySelectorAll('.menu__item')[0]),
            // 8. Forti Skills
            () => {},
            // 9. P2E
            () => {},
            // 10. XP Points
            () => {},
        ];
        selectFirstCategoriesFns[sectionIndex]();
    }

    /**
     * show content of selected stats tab in the Forti Details Pane (the one with "weapons", "skins", etc)
     */
    function onStatTabClicked(tabName) {
        fortiStatPanes.forEach(pane => {
            if(pane.getAttribute('data-stat') == tabName) pane.style.display = 'block';
            else pane.style.display = 'none';
        });
    }

    /**
     * on top nav item clicked
     */
    function onTabClicked(tabIndex, selectedMenuItem) {

        // Select the 1st Category of selected section
        selectFirstCategoryOfSection(tabIndex - 1);

        // Roll to the selected section
        bootcampRoller.style.transform = `translateY(-${(tabIndex - 1) * (100 / TABS_COUNT)}%)`;

        // Unshift all sections
        unshiftPane_all();

        // Set clicked tab as active and other ones as inactive
        mainNav.querySelectorAll('.bootcamp-page__nav-item').forEach(_menuItem => {
            _menuItem.classList.remove('bootcamp-page__nav-item--active');
        })
        selectedMenuItem.classList.add('bootcamp-page__nav-item--active');
    }

    /**
     * on forti category clicked (left navigation)
     */
    function onCategoryClicked_fortis(faction, selectedMenuItem) {

        // Get fortis in selected faction, or all
        const fortis = faction === 'all'
            ? FORTIS
            : FORTIS.filter(f => f.faction == faction);

        // ANIMATION: FADE OUT THE LIST
        cards_fortis.style.opacity = 0;

        setTimeout(() => {

            // Insert into DOM
            cards_fortis.innerHTML = fortis.map(forti => `
                <div class="cards__card ${forti.extraClasses || ''} img-loader" onclick="onCardClicked_fortis(${forti.id})">
                    <img class="cards__card__forti" src="${forti.coverSM}" />
                    <div class="card__title">${forti.name}</div>
                    <span class="card-badge product-rarity product-rarity--sm product-rarity--rounded product-rarity--${forti.rarity}">${forti.rarity.replace(/[-]/g, ' ').toUpperCase()}</span>
                    <div class="card__price">${forti.price}</div>
                    <img class="card__insignia" src="${forti.insignia}" />
                </div>
            `).join('');

            // ANIMATION: FADE IN THE LIST
            cards_fortis.style.opacity = 1;

            // Re-Init Image Loaders
            initImageLoaders();

        }, 200);

        // Set clicked faction as active and other ones as inactive
        categorySelector_fortis.querySelectorAll('.menu__item').forEach(_menuItem => {
            _menuItem.classList.remove('menu__item--active');
        })
        selectedMenuItem.classList.add('menu__item--active');

    }

    /**
     * on alien category clicked (left navigation)
     */
    function onCategoryClicked_aliens(faction, selectedMenuItem) {

        // Get fortis in selected faction, or all
        const aliens = faction === 'all'
            ? ALIENS
            : ALIENS.filter(f => f.faction == faction);

        // ANIMATION: FADE OUT THE LIST
        cards_aliens.style.opacity = 0;

        setTimeout(() => {

            // Insert into DOM
            cards_aliens.innerHTML = aliens.map(alien => `
                <a class="cards__card ${alien.extraClasses || ''} img-loader" onclick="onCardClicked_aliens(${alien.id})">
                    <img class="cards__card__forti" src="${alien.coverSM}" />
                    <div class="card__title">${alien.name}</div>
                    <span class="card-badge product-rarity product-rarity--sm product-rarity--rounded product-rarity--${alien.rarity}">${alien.rarity.replace(/[-]/g, ' ').toUpperCase()}</span>
                    <div class="card__price">${alien.price}</div>
                    <img class="card__insignia" src="${alien.insignia}" />
                </a>
            `).join('');

            // ANIMATION: FADE IN THE LIST
            cards_aliens.style.opacity = 1;

            // Re-Init Image Loaders
            initImageLoaders();
            initImagePopups();

        }, 200);

        // Set clicked faction as active and other ones as inactive
        categorySelector_aliens.querySelectorAll('.menu__item').forEach(_menuItem => {
            _menuItem.classList.remove('menu__item--active');
        })
        selectedMenuItem.classList.add('menu__item--active');

    }

    /**
     * on weapon category clicked (left navigation)
     */
    function onCategoryClicked_weapons(category, selectedMenuItem) {

        console.log('Weapons category clicked: ' + category);

        // Get selected category's cards, or all
        const cards = category === 'all'
            ? WEAPONS
            : WEAPONS.filter(c => c.category == category);

        // ANIMATION: FADE OUT THE LIST
        cards_weapons.style.opacity = 0;

        setTimeout(() => {

            // Insert into DOM
            cards_weapons.innerHTML = cards.map(card => `
                <div class="cards__card cards__card-weapon" onclick="onCardClicked_weapons(${card.id})">
                    <div class="card__top img-loader">
                        <div class="card__title">${card.name}</div>
                        <img class="card__img" src="${card.cover}" loading="lazy" />
                    </div>
                    <div class="card__bottom">
                        <div class="card__bottom__title">Current Value</div>
                        <div class="card__bottom__subtitle">${card.price}</div>
                    </div>
                </div>
            `).join('');

            // ANIMATION: FADE IN THE LIST
            cards_weapons.style.opacity = 1;

            // Re-Init Image Loaders
            initImageLoaders();

        }, 200);

        // Set clicked faction as active and other ones as inactive
        categorySelector_weapons.querySelectorAll('.menu__item').forEach(_menuItem => {
            _menuItem.classList.remove('menu__item--active');
        })
        selectedMenuItem.classList.add('menu__item--active');

    }

    /**
     * on infinity knox category clicked (left navigation)
     */
    function onCategoryClicked_iKnox(category, selectedMenuItem) {

        // Get selected category's cards, or all
        const cards = category === 'all'
            ? I_KNOX
            : category === "gold"
            ? I_KNOX.filter(c => c.group == "gold")
            : category === "emerald"
            ? I_KNOX.filter(c => c.group == "emerald")
            : I_KNOX.filter(c => c.category == category);

        // ANIMATION: FADE OUT THE LIST
        cards_iKnox.style.opacity = 0;

        setTimeout(() => {

            // Insert into DOM
            cards_iKnox.innerHTML = cards.map(card => `
                <div class="cards__knox img-loader" onclick="onCardClicked_iKnox(${card.id})">
                    <img class="cards__knox__img" src="${card.coverSM}" loading="lazy" />
                    <div class="cards__knox__price">${card.price}</div>
                    <div class="cards__knox__edition ${card.group == 'emerald' ? 'cards__knox__edition--shifted' : ''}">${card.group == 'gold' ? 'Holders Edition' : 'Original'}</div>
                </div>
            `).join('');

            // ANIMATION: FADE IN THE LIST
            cards_iKnox.style.opacity = 1;

            // Re-Init Image Loaders
            initImageLoaders();

        }, 200);

        // Set clicked category as active and other ones as inactive
        categorySelector_iKnox.querySelectorAll('.menu__item').forEach(_menuItem => {
            _menuItem.classList.remove('menu__item--active');
        })
        selectedMenuItem.classList.add('menu__item--active');

    }

    /**
     * on power up knox category clicked (left navigation)
     */
    function onCategoryClicked_pKnox(category, selectedMenuItem) {

        // Get selected category's cards, or all
        const cards = category === 'all'
            ? P_KNOX
            : P_KNOX.filter(c => c.category == category);

        // ANIMATION: FADE OUT THE LIST
        cards_pKnox.style.opacity = 0;

        setTimeout(() => {

            // Insert into DOM
            cards_pKnox.innerHTML = cards.map(card => `
                <div class="cards__knox img-loader" onclick="onCardClicked_pKnox(${card.id})">
                    <img class="cards__knox__img" src="${card.cover}" loading="lazy" />
                    <div class="cards__knox__price">${card.price}</div>
                    <div class="cards__knox__letter cards__knox__letter--${getPKnoxColor(card.name)}">${card.name.substr(0, 1)}</div>
                </div>
            `).join('');

            // ANIMATION: FADE IN THE LIST
            cards_pKnox.style.opacity = 1;

            // Re-Init Image Loaders
            initImageLoaders();

        }, 200);

        // Set clicked category as active and other ones as inactive
        categorySelector_pKnox.querySelectorAll('.menu__item').forEach(_menuItem => {
            _menuItem.classList.remove('menu__item--active');
        })
        selectedMenuItem.classList.add('menu__item--active');

    }

    /**
     * on rankings category clicked (left navigation)
     */
    function onCategoryClicked_rankings(category, selectedMenuItem) {

        // Get selected category's cards, or all
        const cards = category === 'all'
            ? RANKINGS
            : RANKINGS.filter(c => c.category == category);

        // ANIMATION: FADE OUT THE LIST
        cards_rankings.style.opacity = 0;

        setTimeout(() => {

            // Insert into DOM
            cards_rankings.innerHTML = cards.map(card => `
                <div class="cards__card cards__card-rank" onclick="onCardClicked_rankings(${card.id})">
                    <div class="card__top">
                        <img class="card__img" src="${card.cover}" loading="lazy" />
                    </div>
                    <div class="card__bottom">
                        <div class="card__bottom__title">${card.name}</div>
                    </div>
                </div>
            `).join('');

            // ANIMATION: FADE IN THE LIST
            cards_rankings.style.opacity = 1;

            // Re-Init Image Loaders
            initImageLoaders();

        }, 200);

        // Set clicked category as active and other ones as inactive
        categorySelector_rankings.querySelectorAll('.menu__item').forEach(_menuItem => {
            _menuItem.classList.remove('menu__item--active');
        })
        selectedMenuItem.classList.add('menu__item--active');

    }

    /**
     * on player rankings category clicked (left navigation)
     */
    function onCategoryClicked_pRankings(rankId, selectedMenuItem) {

        // Get rank
        const rank = P_RANKINGS.filter(r => r.id == rankId)[0];

        // ! If rank not found, don't do anything
        if(!rank) return;

        // ANIMATION: FADE OUT THE LIST
        cards_pRankings.style.opacity = 0;

        setTimeout(() => {

            // Fill Details
            description_pRankings.innerHTML = rank.description;
            profileRank__pRankings.innerHTML = `lvl.${rank.level} ${rank.name}`;
            cover_pRankings.src = rank.cover;
            coverWrapper_pRankings.href = rank.cover;
            coverM_pRankings.src = rank.cover;
            coverWrapperM_pRankings.href = rank.cover;
            price_pRankings.innerHTML = rank.startingRank;
            priceNote_pRankings.innerHTML = rank.startingRankNote;
            specsList__pRankings.innerHTML = rank.specs.map(spec => `<li>${spec}</li>`).join('');
            descriptionIcons_pRankings.innerHTML = `
                <img src="img/icon/soldier.png" />
            `;

            // ANIMATION: FADE IN THE LIST
            cards_pRankings.style.opacity = 1;

        }, 200);

        // Set clicked faction as active and other ones as inactive
        categorySelector_pRankings.querySelectorAll('.menu__item').forEach(_menuItem => {
            _menuItem.classList.remove('menu__item--active');
        })
        selectedMenuItem.classList.add('menu__item--active');

    }

    /**
     * on forti card clicked
     */
    function onCardClicked_fortis(fortiId) {

        // Get selected forti
        const forti = FORTIS.filter(f => f.id == fortiId)[0];

        // Store in State
        selectedForti = forti;

        // Fill forti's details
        backBtnText_fortis.innerHTML = `${forti.name}`;
        description_fortis.innerHTML = forti.description.substr(0, 20);
        fortiDescriptionPopupText.innerHTML = forti.description;
        price_fortis.innerHTML = forti.price;
        fortiAbility.title = forti.ability;
        fortiRarity.innerHTML = forti.rarity.replace(/[-]/g, ' ').toUpperCase();
        fortiRarity.className = '';
        fortiRarity.classList.add(`details__specs__rarity-label`);
        fortiRarity.classList.add(`product-rarity`);
        fortiRarity.classList.add(`product-rarity--rounded`);
        fortiRarity.classList.add(`product-rarity--${forti.rarity}`);
        fortiSpecsList.innerHTML = forti.specs.map(spec => `<li>${spec}</li>`).join('');
        weapon1Name.innerHTML = forti.weapon1Name;
        weapon1Image.src = '';
        weapon1Image.src = forti.weapon1Image;
        if(forti.weapon2Name) {
            weapon2.style.display = "block";
            weapon2Name.innerHTML = forti.weapon2Name;
            weapon2Image.src = '';
            weapon2Image.src = forti.weapon2Image;
        }
        else {
            weapon2.style.display = "none";
        }

        const insigniaName = RANKINGS.filter(r => r.cover == forti.insignia)[0]
            ? RANKINGS.filter(r => r.cover == forti.insignia)[0].name
            : forti.rankName
            ? forti.rankName
            : null;
        descriptionIcons_fortis.innerHTML = `
            <img src="${forti.insignia}" ${insigniaName ? `title="${insigniaName}"` : ''} />
            <img src="${getFortiFactionIcon(forti.faction)}" title="${forti.faction.replace(/[-_]/g, ' ').toUpperCase()}" />
        `;
        fortiVideoBtn.href = forti.video || DEFAULT_FORTI_VIDEO_HREF;

        // Skins
        const skinsTabDOM = bootcamp_fortis.querySelector('[data-stat-tab="skins"]');
        const skinsPaneDOM = bootcamp_fortis.querySelector('[data-stat="skins"]');
        if(forti.skins && forti.skins.length > 0) {
            skinsTabDOM.classList.remove('stats__category--locked');
            skinsTabDOM.onclick = () => onStatTabClicked('skins');
            skinsTabDOM.title = 'Weapon Skins';
            skinsPaneDOM.innerHTML = forti.skins.map(skin => `
                <div class="stats__weapon img-loader">
                    <img class="stats__weapon-img" src=${skin.cover} />
                    <div class="stats__weapon-name">${skin.name}</div>
                </div>
            `)
        }
        else {
            skinsTabDOM.classList.add('stats__category--locked');
            skinsTabDOM.onclick = () => {};
            skinsTabDOM.title = 'Coming soon';
            skinsPaneDOM.innerHTML = '';
        }
        onStatTabClicked('weapons');

        // Exceptions
        // If: Draxxis:
        if(fortiId == 1) {
            priceNote_fortis.innerHTML = `Not sellable`;
        }
        // If: Boomer
        else if(fortiId == 2) {
            priceNote_fortis.innerHTML = `Will be available based on supply (unlimited)`;

        }
        // If: Melon Nusk
        else if(fortiId == 14) {
            cover_fortis.classList.add('details__cover--xmd');
        }
        else {
            priceNote_fortis.innerHTML = DEFAULT_PRICE_NOTE;
            cover_fortis.classList.remove('details__cover--xmd');
        }

        // Open details pane
        shiftPane_fortis();

        // Select Forti's First Weapon
        onFortiWeaponClicked(0);

    }

    /**
     * on alien card clicked
     */
    function onCardClicked_aliens(alienId) {

        // Get selected alien
        const alien = ALIENS.filter(a => a.id == alienId)[0];

        // Store in State
        selectedAlien = alien;

        // Fill Alien's details
        backBtnText_aliens.innerHTML = `${alien.name}`;
        description_aliens.innerHTML = alien.description.substr(0, 20);
        fortiDescriptionPopupText.innerHTML = alien.description;
        price_aliens.innerHTML = alien.price;
        alienAbility.title = alien.ability;
        alienRarity.innerHTML = alien.rarity.replace(/[-]/g, ' ').toUpperCase();
        alienRarity.className = '';
        alienRarity.classList.add(`details__specs__rarity-label`);
        alienRarity.classList.add(`product-rarity`);
        alienRarity.classList.add(`product-rarity--rounded`);
        alienRarity.classList.add(`product-rarity--${alien.rarity}`);
        alienSpecsList.innerHTML = alien.specs.map(spec => `<li>${spec}</li>`).join('');
        weapon1AName.innerHTML = alien.weapon1Name;
        weapon1AImage.src = '';
        weapon1AImage.src = alien.weapon1Image;
        if(alien.weapon2AName) {
            weapon2A.style.display = "block";
            weapon2AName.innerHTML = alien.weapon2Name;
            weapon2AImage.src = '';
            weapon2AImage.src = alien.weapon2Image;
        }
        else {
            weapon2A.style.display = "none";
        }
        descriptionIcons_aliens.innerHTML = `
            <img src="${alien.insignia}" title="${alien.rankName}" />
            <img src="${getAlienFactionIcon(alien.faction)}" title="${alien.faction.replace(/[-_]/g, ' ').toUpperCase()}" />
        `;
        alienVideoBtn.href = alien.video || DEFAULT_FORTI_VIDEO_HREF;

        // Open details pane
        shiftPane_aliens();

        // Select Alien's First Weapon
        onAlienWeaponClicked(0);

    }

    /**
     * on weapon card clicked
     */
    function onCardClicked_weapons(id) {

        // Get selected card
        const card = WEAPONS.filter(c => c.id == id)[0];

        // Update State
        selectedWeapon = card;
        selectedWeapon.skinShown = false;

        // Fill details
        backBtnText_weapons.innerHTML = card.name;
        description_weapons.innerHTML = card.description;
        efficiency_weapons.innerHTML = card.efficiency;
        price_weapons.innerHTML = card.price;
        cover_weapons.src = '';
        cover_weapons.src = card.coverHD;
        coverWrapper_weapons.href = card.coverHD;
        perks_weapons.innerHTML = card.stats.filter(s => s.title != "Supply").map(perk => `
            <tr>
                <td><img src="${perk.icon}" /></td>
                <td>${perk.title}</td>
                <td>${perk.value}</td>
            </tr>
        `).join('');

        // Labels
        labels_weapons.innerHTML = `
            ${card.skin ? `<span class="details__specs__rarity-label product-rarity product-rarity--lg product-rarity--rounded clickable" style="background: var(--emerald);" onclick="switchWeaponSkin();"><i class="fa fa-random"></i> Switch Skins</span>` : ''}
            <span class="details__specs__rarity-label product-rarity product-rarity--lg product-rarity--rounded product-rarity--legacy">Supply: ${card.stats.filter(s => s.title == "Supply")[0].value}</span>
            <span class="details__specs__rarity-label product-rarity product-rarity--lg product-rarity--rounded product-rarity--${card.rarity}">${card.rarity.replace(/[-]/g, ' ').toUpperCase()}</span>
        `;

        // Open details pane
        shiftPane_weapons();

    }

    /**
     * on infinity knox card clicked
     */
    function onCardClicked_iKnox(id) {

        // Get selected card
        const card = I_KNOX.filter(c => c.id == id)[0];

        // Update State
        selectedIKnox = card;

        // Fill details
        backBtnText_iKnox.innerHTML = card.name;
        description_iKnox.innerHTML = card.description;
        price_iKnox.innerHTML = card.price;
        cover_iKnox.src = '';
        cover_iKnox.src = card.cover;
        coverWrapper_iKnox.href = card.cover;
        videoBtn_iKnox.href = card.video;
        perks_iKnox.innerHTML = card.stats.map(perk => `
            <tr>
                <td><img src="${perk.icon}" /></td>
                <td>${perk.title}</td>
                <td>${perk.value}</td>
            </tr>
        `).join('');

        if(card.group == 'gold') {
            priceNote_iKnox.innerHTML = "Will be transferred for the value of BFK";
        }
        else {
            priceNote_iKnox.innerHTML = "Sold only in marketplace";
        }

        // Open details pane
        shiftPane_iKnox();

    }

    /**
     * on power up knox card clicked
     */
    function onCardClicked_pKnox(id) {

        // Get selected card
        const card = P_KNOX.filter(c => c.id == id)[0];

        // Update State
        selectedPKnox = card;

        // Fill details
        backBtnText_pKnox.innerHTML = card.name;
        description_pKnox.innerHTML = card.description;
        price_pKnox.innerHTML = card.price;
        cover_pKnox.src = '';
        cover_pKnox.src = card.cover;
        coverWrapper_pKnox.href = card.cover;
        perks_pKnox.innerHTML = card.stats.map(perk => `
            <tr>
                <td><img src="${perk.icon}" /></td>
                <td>${perk.title}</td>
                <td>${perk.value}</td>
            </tr>
        `).join('');

        // Open details pane
        shiftPane_pKnox();

    }

    /**
     * on rankings card clicked
     */
    function onCardClicked_rankings(id) {

        // Get selected card
        const card = RANKINGS.filter(c => c.id == id)[0];

        // Fill details
        backBtnText_rankings.innerHTML = card.name;
        cover_rankings.src = '';
        cover_rankings.src = card.cover;
        coverWrapper_rankings.href = card.cover;
        perks_rankings.innerHTML = `
            <tr>
                <td></td>
                <td>Skill Level to Rank</td>
                <td><span class="badge badge--blue" style="font-size: 15px;">${card.skill}</span></td>
            </tr>
            <tr class="skill-note">
                <td></td>
                <td></td>
                <td>1 Skill = 200xp</td>
            </tr>
            <tr>
                <td></td>
                <td>Service</td>
                <td>${card.category.replace('_', ' ').split(' ').map(s => s.toUpperCase()).join(' ')}</td>
            </tr>
            <tr>
                <td></td>
                <td>Rank</td>
                <td>${card.name}</td>
            </tr>
        `;

        // Open details pane
        shiftPane_rankings();

    }

    /******************************************** */
    /***************** 6. Init ****************** */
    /******************************************** */
    // On viewport change, fix bootcamp height
    $(window).resize(fixBootcampHeight);
    $(document).ready(fixBootcampHeight);

    // Init
    window.addEventListener('load', function() {

        // Select first category of first section
        selectFirstCategoryOfSection(0);

        // Share Link Navigations: Check if URL contains an ID that should open a specific section in this page
        const fortiId = <?php if(isset($_GET['fortiId'])) echo $_GET['fortiId']; else echo 'null' ?>;
        const alienId = <?php if(isset($_GET['alienId'])) echo $_GET['alienId']; else echo 'null' ?>;
        const iKnoxId = <?php if(isset($_GET['iKnoxId'])) echo $_GET['iKnoxId']; else echo 'null' ?>;
        const pKnoxId = <?php if(isset($_GET['pKnoxId'])) echo $_GET['pKnoxId']; else echo 'null' ?>;
        const weaponeId = <?php if(isset($_GET['weaponeId'])) echo $_GET['weaponeId']; else echo 'null' ?>;

        // Navigate to forti if "fortiId" passed in URL
        if(fortiId) {
            onCardClicked_fortis(fortiId);
        }
        // Navigate to alien if "alienId" passed in URL
        else if(alienId) {
            onTabClicked(2, document.querySelector('.bootcamp-page__nav > div:nth-child(2)'));
            onCardClicked_aliens(alienId);
        }
        else if(weaponeId) {
            onTabClicked(3, document.querySelector('.bootcamp-page__nav > div:nth-child(3)'));
            onCardClicked_weapons(weaponeId);
        }
        // Navigate to Infinity Knox if "iKnoxId" passed in URL
        else if(iKnoxId) {
            onTabClicked(4, document.querySelector('.bootcamp-page__nav > div:nth-child(4)'));
            onCardClicked_iKnox(iKnoxId);
        }
        // Navigate to Power Up Knox if "pKnoxId" passed in URL
        else if(pKnoxId) {
            onTabClicked(5, document.querySelector('.bootcamp-page__nav > div:nth-child(5)'));
            onCardClicked_pKnox(pKnoxId);
        }

    })

    // SVG Popups Init
    svgPanZoom('#supplyFortis');
    svgPanZoom('#rankingSystem');
    svgPanZoom('#Shards-svg');
    svgPanZoom('#ecoSystemSVG');

</script>