<h3 class="logos-title">OUR PRESENCE</h3>
<div class="logos-flex">
    
    <div class="item clickable img-loader br bb" onclick="openInNewTab('<?php echo $BFK_COIN_GECKO_LINK;?>')">
        <img loading="lazy" src="img/logos/logo1.png" alt="" <?php echo imgSize('img/logos/logo1.png'); ?>>
    </div>
    <div class="item clickable img-loader br bb" onclick="openInNewTab('<?php echo $BFK_COIN_MARKET_CAP_LINK;?>')">
        <img loading="lazy" src="img/logos/logo2.png" alt="" <?php echo imgSize('img/logos/logo2.png'); ?>>
    </div>
    <div class="item clickable img-loader br bb" onclick="openInNewTab('<?php echo $BFK_PANCAKESWAP_LINK;?>')">
        <img loading="lazy" src="img/logos/logo3.png" alt="" <?php echo imgSize('img/logos/logo3.png'); ?>>
    </div>
    <div class="item clickable img-loader bb" onclick="openInNewTab('<?php echo $BFK_BSC_SCAN_LINK;?>')">
        <img loading="lazy" src="img/logos/logo4.png" alt="" <?php echo imgSize('img/logos/logo4.png'); ?>>
    </div>
    <div class="item clickable img-loader br bb" onclick="openInNewTab('<?php echo $BFK_DEXTOOLS_LINK;?>')">
        <img loading="lazy" src="img/logos/logo5.png" alt="" <?php echo imgSize('img/logos/logo5.png'); ?>>
    </div>
    <div class="item clickable img-loader br bb" onclick="openInNewTab('<?php echo $BFK_COINBASE_LINK;?>')">
        <img loading="lazy" src="img/logos/coinbase.png" alt="" <?php echo imgSize('img/logos/coinbase.png'); ?>>
    </div>
    <div class="item clickable img-loader br bb" onclick="openInNewTab('<?php echo $BFK_CRYPTO_LINK;?>')">
        <img loading="lazy" src="img/logos/crypto.png" alt="" <?php echo imgSize('img/logos/crypto.png'); ?>>
    </div>
    <div class="item clickable img-loader bb" onclick="openInNewTab('<?php echo $BFK_NOMICS_LINK;?>')">
        <img loading="lazy" src="img/logos/nomics.png" alt="" <?php echo imgSize('img/logos/nomics.png'); ?>>
    </div>
    <div class="item clickable img-loader br bb" onclick="openInNewTab('<?php echo $BFK_CERTIK_LINK;?>')">
        <img loading="lazy" src="img/logos/certik.png" alt="" <?php echo imgSize('img/logos/certik.png'); ?>>
    </div>
    <div class="item clickable img-loader br bb" onclick="openInNewTab('<?php echo $BFK_BINANCE_LINK;?>')">
        <img loading="lazy" src="img/logos/binance.png" alt="" <?php echo imgSize('img/logos/binance.png'); ?>>
    </div>
    <div class="item clickable img-loader br bb" onclick="openInNewTab('https://cryptoexpodubai.com/')">
        <img loading="lazy" src="img/logos/cryptoexpo.png" alt="" <?php echo imgSize('img/logos/cryptoexpo.png'); ?>>
    </div>
    <div class="item clickable img-loader bb" onclick="openInNewTab('https://tresconglobal.com/conferences/blockchain/')">
        <img loading="lazy" src="img/logos/wbs.png" alt="" <?php echo imgSize('img/logos/wbs.png'); ?>>
    </div>
    <div class="item clickable img-loader br bb" onclick="openInNewTab('https://www.flaticon.com/')">
        <img loading="lazy" src="img/logos/flaticon.png" alt="" <?php echo imgSize('img/logos/flaticon.png'); ?>>
    </div>
    <div class="item clickable img-loader br bb" onclick="openAlertPopup(`<img class='copyright-img' src='img/copyright/copyright.png' alt='' height='100%' width='100%'>`)">
        <img loading="lazy" src="img/logos/ministry.png" alt="" <?php echo imgSize('img/logos/ministry.png'); ?>>
    </div>
    <div class="item clickable img-loader br bb" onclick="openInNewTab('<?php echo $BFK_CGC_LINK;?>')">
        <img loading="lazy" src="img/logos/CGC.jpg" alt="" <?php echo imgSize('img/logos/CGC.jpg'); ?>>
    </div>
    <div class="item clickable img-loader bb" onclick="openInNewTab('https://twitter.com/CoinLiquidity/status/1511044546058145793?s=20&t=nE78Wy8XydirH9WV34OAqQ')">
        <img loading="lazy" src="img/logos/cls.png" alt="" <?php echo imgSize('img/logos/cls.png'); ?>>
    </div>
    <div class="item clickable img-loader br bb">
        <img loading="lazy" src="img/logos/ekaesap.png" alt="" <?php echo imgSize('img/logos/ekaesap.png'); ?>>
    </div>
    <div class="item clickable img-loader br bb" onclick="openInNewTab('<?php echo $BFK_LUDUFI_LINK;?>')">
        <img loading="lazy" src="img/logos/ludufi.png" alt="" <?php echo imgSize('img/logos/ludufi.png'); ?>>
    </div>
    <div class="item clickable img-loader br bb" onclick="openInNewTab('<?php echo $BFK_ZEX_LINK;?>')">
        <img loading="lazy" src="img/logos/zex.png" alt="" <?php echo imgSize('img/logos/zex.png'); ?>>
    </div>
    <div class="item clickable img-loader bb" onclick="openInNewTab('<?php echo $BFK_YUDIZ_LINK;?>')">
        <img loading="lazy" src="img/logos/yudiz.png" alt="" <?php echo imgSize('img/logos/yudiz.png'); ?>>
    </div>
    <div class="item clickable img-loader br">
        <img loading="lazy" src="img/logos/Google-Play.png" alt="" <?php echo imgSize('img/logos/Google-Play.png'); ?>>
    </div>
    <div class="item clickable img-loader br">
        <img loading="lazy" src="img/logos/AppStore.png" alt="" <?php echo imgSize('img/logos/AppStore.png'); ?>>
    </div>
    <div class="item clickable img-loader br">
        <img loading="lazy" src="img/logos/bitrue.png" alt="" <?php echo imgSize('img/logos/bitrue.png'); ?>>
    </div>
    <div class="item clickable img-loader">
        <img loading="lazy" src="img/logos/lbank.svg" alt="" <?php echo imgSize('img/logos/lbank.svg'); ?>>
    </div>
</div>