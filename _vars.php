<?php

// General Info
$ADDRESS = "RAK Business Center, BS Zone, United Arab Emirates";
$PHONE = "+971 58 578 7267";
$EMAIL = "business@bfkwarzone.com";
$CONTRACT_ADDRESS = "0xed44623b06616bccec876617c124f5461bd5f79b";

// General Links
$BFK_ALPHA_GAME_LINK = "https://narsunprojects.com/bfk/";
$BFK_NARSUN_LINK = "https://www.narsunstudios.com/bfk";
$BFK_MARKETPLACE_LINK = "https://marketplace.bfkwarzone.com";
$BFK_BSC_SCAN_LINK = "https://bscscan.com/address/0xed44623b06616bccec876617c124f5461bd5f79b";
$BFK_PANCAKESWAP_LINK = "https://pancakeswap.finance/swap?outputCurrency=0xed44623b06616bccec876617c124f5461bd5f79b";
$BFK_DEXTOOLS_LINK = "https://www.dextools.io/app/bsc/pair-explorer/0xc6dc902af09e56cb82666785efcdc3d33e557776";
$BFK_POOCOIN_LINK = "https://poocoin.app/tokens/0xed44623b06616bccec876617c124f5461bd5f79b";
$BFK_COIN_MARKET_CAP_LINK = "https://coinmarketcap.com/currencies/bfk-warzone/";
$BFK_COIN_GECKO_LINK = "https://www.coingecko.com/en/coins/bfk-warzone";
$BFK_AUDIT_LINK = "https://www.certik.org/projects/bfkwarzone";
$BFK_SWAP_LINK = "https://swap.babyfortknox.io/";
$BFK_WHITELIST_CONTEST_LINK = "https://gleam.io/hQ4Iq/baby-fort-knox-whitelist-competition";
$BFK_GLEAM_CONTEST_LINK = "https://gleam.io/zkERC/bfk-warzone";
$BFK_PINKSALE_LINK = "https://pancakeswap.finance/swap?outputCurrency=0xed44623b06616bccec876617c124f5461bd5f79b";
$BFK_MINIGAME_LINK = "https://minigame.babyfortknox.io/";
$BFK_DASHBOARD_LINK = "https://dashboard.babyfortknox.io/";
$BFK_WHITELISTS_LINK = "https://whitelists.babyfortknox.io/";
$BFK_COINBASE_LINK = "https://www.coinbase.com/price/bfk-warzone";
$BFK_CRYPTO_LINK = "https://crypto.com/price/bfk-warzone";
$BFK_NOMICS_LINK = "https://nomics.com/assets/bfk2-bfk-warzone";
$BFK_CERTIK_LINK = "https://www.certik.org/projects/bfkwarzone";
$BFK_SUPPORT_LINK = "https://forms.gle/A2bjtTFq7DR1GJK56";
$BFK_GAME_MAINTENANCE_URL = "http://marketplace.bfkwarzone.com/maintenance";
$BFK_ACCOUNT_RECOVERY_LINK = "https://forms.gle/1xin62mhrd5gU8wB8";
$BFK_BUY_GAME_LINK = "https://pancakeswap.com/swap/0xed44623b06616bccec876617c124f5461bd5f79b";
$TGC_LINK = "https://tgcinternational.net";
$BFK_MAIN_LINK = "https://bfkwarzone.com";
$BFK_BINANCE_LINK = "https://www.binance.com/en/price/bfk-warzone";
$BFK_CRYPTOEXPO_LINK = "https://cryptoexpodubai.com/";
$BFK_SUMMITLOGO_LINK = "https://tresconglobal.com/conferences/blockchain/dubai/";
$BFK_CGC_LINK = "https://www.cgc.one/";
$BFK_LUDUFI_LINK = "https://ludufi.io/";
$BFK_ZEX_LINK = "https://zexprwire.com/";
$BFK_YUDIZ_LINK = "https://www.yudiz.com/";
$BFK_STORE_LINK = "https://store.bfkwarzone.com/";
$BFK_CONTRIBUTION_LINK = "https://contribution.bfkwarzone.com/";

// Telegram Links
$BFK_TELEGRAM_CN_LINK = "https://t.me/BabyFortKnox_Chinese";
$BFK_TELEGRAM_ID_LINK = "https://t.me/bfkwarzone_ID";
$BFK_TELEGRAM_JP_LINK = "https://t.me/bfkwarzone_JP";
$BFK_TELEGRAM_PH_LINK = "https://t.me/BabyFortKnox_PH";
$BFK_TELEGRAM_TR_LINK = "https://t.me/bfkwarzone_TR";
$BFK_TELEGRAM_SHILL_LINK = "https://t.me/BabyFortKnox_ShillSquad";
$BFK_TELEGRAM_ANNOUNCEMENTS_LINK = "https://t.me/bfkannouncements";
$BFK_TELEGRAM_OFFICIAL_LINK = "https://t.me/bfkwarzone";

// Social Media Links
$BFK_FACEBOOK_LINK = "https://www.facebook.com/BFK-Warzone-113049517816470";
$BFK_TWITTER_LINK = "https://twitter.com/bfkwarzone";
$BFK_INSTAGRAM_LINK = "https://www.instagram.com/bfk_warzone/";
$BFK_TIKTOK_LINK = "https://www.tiktok.com/@bfkwarzone";
$BFK_GITHUB_LINK = "https://github.com/bfkwarzone";
$BFK_YT_LINK = "https://www.youtube.com/c/BFKWarzone";
$BFK_DISCORD_LINK = "https://discord.gg/qdjqYg2xm2";
$BFK_TWITCH_LINK = "https://www.twitch.tv/bfkwarzone/";
$BFK_TIKTOK_LINK = "https://www.tiktok.com/@bfkwarzone";

// Alpha Game
$BFK_ALPHA_USER = "bfkwarzone";
$BFK_ALPHA_PASS = "bfk-2021";
$BFK_GAME_RELEASE = 1642809599;
$BFK_GAME_RELEASE_DATE = "01/22/2022 03:59:59";

?>