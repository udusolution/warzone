<?php

// Return image height width as HTML attributes
function imgSize($path) {
    $result = getimagesize($path);
    if($result == false) {
        return '';
    }
    else {
        return $result[3];
    }
}

// HELPER FUNCTION TO encodeURIComponent
function encodeURIComponent($str) {
    $revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
    return strtr(rawurlencode($str), $revert);
}

?>