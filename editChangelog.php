<?php

    include 'env.php';

    $error = '';
    $success = '';

    // Create connection
    $conn = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Get Log
    $log = null;
    $editId = null;
    
    if(isset($_GET['editId'])) {
        $editId = $_GET['editId'];

        $log = $conn->query("SELECT * FROM changelog WHERE id = " . $conn->escape_string($editId))->fetch_assoc();

        // do not use fetch_all() since it requires mysqlind
        // $log['images'] = $conn->query("SELECT * FROM changelog_image WHERE changelogId = " . $conn->escape_string($editId))->fetch_all(MYSQLI_ASSOC);

        $ires1 = $conn->query("SELECT * FROM changelog_image WHERE changelogId = " . $conn->escape_string($editId));
        $imgs1 = [];
        while($img1 = $ires1->fetch_assoc()) {
            $imgs1[] = $img1;
        }
        $log['images'] = $imgs1;
    }

    // Get form values
    if(isset($_POST['title']) && isset($editId)) {

        // Upload Vars
        $targetDir = "uploads/";
        $total = count($_FILES['upload']['name']);
        $uploadedFiles = [];

        for( $i=0 ; $i < $total ; $i++ ) {

            //Get the temp file path
            $tmpFilePath = $_FILES['upload']['tmp_name'][$i];
          
            //Make sure we have a file path
            if ($tmpFilePath != ""){
              //Setup our new file path
              $newFilePath = $targetDir . uniqid() . '-' . $_FILES['upload']['name'][$i];
          
              //Upload the file into the temp dir
              if(move_uploaded_file($tmpFilePath, $newFilePath)) {
          
                //Handle other code here
                array_push($uploadedFiles, $newFilePath);
              }
            }
        }

        // Form Data
        $title = $_POST['title'];
        $description = $_POST['description'];
        $date = $_POST['date'];
        $version = $_POST['version'];
        $usrPassword = $_POST['password'];

        if($usrPassword != 'BFK!99') {
            $error = 'Incorrect password';
        }
        else {

            $sql = "UPDATE changelog SET `title` = ?, `description` = ?, `date` = ?, `version` = ? WHERE id = ?";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param("ssssi", $title, $description, $date, $version, $editId);
    
            try {
                $stmt->execute();
    
                foreach($uploadedFiles as $file) {
                    $conn->query("INSERT INTO changelog_image (`changelogId`, `file`) VALUES (". $editId .", '". $file ."')");
                }

                foreach($log['images'] as $curImg) {
                    if(!isset($_POST['img' . $curImg['id']])) {
                        $conn->query("DELETE FROM changelog_image WHERE id = " . $curImg['id']);
                    }
                }

                $success = 'Successfully updated changelog!';

            }
            catch(Error $e) {
                echo "Issue inserting into database";
            }

        }

        
    }

    if(isset($_GET['editId'])) {
        $editId = $_GET['editId'];

        $log = $conn->query("SELECT * FROM changelog WHERE id = " . $conn->escape_string($editId))->fetch_assoc();

        // do not use fetch_all() since it requires mysqlind
        // $log['images'] = $conn->query("SELECT * FROM changelog_image WHERE changelogId = " . $conn->escape_string($editId))->fetch_all(MYSQLI_ASSOC);

        $ires2 = $conn->query("SELECT * FROM changelog_image WHERE changelogId = " . $conn->escape_string($editId));
        $imgs2 = [];
        while($img2 = $ires2->fetch_assoc()) {
            $imgs2[] = $img2;
        }
        $log['images'] = $imgs2;
    }
    
    $conn->close();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BFK Warzone | Edit Changelog</title>

    <script src="https://cdn.ckeditor.com/ckeditor5/29.2.0/classic/ckeditor.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/changelog-admin.css">

</head>
<body>
    
    <div class="form-container">

        <!-- Form -->
        <form class="add-changelog section" method="POST" action="editChangelog.php?editId=<?php if(isset($_GET['editId'])) echo $_GET['editId']; ?>" enctype="multipart/form-data" id="theForm">
    
            <a href="changelog-admin.php" class="btn btn-info btn-sm">Back</a>

            <h2>Edit Changelog Entry</h2>

            <!-- Error -->
            <?php if(isset($error) && $error != "") { ?>
                <div class="alert alert-danger" role="alert">
                    <i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
                </div>
            <?php } ?>
            <?php if(isset($success) && $success != "") { ?>
                <div class="alert alert-success" role="alert">
                    <i class="fa fa-check-circle"></i> <?php echo $success; ?>
                </div>
            <?php } ?>
    
            <!-- Title -->
            <div class="form-group">
                <label>* Title</label>
                <input class="form-control" name="title" required min="3" max="30" value="<?php if(isset($log['title'])) echo $log['title']; ?>" />
                <span class="form-error"></span>
            </div>

            <!-- Version -->
            <div class="form-group">
                <label>* Version</label>
                <input class="form-control" type="text" name="version" required value="<?php if(isset($log['version'])) echo $log['version']; ?>" />
                <span class="form-error"></span>
            </div>

            <!-- Date -->
            <div class="form-group">
                <label>* Date</label>
                <input class="form-control" type="date" name="date" required value="<?php if(isset($log['date'])) echo $log['date']; ?>" />
                <span class="form-error"></span>
            </div>

            <!-- Description -->
            <div class="form-group">
                <label>* Description</label>
                <textarea id="editor" name="description"><?php if(isset($log['description'])) echo $log['description']; ?></textarea>
                <span class="form-error"></span>
            </div>

            <!-- Current Images -->
            <label>Remove Current Image(s), Audio(s), or Video(s) (Optional)</label>
            <div class="image-group">
                <?php if(isset($log['images']) && count($log['images']) > 0) {
                    foreach($log['images'] as $img) { ?>
                        <div class="img-selection">
                            <input type="checkbox" name="img<?php echo $img['id']; ?>" checked />
                            <?php if(in_array(pathinfo($img['file'], PATHINFO_EXTENSION), ['png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'])) { ?>
                                <img src="<?php echo $img['file']; ?>" />
                            <?php } else if(in_array(pathinfo($img['file'], PATHINFO_EXTENSION), ['mp3', 'aac', 'wav', 'flac', 'ogg', 'weba', 'MP3', 'AAC', 'WAV', 'FLAC', 'OGG', 'WEBA'])) { ?>
                                <audio controls src="<?php echo $img['file']; ?>" style="margin-top: 50px;"></audio>
                            <?php } else { ?>
                                <video controls src="<?php echo $img['file']; ?>" style="width: 300px;"></video>
                            <?php } ?>
                        </div>
                <?php }} ?>
            </div>

            <!-- Images -->
            <div class="form-group">
                <label>Add New Images, Audio, or Videos (Optional)</label>
                <input class="form-control" type="file" name="upload[]" accept="image/*,audio/*,video/mp4,video/webm" multiple="multiple" />
                <span class="form-error"></span>
            </div>

            <!-- Password -->
            <div class="form-group">
                <label>* Password</label>
                <input class="form-control" name="password" required />
                <span class="form-error"></span>
            </div>
    
            <!-- Submit -->
            <button type="submit" class="btn btn-primary" id="saveBtn">Save</button>

        </form>

    </div>
    
    <!-- Loader -->
    <div class="loader-wrapper">
        <div class="loader">
            <i class="fa fa-spinner loader-icon"></i>
        </div>
    </div>

</body>
</html>

<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }

    // On form submit, disable the button and show a loader
    const theForm = document.querySelector('#theForm');
    const saveBtn = document.querySelector('#saveBtn');
    const loader = document.querySelector('.loader-wrapper');
    loader.classList.remove('loader-wrapper--show');
    theForm.addEventListener('submit', function() {
        saveBtn.disabled = 'disabled';
        loader.classList.add('loader-wrapper--show');
    })
</script>

<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ), {
            removePlugins: ['EasyImage', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed', 'Table']
        })
        .catch( error => {
            console.error( error );
        } );
</script>