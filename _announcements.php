<?php if(isset($ANNOUNCEMENTS) && count($ANNOUNCEMENTS) > 0) { ?>
    <div class="announcements-carousel">

        <?php foreach($ANNOUNCEMENTS as $k => $v) { ?>
            <div class="slide-wrap">
                <div class="announcement">
                    <div class="announcement__title"><?php echo $v['title']; ?></div>
                    <div class="announcement__subtitle"><?php echo $v['subtitle']; ?></div>
                    <?php if(isset($v['description']) && $v['description'] != '') { ?>
                        <button class="announcement__read-more-btn" onclick="openAnnouncementPopup(`<?php echo encodeURIComponent($v['description']); ?>`)">read more</button>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>

    </div>
<?php } ?>