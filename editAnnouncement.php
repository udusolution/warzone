<?php

    include 'env.php';

    $error = '';
    $success = '';

    // Create connection
    $conn = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Get announcement
    $announcement = null;
    $editId = null;
    
    if(isset($_GET['editId'])) {
        $editId = $_GET['editId'];

        $announcement = $conn->query("SELECT * FROM announcement WHERE id = " . $conn->escape_string($editId))->fetch_assoc();
    }

    // Get form values
    if(isset($_POST['title']) && isset($editId)) {

        // Form Data
        $title = $_POST['title'];
        $subtitle = $_POST['subtitle'];
        $description = $_POST['description'];
        $isUrgent = $_POST['isUrgent'] ? 1 : 0;
        $usrPassword = $_POST['password'];

        if($usrPassword != 'BFK!99') {
            $error = 'Incorrect password';
        }
        else {

            $sql = "UPDATE announcement SET `title` = ?, `subtitle` = ?, `description` = ?, `isUrgent` = ? WHERE id = ?";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param("sssii", $title, $subtitle, $description, $isUrgent, $editId);
    
            try {

                // If urgent, un-urgent the other announcements
                if($isUrgent == 1) {
                    $conn->query("UPDATE announcement SET isUrgent = 0");
                }

                $stmt->execute();

                $success = 'Successfully updated changelog!';

            }
            catch(Error $e) {
                echo "Issue inserting into database";
            }

        }

        
    }

    if(isset($_GET['editId'])) {
        $editId = $_GET['editId'];

        $announcement = $conn->query("SELECT * FROM announcement WHERE id = " . $conn->escape_string($editId))->fetch_assoc();

    }
    
    $conn->close();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BFK Warzone | Edit Changelog</title>

    <script src="https://cdn.ckeditor.com/ckeditor5/29.2.0/classic/ckeditor.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/changelog-admin.css">

</head>
<body>
    
    <div class="form-container">

        <!-- Form -->
        <form class="add-changelog section" method="POST" action="editAnnouncement.php?editId=<?php if(isset($_GET['editId'])) echo $_GET['editId']; ?>" id="theForm">
    
            <a href="announcements-admin.php" class="btn btn-info btn-sm">Back</a>

            <h2>Edit Announcement</h2>

            <!-- Error -->
            <?php if(isset($error) && $error != "") { ?>
                <div class="alert alert-danger" role="alert">
                    <i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
                </div>
            <?php } ?>
            <?php if(isset($success) && $success != "") { ?>
                <div class="alert alert-success" role="alert">
                    <i class="fa fa-check-circle"></i> <?php echo $success; ?>
                </div>
            <?php } ?>
    
            <!-- Title -->
            <div class="form-group">
                <label>* Title</label>
                <input class="form-control" name="title" required min="3" max="100" value="<?php if(isset($announcement['title'])) echo $announcement['title']; ?>" />
                <span class="form-error"></span>
            </div>

            <!-- Subtitle -->
            <div class="form-group">
                <label>* Subtitle</label>
                <textarea id="editor2" name="subtitle"><?php if(isset($announcement['subtitle'])) echo $announcement['subtitle']; ?></textarea>
                <span class="form-error"></span>
            </div>

            <!-- Description -->
            <div class="form-group">
                <label>* Description</label>
                <textarea id="editor" name="description"><?php if(isset($announcement['description'])) echo $announcement['description']; ?></textarea>
                <span class="form-error"></span>
            </div>

            <!-- Is Urgent -->
            <div class="form-group">
                <label>
                    <input type="checkbox" name="isUrgent" <?php if(isset($announcement['isUrgent']) && $announcement['isUrgent'] == 1) echo 'checked' ?> />
                    <span>show as urgent announcement?</span>
                </label>
                <span class="form-error"></span>
            </div>

            <!-- Password -->
            <div class="form-group">
                <label>* Password</label>
                <input class="form-control" name="password" required />
                <span class="form-error"></span>
            </div>
    
            <!-- Submit -->
            <button type="submit" class="btn btn-primary" id="saveBtn">Save</button>

        </form>

    </div>
    
    <!-- Loader -->
    <div class="loader-wrapper">
        <div class="loader">
            <i class="fa fa-spinner loader-icon"></i>
        </div>
    </div>

</body>
</html>

<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }

    // On form submit, disable the button and show a loader
    const theForm = document.querySelector('#theForm');
    const saveBtn = document.querySelector('#saveBtn');
    const loader = document.querySelector('.loader-wrapper');
    loader.classList.remove('loader-wrapper--show');
    theForm.addEventListener('submit', function() {
        saveBtn.disabled = 'disabled';
        loader.classList.add('loader-wrapper--show');
    })
</script>

<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ), {
            removePlugins: ['EasyImage', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed', 'Table']
        })
        .catch( error => {
            console.error( error );
        } );

    ClassicEditor
        .create( document.querySelector( '#editor2' ), {
            removePlugins: ['EasyImage', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed', 'Table']
        })
        .catch( error => {
            console.error( error );
        } );
</script>