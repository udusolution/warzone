<?php

    require_once './vendor/autoload.php';
    require './env.php';
    use Mailgun\Mailgun;
    // use Google_Client;
    // use Google_Service_Sheets;
    // use Google_Service_Sheets_ValueRange;

    $data = json_decode($_POST['data']);
    
    try {

        
        $message = 'Your message has been sent!';
        $emailSubject = 'BFK Warzone - New Subscriber';
        $emailText = 'A new email subscription has been submitted. Email: "'.$data->email.'".';

        // EMAIL
        # Instantiate the client.
        $mg = Mailgun::create($MAILGUN_KEY);
        $domain = $MAILGUN_DOMAIN;

        # Make the call to the client.
        # COMMENTED OUT DUE TO SPAM ATTACKS (TEMPORARY SOLUTION)
        $mg->messages()->send($domain, [
            'from'    => 'BFK Warzone <no-reply@'.$domain.'>',
            'to'      => $RECIPIENT_EMAIL,
            'subject' => $emailSubject,
            'text'    => $emailText
        ]);

        
    } catch (Error $e) {
        
        echo json_encode(['status' => 'error', 'message' => $e->message]);
        
    }
    
    try {
        //////////////////// GOOGLE SHEETS ////////////////////////
        # COMMENTED OUT DUE TO SPAM ATTACKS (TEMPORARY SOLUTION)
        $client = new Google_Client();
        $client->setApplicationName('warzone');
        $client->setScopes([Google_Service_Sheets::SPREADSHEETS]);
        $client->setAccessType('offline');
        $client->setAuthConfig(__DIR__ . '/sheet-creds.json');
        $service = new Google_Service_Sheets($client);
        $spreadsheetId = "1ezna5T0c10VE9YLvJhuL2Xei__zRpJsWTtNF6rwV0YM";
        $sheetName = 'BFK WARZONE SUBSCRIPTION FORM';
        $body = [
            $data->email
        ];
        $valueRange = new Google_Service_Sheets_ValueRange();
        $valueRange->setValues([
            "values" => $body
        ]);
        $params = ["valueInputOption" => "RAW", "insertDataOption" => "INSERT_ROWS"];
        $service->spreadsheets_values->append($spreadsheetId, $sheetName, $valueRange, $params);
        ////////////////////////////////////////////////////////////

    } catch (Google_Exception $e) {
        echo json_encode(['status' => 'error', 'message' => $e->getMessage()]);
    }


    echo json_encode([
        'status' => 'success',
        'message' => 'Success'
    ]);
?>