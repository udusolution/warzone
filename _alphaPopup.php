<div class="alpha-popup">
    <div class="alpha-popup__inner">
        <div class="alpha-popup__overlay" onclick="closeAlphaPopup()"></div>
        <div class="alpha-popup__container">

            <!-- Close button -->
            <div class="alpha-popup__top">
                <i class="fa fa-times" onclick="closeAlphaPopup()"></i>
            </div>

            <div class="alpha-popup__content">
            
                <!-- Content -->
                <p>Please use the following login credentials to play the alpha game:</p>
                <div>
                    <span class="primary-color">Username: </span>
                    <span><?php echo $BFK_ALPHA_USER; ?></span>
                </div>
                <div>
                    <span class="primary-color">Password: </span>
                    <span><?php echo $BFK_ALPHA_PASS; ?></span>
                </div>

                <!-- Note -->
                <span class="info-alert"><i class="fas fa-info-circle"></i> The alpha is only playable on PC Web app now</span>
    
                <!-- Bottom -->
                <!-- <div class="alpha-popup__bottom">
                    <a href="<?php echo $BFK_ALPHA_GAME_LINK; ?>" class="btn-styled" target="_blank">Play Alpha <i class="fa fa-arrow-right"></i></a>
                </div> -->

            </div>

        </div>
    </div>
</div>

<script>

    // Alpha Popup
    const alphaPopup = document.querySelector('.alpha-popup');

    function showAlphaPopup() {
        alphaPopup.classList.add('alpha-popup--open');
    }
    function closeAlphaPopup() {
        alphaPopup.classList.remove('alpha-popup--open');
    }

</script>