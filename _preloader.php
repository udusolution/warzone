<div id="preloader">
    <div id="loading-center">
        <div id="loading-center-absolute">
            <div class="loader-wrapper">
                <img src="img/icon/preloader.svg" alt="" height="150">
                <img class="loader-icon" src="img/favicon.png" />
            </div>
        </div>
    </div>
</div>