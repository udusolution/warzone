<!-- Global Variables -->
<?php include '_vars.php'; ?>
<?php include '_content.php'; ?>
<?php include '_functions.php'; ?>
<?php include 'operations/getAnnouncements.php'; ?>

<!-- Title -->
<title>BFK WARZONE 2D SHOOTER GAME | NFT PLAY TO EARN</title>

<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="We are providing $BFK holders a platform where they can play a game and earn money while battling friends and completing raids! We are building an NFT interactive game where users can battle their purchased OR custom made and improved NFT characters on the battlefield. Community members can purchase pre-built characters, or use the free NFT characters and start battling opponents online! We will also host regular dungeons and raids with our events which give users a chance to gain more tokens. Players can combine forces and join a multiplayer raid where defeated bosses (Ethereum aliens) drop tons of loot and $BFK tokens - whether you decide to sell your tokens for BNB, or unlock better gear and continue moving up the ranks, is completely up to you! Users will be able to upgrade their NFT character by buying the latest and most powerful weapons, armor, and special abilities from our marketplace. The users will have the option to build characters from scratch in our “Build your own NFT-SOLDIER” section. Importantly, users will have an option to start with a free standard character that is upgradable to higher ranks by earning $BFK and unlocking better abilities and gear as they grind away in multiplayer dungeons. You can simply start defeating your opponents  after staking coins, in 1v1 style gameplay where the winner takes all! Feeling lucky? Try battling it out for NFT ownership after being matched up with a suitable opponent! Build your arsenal of baby's and defend our Fort Knox.">
<meta name="keywords" content="bfk, warzone">
<meta property="og:locale" content="en_US">
<meta property="og:type" content="article">
<meta property="og:title" content="BFK WARZONE 2D SHOOTER GAME | NFT PLAY TO EARN">
<meta property="og:description" content="We are providing $BFK holders a platform where they can play a game and earn money while battling friends and completing raids! We are building an NFT interactive game where users can battle their purchased OR custom made and improved NFT characters on the battlefield. Community members can purchase pre-built characters, or use the free NFT characters and start battling opponents online! We will also host regular dungeons and raids with our events which give users a chance to gain more tokens. Players can combine forces and join a multiplayer raid where defeated bosses (Ethereum aliens) drop tons of loot and $BFK tokens - whether you decide to sell your tokens for BNB, or unlock better gear and continue moving up the ranks, is completely up to you! Users will be able to upgrade their NFT character by buying the latest and most powerful weapons, armor, and special abilities from our marketplace. The users will have the option to build characters from scratch in our “Build your own NFT-SOLDIER” section. Importantly, users will have an option to start with a free standard character that is upgradable to higher ranks by earning $BFK and unlocking better abilities and gear as they grind away in multiplayer dungeons. You can simply start defeating your opponents  after staking coins, in 1v1 style gameplay where the winner takes all! Feeling lucky? Try battling it out for NFT ownership after being matched up with a suitable opponent! Build your arsenal of baby's and defend our Fort Knox.">
<meta property="og:url" content="https://warzone.bfkwarzone.com/">
<meta property="og:site_name" content="BFK Warzone">
<meta property="og:updated_time" content="2021-12-15T12:20:00+00:00">
<meta property="og:image" content="https://warzone.bfkwarzone.com/img/favicon.png">
<meta property="og:image:width" content="100">
<meta property="og:image:height" content="100">
<meta property="og:image:alt" content="BFK Warzone">
<meta property="og:image:type" content="image/png">
<meta name="facebook-domain-verification" content="kwh6zcmkprdx5f32ll2uur1n3lwc11" />

<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">

<!-- CSS Links (Uncommented in favor of injecting into HTML directly for optimization) -->
<!-- <link rel="stylesheet" href="css/_mini.css"> -->
<!-- <link rel="stylesheet" href="css/style.css?rand=<?php echo time(); ?>"> -->

<!-- Include the CSS files into the HTML directly for optimization -->
<style>
    <?php include 'css/_mini.css'; ?>
    <?php include 'css/style.css'; ?>
</style>

<!-- Font -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css?family=Oxanium:400,500,600,700,800|Poppins:400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet"> 