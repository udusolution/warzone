<?php

$EVENT = [
    array(
        "id" => 1,
        "name" => "Crypto Expo 2022",
        "date" => "March 2022",
        "img" => "img/media/expo2022/banner.jpeg",
        "link" => "expo2022.php"
    ),
    array(
        "id" => 2,
        "name" => "World Blockchain Summit 2022",
        "date" => "March 2022",
        "img" => "img/media/wbs2022/banner.jpeg",
        "link" => "wbs2022.php"
    ),
    array(
        "id" => 3,
        "name" => "Welups 2022",
        "date" => "April 2022",
        "img" => "img/media/welups2022/banner.jpeg",
        "link" => "welups2022.php"
    ),
    array(
        "id" => 4,
        "name" => "Lbank ETHDubai 2022",
        "date" => "April 2022",
        "img" => "img/media/lbank2022/banner.png",
        "link" => "lbank2022.php"
    ),
    array(
        "id" => 5,
        "name" => "India Trip 2022",
        "date" => "April 2022",
        "img" => "img/media/indiatrip2022/banner.jpg",
        "link" => "indiatrip2022.php"
    ),
    array(
        "id" => 6,
        "name" => "Blockchain Mania 2022",
        "date" => "May 2022",
        "img" => "img/media/Blockchainmania2022/banner.jpg",
        "link" => "Blockchainmania2022.php"
    ),
    array(
        "id" => 7,
        "name" => "CGC - Summit 2022",
        "date" => "May  2022",
        "img" => "img/media/CGC-Gaming2022/banner.jpg",
        "link" => "CGC-Gaming2022.php"
    ),
]

?>

<script>

    const EVENT = <?php echo json_encode($EVENT); ?>;

</script>