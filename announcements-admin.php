<?php
     ini_set('display_errors', 1);
    include 'env.php';

    $error = '';
    $success = '';

    $title = '';
    $subtitle = '';
    $description = '';
    $isUrgent = false;

    $alertMsg = '';
    $pw = '';
    if(isset($_GET['pw'])) $pw = $_GET['pw'];
    if(isset($_GET['msg'])) {
        $alertMsg = $_GET['msg'];
        $error = $alertMsg;
    }

    // Create connection
    $conn = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Get Announcements
    $res3 = $conn->query("SELECT * FROM announcement ORDER BY id DESC");
    $announcements = [];
    while ($row3 = $res3->fetch_assoc()) {
        array_push($announcements, $row3);
    }

    // Get form values
    if(isset($_POST['title'])) {

        // Form Data
        $title = $_POST['title'];
        $subtitle = $_POST['subtitle'];
        $description = $_POST['description'];
        $isUrgent = $_POST['isUrgent'] ? 1 : 0;
        $usrPassword = $_POST['password'];

        if($usrPassword != 'BFK!99') {
            $error = 'Incorrect password';
        }
        else {

            try {

                // If urgent, un-urgent the other announcements
                if($isUrgent == 1) {
                    $conn->query("UPDATE `announcement` SET isUrgent = 0");
                }

                $sqlInsert = "INSERT INTO `announcement` (`title`, `subtitle`, `description`, `isUrgent`) VALUES (?,?,?,?)";
                $stmtInsert = $conn->prepare($sqlInsert);
                if($stmtInsert === false) {
                    die('error inserting into database [prepare failed]: ' . htmlspecialchars($conn->error));
                }
                $bindResult = $stmtInsert->bind_param("ssss", $title, $subtitle, $description, $isUrgent);
                if($bindResult === false) {
                    die('error inserting into database [bind failed]: ' . htmlspecialchars($stmtInsert->error));
                }
                $executeResult = $stmtInsert->execute();
                if($executeResult === false) {
                    die('error inserting into database [execute failed]: ' . htmlspecialchars($stmtInsert->error));
                }

                // Success Message
                $success = 'Successfully added announcement!';
            }
            catch(Exception $e) {
                echo "<script>alert('Issue inserting into database: ".$e->getMessage()."');</script>";
            }

        }

        
    }

    // Get Announcements
    $res4 = $conn->query("SELECT * FROM announcement ORDER BY id DESC");
    $announcements = [];
    while ($row4 = $res4->fetch_assoc()) {
        array_push($announcements, $row4);
    }
    
    // Close Connection
    $conn->close();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Battle Fort Knox | BFK Warzone | NFT Marketplace</title>

    <script src="https://cdn.ckeditor.com/ckeditor5/29.2.0/classic/ckeditor.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/changelog-admin.css">

</head>
<body>
    
    <div class="form-container">

        <h1>BFK Warzone Announcements Admin Page</h1>

        <!-- Password -->
        <div class="section password-section">
            <!-- Error & Success Messages -->
            <?php if(isset($error) && $error != "") { ?>
                <div class="alert alert-danger" role="alert">
                    <i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
                </div>
            <?php } ?>
            <?php if(isset($success) && $success != "") { ?>
                <div class="alert alert-success" role="alert">
                    <i class="fa fa-check-circle"></i> <?php echo $success; ?>
                </div>
            <?php } ?>

            <h2><i class="fa fa-lock"></i> Password</h2>
            <input type="text" class="form-control" placeholder="Enter password" id="pagePassword" <?php if(isset($pw)) echo 'value="'.$pw.'"'; ?> />
        </div>

        <!-- Table -->
        <div class="section">

            <h2>Announcements</h2>

            <div class="table-responsive">
                <table class="table table-striped changelogs-table">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Subtitle</th>
                            <th>isUrgent</th>
                            <!-- <th>Created On</th> -->
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($announcements)) { foreach($announcements as $announcement) { ?>
                            <tr>
                                <td><?php echo $announcement['title']; ?></td>
                                <td><?php echo $announcement['subtitle']; ?></td>
                                <td><?php echo $announcement['isUrgent'] == 1 ? 'Yes' : 'No'; ?></td>
                                <!-- <td><?php echo $announcement['createdOn']; ?></td> -->
                                <td class="actions-row">
                                    <a class="btn btn-primary btn-sm locked" href="editAnnouncement.php?editId=<?php echo $announcement['id']; ?>">Edit</a>
                                    <?php if($announcement['isDeleted'] == 1) { ?>
                                        <form method="POST" action="operations/restoreAnnouncement.php" class="actionForm">
                                            <input type="hidden" name="password" class="passwordField" />
                                            <input type="hidden" name="id" value="<?php echo $announcement['id']; ?>" />
                                            <button class="btn btn-success btn-sm locked">Restore</button>
                                        </form>
                                    <?php } else { ?>
                                        <form method="POST" action="operations/deleteAnnouncement.php" class="actionForm">
                                            <input type="hidden" name="password" class="passwordField" />
                                            <input type="hidden" name="id" value="<?php echo $announcement['id']; ?>" />
                                            <button class="btn btn-danger btn-sm locked">Delete</button>
                                        </form>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>

        </div>

        <!-- Form -->
        <form class="add-changelog section" method="POST" action="announcements-admin.php" id="theForm">
    
            <h2>Add Announcement</h2>
    
            <!-- Title -->
            <div class="form-group">
                <label>* Title</label>
                <input class="form-control" placeholder="Enter title" name="title" required min="3" max="100" value="<?php echo $title; ?>" />
                <span class="form-error"></span>
            </div>

            <!-- Subtitle -->
            <div class="form-group">
                <label>* Subtitle</label>
                <textarea id="editor2" placeholder="Enter subtitle" name="subtitle"><?php echo $subtitle; ?></textarea>
                <span class="form-error"></span>
            </div>

            <!-- Description -->
            <div class="form-group">
                <label>Description (Optional)</label>
                <textarea id="editor" name="description" placeholder="Enter description"><?php echo $description; ?></textarea>
                <span class="form-error"></span>
            </div>

            <!-- Is Urgent -->
            <div class="form-group">
                <label>
                    <input type="checkbox" name="isUrgent" />
                    <span>show as urgent announcement?</span>
                </label>
                <span class="form-error"></span>
            </div>

            <input type="hidden" name="password" class="passwordField" />
    
            <!-- Submit -->
            <button type="submit" class="btn btn-primary" id="saveBtn">Add</button>

        </form>

    </div>

    <!-- Loader -->
    <div class="loader-wrapper">
        <div class="loader">
            <i class="fa fa-spinner loader-icon"></i>
        </div>
    </div>

</body>
</html>

<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }

    // Password
    const pagePassword = document.querySelector('#pagePassword');

    // Password Listener
    document.querySelectorAll('.passwordField').forEach(field => {
        field.value = pagePassword.value;
    })
    pagePassword.addEventListener('change', function(ev) {
        const enteredPassword = ev.target.value;
        const isCorrect = (enteredPassword == 'BFK!99');

        document.querySelectorAll('.passwordField').forEach(field => {
            field.value = enteredPassword;
        })
    })

    // On form submit, disable the button and show a loader
    const theForm = document.querySelector('#theForm');
    const saveBtn = document.querySelector('#saveBtn');
    const loader = document.querySelector('.loader-wrapper');
    loader.classList.remove('loader-wrapper--show');
    theForm.addEventListener('submit', function() {
        saveBtn.disabled = 'disabled';
        loader.classList.add('loader-wrapper--show');
    })

    // On Actions Clicked, disable the buttons
    const actionForms = document.querySelectorAll('.actionForm');
    const actionFormBtns = document.querySelectorAll('.locked');
    actionForms.forEach(actionForm => {
        actionForm.addEventListener('submit', function() {
            actionFormBtns.forEach(actionFromBtn => {
                actionFromBtn.disabled = 'disabled';
            })
        })
    })

</script>

<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ), {
            removePlugins: ['EasyImage', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed', 'Table']
        })
        .catch( error => {
            console.error( error );
        } );

    ClassicEditor
        .create( document.querySelector( '#editor2' ), {
            removePlugins: ['EasyImage', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed', 'Table']
        })
        .catch( error => {
            console.error( error );
        } );
</script>
