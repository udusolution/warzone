<?php

    include 'env.php';

    ini_set('display_errors', 1);
    error_reporting(-1);

    $conn = null;
    
    try {
        // Create connection
        $conn = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        // Fetch Changelogs
        $sql = "SELECT * FROM changelog WHERE isDeleted = 0 ORDER BY id DESC";
        $result = $conn->query($sql);
        $logs = [];
    
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                
                $log = $row;
    
                $sql2 = "SELECT `file` FROM changelog_image WHERE changelogId = " . $row["id"];
                $result2 = $conn->query($sql2);
    
                if ($result2->num_rows > 0) {
                    // Do not use fetch_all since it is not provided without installing mysqlind
                    // $rows2 = $result2->fetch_all();
                    // $log["images"] = $rows2;

                    $logImages = [];
                    while ($rowImage = $result2->fetch_row()) {
                        array_push($logImages, $rowImage);
                    }
                    $log["images"] = $logImages;
                }
    
                array_push($logs, $log);
    
    
            }
        }

        $conn->close();
    }
    catch(mysqli_sql_exception $e) {
        echo $e->message;
        if($conn) $conn->close();
    }




?>


<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Head -->
    <?php include '_head.php'; ?>

</head>

<body>

        <!-- Preloader -->
        <?php include '_preloader.php'; ?>

        <!-- Header -->
        <header class="third-header-bg">
            <div id="sticky-header">
                <div class="container custom-container desktop-header">
                    <div class="row">
                        <div class="col-12">
                            <div class="mobile-menu-wrap d-block d-lg-none">
                                <nav>
                                    <div id="mobile-menu" class="navbar-wrap">
                                        <ul>
                                            <li class="show"><a href="">Home</a></li>
                                            <!-- <li><a href="https://bfkwarzone.com" target="_blank">About</a></li> -->
                                            <li><a href="">BFK <span class="badge badge--blue">CONTRACT</span></a></li>
                                            <!-- <li><a href="bootcamp.php" target="_blank">Bootcamp</a></li> -->
                                            <li><a href="">Bootcamp <span class="badge badge--blue">EARLY VIEW</span></a></li>
                                            <li><a href="">Gamelog <span class="badge badge--blue">UPDATED</span></a></li>
                                            <li><a href="<?php echo $BFK_MARKETPLACE_LINK; ?>" target="_blank">Marketplace <span class="badge badge--green">ACTIVE</span></a></li>
                                            <li><a href="">Contact</a></li>
                                            <?php if(time() > $BFK_GAME_RELEASE) { ?>
                                                <li><a href="">Team</a></li>
                                            <?php } ?>
                                            <?php if(time() > $BFK_GAME_RELEASE) { ?>
                                                <li><a href="<?php echo $BFK_MARKETPLACE_LINK; ?>" target="_blank">Play Beta <span class="badge badge--red">LIVE</span></a></li>
                                            <?php } else { ?>
                                                <li><a onclick="showAlphaPopup()" class="clickable">Play Alpha</a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- main-area -->
        <main>

            <!-- Announcements -->
            <?php include '_announcements.php'; ?>

            <!-- breadcrumb-area -->
            <section class="breadcrumb-area breadcrumb-bg team-breadcrumbs  gameversion-section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb-content text-center">
                                <h2>GAMELOG <span class="primary-color">HISTORY</span></h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">GAMELOG</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="gameversion">
                        <div class="row gameversion-row">
                            <div class="col-12">
                                <div class="breadcrumb-content text-center">
                                    <h4>BFK Warzone Game Version</h4>
                                </div>
                                <div class="gameversion-span">
                                    <span>LIVE</span><h5>PUBLIC BETA V1 R1</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- breadcrumb-area-end -->

            <!-- Changelog -->
            <section id="changelog-section">

                <!-- Title -->
                <!-- <div class="block-title text-center">
                    <p>BFK Warzone Game Changelog</p>
                </div> -->

                

                <!-- List -->
                <div class="changelog__list" id="changelogList">
                    <?php if(isset($logs) && count($logs) == 0) { ?>
                        <p style="text-align: center;">No changelog yet</p>
                    <?php } ?>
                </div>

                <!-- Show More -->
                <div class="show-more-btn">
                    <button onclick="showMoreLogs()" class="btn-styled clickable">
                        <i class="fa fa-plus"></i> Show More
                    </button>
                </div>

            </section>

        </main>
        <!-- main-area-end -->

        <!-- Alert Popup -->
        <div class="alert-popup" id="alertPopup">
            <div class="alert-popup__relative-wrapper">
                <div class="alert-popup__overlay" onclick="closeAlertPopup()"></div>
                <div class="alert-popup__container">
                    <div class="alert-popup__close">
                        <i class="alert-popup__close-btn fa fa-times" onclick="closeAlertPopup()"></i>
                    </div>
                    <p class="alert-popup__description" id="alertPopupMessage">text</p>
                </div>
            </div>
        </div>
        
        <!-- Announcement Popup -->
        <div class="announcement-popup" id="announcementPopup">
            <div class="announcement-popup__relative-wrapper">
                <div class="announcement-popup__overlay" onclick="closeAnnouncementPopup()"></div>
                <div class="announcement-popup__container">
                    <div class="announcement-popup__close">
                        <i class="announcement-popup__close-btn fa fa-times" onclick="closeAnnouncementPopup()"></i>
                    </div>
                    <div class="announcement-popup__description" id="announcementPopupText">
                        <!-- <p><span class="primary-color">BFK ARMY!</span></p>
                        <p>$BFK will pause trading today Friday 18th February 2022 at 6PM EST / 11PM UTC. Please send in your tokens for migration for the V3 launch.</p>
                        <p><span class="primary-color">We have a simple and effective migration process;</span> simply follow the instructions below and you will receive your new tokens via airdrop before launch.</p>
                        <p><span class="primary-color">1. If you have not yet done so, add $BFK to your wallet:</span>
                            <br />0xEd44623b06616BCceC876617c124F5461Bd5f79B
                            <br />(THIS IS ONLY TOKEN ADDRESS - DO NOT SEND HERE)
                        </p>
                        <p><span class="primary-color">2. Copy the address below, and send all your $BFK tokens to THIS address:</span>
                            <br />0x4bE4F71Fa1eC64F9c4Bf321337229be7C6868B74
                            <br />(MIGRATION ADDRESS, SEND HERE)
                        </p>
                        <p>This means, send the tokens from whichever wallet you hold them, to the above address. Do NOT swap through Pancakeswap; SEND ALL $BFK TOKENS YOU HOLD TO THE ABOVE ADDRESS.</p>
                        <p> <span class="primary-color">3. Token supply will remain the same on BSC.</span> All current holders will retain EXACT same 1:1 ratio and amount tokens supply at the time of trading paused (ONLY IF YOU SEND IN FOR MIGRATION). There will be a vesting period for all airdropped holders of V2 to V3. The estimated vesting schedule is as below: Exact dates will be announced once we have the official V3 launch date.</p>
                        <p><span class="primary-color">VESTING PERIOD:</span>
                            <br />43% received at launch (100% OF CURRENT USD/BNB VALUE BEFORE TRADING IS PAUSED)
                            <br />10% after 30 days
                            <br />10% after 60 days
                            <br />10% after 90 days
                            <br />10% after 120 days
                            <br />10% after 150 days
                            <br />7% after 180 days
                        </p>
                        <p>Anybody that holds their tokens through the migration process will receive a total of 2.3X current token value, based on the new launch price over 180 days (6 month period). <span class="primary-color">Disclaimer: this may be less or more depending on the price at the time of receiving tokens.</span></p>
                        <p><span class="primary-color">4. We are providing all members at least 72 hours (or more) to complete this process.</span> We will not abandon anyone who doesn't make the deadline, but it may take longer to receive tokens if you miss this deadline.</p>
                        <p><span class="primary-color">BFK V3 BREAKDOWN:</span>
                            <br />The current price of $BFK is roughly $0.001 ($1 Million Market Capital). The presale price is estimated for $0.002 ($2 Million Market Capital) with a launch price of $0.0023 ($2.3 Million Market Capital). We will be working with several launchpads to raise a target of $500k - $1 Million for our V3 and ETH launch. 60% to Liquidity Pool, 20% to Marketing, 20% to Development. The MC-LP ratio will be higher which will allow for sustained growth and protect from heavy sell pressure. Update - we have turned off max transaction amount so you can send all your tokens in one transaction.
                        </p>
                        <p><span class="primary-color">$BFK V3 TOKENOMICS:</span>
                            <br />9% TOTAL Token Tax:
                        </p>
                        <p>1% P2E/Game Prizes (BFK)
                            <br />1% LP (AUTO OR MANUAL)
                            <br />3.5% Development (AUTOSWAP)
                            <br />3.5% Marketing (AUTOSWAP)
                        </p>
                        <p><span class="primary-color">NFT Marketplace Tax 3% (Each way Buy or Sell)</span>
                            <br />1% Ecosystem (BFK)
                            <br />1% Development (AUTOSWAP)
                            <br />1% Marketing (AUTOSWAP)
                        </p>
                        <p>Estimated launch date 4+ Weeks.</p>
                        <p><span class="primary-color">NFT and Game:</span></p>
                        <p>For NFT's Recovery / Account, Please make sure to visit the website and press on account recovery, fill in the information required, and submit the form.
                            <br />the information submitted will be used to provide the NEW minted NFT's with the new contract to the respective address at the same 1:1 value of floor price purchased.
                        </p>
                        <p>or use this link here: <a href="https://forms.gle/8LURhEHtH3RLfxmc9" target="_blank">https://forms.gle/8LURhEHtH3RLfxmc9</a></p>
                        <p><span class="primary-color">Please make sure to type in the correct information so we can have a smooth recovery at the time of launch V3</span>
                            <br />the website holds most of the updates that will be coming in the near future for the game and Contract.
                        </p>
                        <p><span class="primary-color">Considering the Server-side and marketplace, Server access is down</span> as we are deploying things and performing needed access keys, also the game-log will start showing updates soon, starting Monday we roll back to updating the logs, also we have added the development statement.</p>
                        <p>We're building the Future of 2D Shooter Games with BFK next Huge move. <img src="https://a.slack-edge.com/production-standard-emoji-assets/13.0/google-medium/2694-fe0f.png" /> <img src="https://a.slack-edge.com/production-standard-emoji-assets/13.0/google-medium/1f64c.png" /></p> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Scripts -->
        <?php include '_scripts.php'; ?>
        
    </body>

</html>

<script>

    // State
    let allLogs = [];
    let unShownLogs = [];
    let shownLogs = [];

    // Config
    const imgExt = ['png', 'jpg', 'jpeg', 'gif'];
    const audioExt = ['mp3', 'aac', 'wav', 'flac', 'ogg', 'weba'];
    const videoExt = ['mp4', 'webm'];
    const pageSize = 5;

    // DOM
    const changelogList = document.querySelector('#changelogList');
    const showMoreBtn = document.querySelector('.show-more-btn');

    // Fill Logs from DB
    <?php if(isset($logs)) foreach($logs as $log) {
        echo 'allLogs.push('.json_encode($log).');';
    } ?>

    allLogs = allLogs.map(l => {
        l.images = l.images?.map(i => i[0] ?? null) ?? [];
        return l;
    })
    unShownLogs = allLogs;

    /**
     * Show more logs (pagination "Show More" button handler)
     */
    function showMoreLogs() {

        // New logs to show
        let toAdd = [];
        
        // If there are no more logs to show, return
        if(unShownLogs.length == 0) return;

        // If number of logs to show is less than the page size
        if(unShownLogs.length <= pageSize) {

            // Add remaining un-shown logs to shown logs
            toAdd = [...unShownLogs];
            shownLogs = [...shownLogs, ...toAdd];

            // Empty the un-shown logs array
            unShownLogs = [];

            // Hide the "show more" button
            showMoreBtn.style.display = 'none';
        }
        // Else if remaining un-shown logs are more than the page size
        else {

            // Pop 5 un-shown logs into the shown logs array
            toAdd = [...unShownLogs.splice(0, pageSize)];
            shownLogs = [...shownLogs, ...toAdd];
        }

        // Add new logs to DOM
        addLogsToDOM(toAdd);
    }

    /**
     * apply date filter to logs
     * show logs that are only X days old
     */
    function applyFilter(showDaysAgo) {

        // Get selected filter value
        const daysRangeToFilterBy = parseInt(showDaysAgo);

        // Filter the logs that are within the date range (all if "0")
        unShownLogs = daysRangeToFilterBy == 0
            ? [...allLogs]
            : allLogs.filter(log => Math.round(((new Date()) - Date.parse(log.createdOn)) / 86400000) < daysRangeToFilterBy);

        // Pop the first 5 un-shown logs to the shown logs array
        shownLogs = unShownLogs.splice(0, pageSize);

        // Clear the current shown logs from DOM
        changelogList.innerHTML = '';

        // Show the new logs
        addLogsToDOM(shownLogs);

        // If there are un-shown logs left, show the "show-more" button, else hide it
        if(unShownLogs.length > 0) {
            showMoreBtn.style.display = 'flex';
        }
        else {
            showMoreBtn.style.display = 'none';
        }
        
    }

    /**
     * Add logs to DOM
     * @param {array} logsToAdd logs to add
     */
    function addLogsToDOM(logsToAdd) {
        if(logsToAdd.length > 0) {
            changelogList.innerHTML = `<div class="changelog__filters">
                    <div class="changelog__filter">
                        <div class="changelog__filter__label"><i class="fa fa-calendar"></i> Filter</div>
                        <select class="changelog__filter__input--select" onchange="applyFilter(this.value)">
                            <option value="0" selected>All</option>
                            <option value="3">Last 3 days</option>
                            <option value="7">Last week</option>
                            <option value="14">Last 2 weeks</option>
                            <option value="30">Last month</option>
                        </select>
                    </div>
                </div>`;
            logsToAdd.forEach(l => {
                changelogList.innerHTML += `

                <div class="changelog" id="card-${l.id}">
                    <div class="changelog__top" onclick="toggleCard('card-${l.id}')">
                        <span>${l.version} - ${l.title} - ${l.date}</span>
                        <i class="fa fa-plus card-toggle"></i>    
                    </div>
                    <div class="changelog__bottom changelog__bottom--closed">
                        <div class="changelog__description">${l.description}</div>
                        ${l.images?.length > 0 ? (
                            `
                                <div class="changelog__audio">
                                    ${l.images.filter(i => audioExt.some(ext => ext == i.split('.').pop().toLowerCase())).map(i => `<audio controls src="${i}"></audio>`)}
                                </div>
                                <div class="changelog__images">
                                    ${l.images.filter(i => imgExt.some(ext => ext == i.split('.').pop().toLowerCase())).map(i => `<img src="${i}" loading="lazy" />`)}
                                </div>
                                <div class="changelog__videos">
                                    ${l.images.filter(i => videoExt.some(ext => ext == i.split('.').pop().toLowerCase())).map(i => `<video src="${i}" controls></video>`)}
                                </div>
                            `
                        ) : ''}
                    </div>
                </div>
                `;
            })
        }
    }
    
    /**
     * open/close a card (log)
     */
    function toggleCard(cardId) {

        console.log('TOGGLE')

        const cardDOM = document.querySelector(`#${cardId}`);
        const top = cardDOM.querySelector('.changelog__top');
        const bottom = cardDOM.querySelector('.changelog__bottom');
        const toggle = top.querySelector('.card-toggle');

        if(bottom.classList.contains('changelog__bottom--closed')) {
            bottom.classList.remove('changelog__bottom--closed');
            toggle.classList.remove('fa-minus');
            toggle.classList.remove('fa-plus');
            toggle.classList.add('fa-minus');
        }
        else {
            bottom.classList.add('changelog__bottom--closed');
            toggle.classList.remove('fa-minus');
            toggle.classList.remove('fa-plus');
            toggle.classList.add('fa-plus');
        }

    }
    
    // Init
    applyFilter(0);

    // Turn links to blank target
    const links = changelogList.querySelectorAll('a[href]');
    links.forEach(l => {
        l.target = '_blank';
    })

</script>