<footer>
    <div class="footer-top footer-bg s-footer-bg">
        <!-- newsletter-area -->
        <div class="newsletter-area s-newsletter-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="newsletter-wrap">
                            <div class="section-title newsletter-title">
                                <h2>Game <span>Updates</span></h2>
                            </div>
                            <div class="newsletter-form">
                                <form id="subscriptionForm">
                                    <div class="newsletter-form-grp">
                                        <i class="far fa-envelope"></i>
                                        <input type="email" placeholder="Enter your email..." required />
                                    </div>
                                    <button>SUBSCRIBE <i class="fas fa-paper-plane"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- newsletter-area-end -->
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <div class="footer-widget mb-50">
                        <div class="footer-logo mb-35">
                            <a href="index.php"><img loading="lazy" src="img/logo/logo_secondary.png" alt=""></a>
                        </div>
                        <div class="footer-text">
                            <p>BFK Warzone the First Play to Earn NFT Ecosystem Game on the BSC Smartchain.</p>
                            <div class="footer-contact">
                                <ul>
                                    <li><i class="fas fa-map-marker-alt"></i><span>Address : </span><?php echo $ADDRESS; ?></li>
                                    <li><i class="fas fa-phone"></i><span>Phone : </span><a href="tel:<?php echo $PHONE; ?>"><?php echo $PHONE; ?></a></li>
                                    <li><i class="fas fa-envelope-open"></i><span>Email : </span><a href="mailto:<?php echo $EMAIL; ?>"><?php echo $EMAIL; ?></a></li>
                                    <li><i class="fas fa-building"></i><span>TGC International FZ-LLC Studios</li>
                                    <li><img class="footer-license-icon" loading="lazy" src="img/icon/license.png" alt=""><span>BFK WARZONE | licensed &amp; Copyrighted in the UAE</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-sm-6">
                    <div class="footer-widget mb-50">
                        <div class="fw-title mb-35">
                            <h5>Explore</h5>
                        </div>
                        <div class="fw-link">
                            <ul>
                                <li><a href="javascript:openAlertPopup(`<img class='copyright-img' src='img/copyright/copyright.png' alt='' height='100%' width='100%'>`)">Copyright</a></li>
                                <li><a href="https://bfkwarzone.com/changelog.php" target="_blank">Gamelog</a></li>
                                <li><a href="https://drive.google.com/drive/folders/1tb-mkCMoNi5Ay7Jjvd8ZirtchUPg9psp?usp=sharing" target="_blank">Deck</a></li>
                                <li><a href="files/gdd.pdf" target="_blank" download>GDD Document</a></li>
                                <li><a href="https://drive.google.com/drive/folders/1tb-mkCMoNi5Ay7Jjvd8ZirtchUPg9psp?usp=sharing" target="_blank" download>Whitepaper</a></li>
                                <!-- <li><button onclick="showAlphaPopup()" class="footer-alpha-game-link">Play Alpha Game</button></li> -->
                                <li><button onclick="openInNewTab('<?php echo $BFK_MARKETPLACE_LINK; ?>')" class="footer-alpha-game-link">Play Beta Game</button></li>
                                <li>
                                    <a href="https://www.cgc.one/" target="_blank">
                                        <img class="footer-brand" src="img/logos/CGC.jpg" />
                                    </a>
                                </li>
                                <li>
                                    <a href="https://cryptoexpodubai.com/" target="_blank">
                                        <img class="footer-brand" src="img/brand/cryptoexpo.png" />
                                    </a>
                                </li>
                                <li>
                                    <a href="https://tresconglobal.com/conferences/blockchain/" target="_blank">
                                        <img class="footer-brand" src="img/brand/wbs.png" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-sm-6">
                    <div class="footer-widget mb-50">
                        <div class="fw-title mb-35">
                            <h5>Links</h5>
                        </div>
                        <div class="fw-link">
                            <ul>
                                <li>
                                    <a target="_blank" class="telegram-trigger" tabindex>
                                            
                                        <!-- Telegram Popup -->
                                        <div class="telegram-popup telegram-popup--right">
                                            <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_OFFICIAL_LINK; ?>')"><i class="fab fa-telegram purify"></i>Official</div>
                                            <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_ID_LINK; ?>')"><i class="fab fa-telegram purify"></i>Indonesian Group</div>
                                            <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_JP_LINK; ?>')"><i class="fab fa-telegram purify"></i>Japanese Group</div>
                                            <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_TR_LINK; ?>')"><i class="fab fa-telegram purify"></i>Turkish Group</div>
                                            <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_SHILL_LINK; ?>')"><i class="fab fa-telegram purify"></i>Shill Squad</div>
                                            <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_ANNOUNCEMENTS_LINK; ?>')"><i class="fab fa-telegram purify"></i>Announcements</div>
                                        </div>
                                        
                                        <b>Telegram <i class="fa fa-caret-right primary-color"></i></b>
                                    </a>
                                </li>
                                <li><a href="<?php echo $BFK_SUPPORT_LINK; ?>" target="_blank">Support & Feedback</a></li>
                                <li><a href="<?php echo $BFK_ACCOUNT_RECOVERY_LINK; ?>" target="_blank">Account Recovery</a></li>
                                <li><a href="https://links.bfkwarzone.com/" target="_blank">Multi-Links</a></li>
                                <li><button onclick="openInNewTab('<?php echo $BFK_CERTIK_LINK; ?>')" class="footer-alpha-game-link">Certik</button></li>
                                <!-- <li><a href="<?php echo $BFK_GITHUB_LINK; ?>" target="_blank">Github</a></li> -->
                            </ul>
                        </div>
                    </div>
                    <div class="footer-widget mb-50">
                        <div class="fw-title mb-35">
                            <h5>Legal</h5>
                        </div>
                        <div class="fw-link">
                            <ul>
                                <li><a href="files/Website_Terms_and_Conditions.pdf" target="_blank">Terms and Conditions</a></li>
                                <li><a href="files/Website_Privacy_Policy.pdf" target="_blank">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <div class="footer-widget mb-50">
                        <div class="fw-title mb-35">
                            <h5>Follow us</h5>
                        </div>
                        <div class="footer-social">
                            <ul>
                                <!-- <li><a class="flex-center" href="<?php echo $BFK_TELEGRAM_OFFICIAL_LINK; ?>" target="_blank"><i class="fab fa-telegram"></i></a></li> -->
                                <li>
                                    <a target="_blank" class="telegram-trigger" tabindex>
                                            
                                        <!-- Telegram Popup -->
                                        <div class="telegram-popup telegram-popup--right">
                                            <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_OFFICIAL_LINK; ?>')"><i class="fab fa-telegram purify"></i>Official</div>
                                            <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_ID_LINK; ?>')"><i class="fab fa-telegram purify"></i>Indonesian Group</div>
                                            <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_JP_LINK; ?>')"><i class="fab fa-telegram purify"></i>Japanese Group</div>
                                            <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_TR_LINK; ?>')"><i class="fab fa-telegram purify"></i>Turkish Group</div>
                                            <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_SHILL_LINK; ?>')"><i class="fab fa-telegram purify"></i>Shill Squad</div>
                                            <div onclick="openInNewTab('<?php echo $BFK_TELEGRAM_ANNOUNCEMENTS_LINK; ?>')"><i class="fab fa-telegram purify"></i>Announcements</div>
                                        </div>
                                        
                                        <i class="fab fa-telegram absolute-center"></i>
                                    </a>
                                </li>

                                <li><a class="flex-center" href="<?php echo $BFK_FACEBOOK_LINK; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a class="flex-center" href="<?php echo $BFK_TWITTER_LINK; ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a class="flex-center" href="<?php echo $BFK_INSTAGRAM_LINK; ?>" target="_blank" style="background-color: #c13584;"><i class="fab fa-instagram"></i></a></li>
                                <li><a class="flex-center" href="<?php echo $BFK_YT_LINK; ?>" target="_blank" style="background-color: red;"><i class="fab fa-youtube"></i></a></li>
                                <li><a class="flex-center" href="<?php echo $BFK_DISCORD_LINK; ?>" target="_blank" style="background-color: #5865F2;"><i class="fab fa-discord"></i></a></li>
                                <li><a class="flex-center mt-2" href="<?php echo $BFK_TWITCH_LINK; ?>" target="_blank" style="background-color: #6441a5;"><i class="fab fa-twitch"></i></a></li>
                                <li><a class="flex-center mt-2" href="<?php echo $BFK_TIKTOK_LINK; ?>" target="_blank" style="background-color: #FFFFFF;"><i class="fab fa-tiktok mb-3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!-- Font Awesome Pro 5.15.4 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) --><path fill="#010101" d="M448,209.91a210.06,210.06,0,0,1-122.77-39.25V349.38A162.55,162.55,0,1,1,185,188.31V278.2a74.62,74.62,0,1,0,52.23,71.18V0l88,0a121.18,121.18,0,0,0,1.86,22.17h0A122.18,122.18,0,0,0,381,102.39a121.43,121.43,0,0,0,67,20.14Z"/></svg></i></a></li>
                            </ul>
                        </div>
                        <!-- <div class="footer-widget mt-10">
                            <iframe src="https://discord.com/widget?id=975422465176109096&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>
                        </div> -->
                    </div>
                    <div class="footer-widget mb-50">
                        <div class="fw-title mb-35">
                            <h5>Early Access Sign Up</h5>
                        </div>
                        <div class="footer-newsletter">
                            <form id="subscriptionForm2">
                                <input type="text" placeholder="Enter your email" required />
                                <button><i class="fas fa-rocket"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-fire"><img loading="lazy" src="img/footer-left.png" alt=""></div>
        <div class="footer-fire footer-fire-right"><img loading="lazy" src="img/footer-right.png" alt=""></div>
    </div>
    <div class="copyright-wrap s-copyright-wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="copyright-text">
                        <p>BFK Warzone 2D Shooter Game | NFT Play to Earn © All Rights Reserved 2022 | Battle Fort Knox (<a href="https://bfkwarzone.com" target="_blank" class="primary-color">$BFK</a>) | TGC international FZ-LLC Studios</p>
                    </div>
                </div>
                <!-- <div class="col-lg-6 col-md-6 d-none d-md-block">
                    <div class="payment-method-img text-right">
                        <img src="img/images/card_img.png" alt="img">
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</footer>

<!-- Alert Popup -->
<div class="alert-popup" id="alertPopup">
    <div class="alert-popup__relative-wrapper">
        <div class="alert-popup__overlay" onclick="closeAlertPopup()"></div>
        <div class="alert-popup__container">
            <div class="alert-popup__close">
                <i class="alert-popup__close-btn fa fa-times" onclick="closeAlertPopup()"></i>
            </div>
            <p class="alert-popup__description" id="alertPopupMessage">text</p>
        </div>
    </div>
</div>

<!-- Announcement Popup -->
<div class="announcement-popup" id="announcementPopup">
    <div class="announcement-popup__relative-wrapper">
        <div class="announcement-popup__overlay" onclick="closeAnnouncementPopup()"></div>
        <div class="announcement-popup__container">
            <div class="announcement-popup__close">
                <i class="announcement-popup__close-btn fa fa-times" onclick="closeAnnouncementPopup()"></i>
            </div>
            <div class="announcement-popup__description" id="announcementPopupText">
                <!-- <p><span class="primary-color">BFK ARMY!</span></p>
                <p>$BFK will pause trading today Friday 18th February 2022 at 6PM EST / 11PM UTC. Please send in your tokens for migration for the V3 launch.</p>
                <p><span class="primary-color">We have a simple and effective migration process;</span> simply follow the instructions below and you will receive your new tokens via airdrop before launch.</p>
                <p><span class="primary-color">1. If you have not yet done so, add $BFK to your wallet:</span>
                    <br />0xEd44623b06616BCceC876617c124F5461Bd5f79B
                    <br />(THIS IS ONLY TOKEN ADDRESS - DO NOT SEND HERE)
                </p>
                <p><span class="primary-color">2. Copy the address below, and send all your $BFK tokens to THIS address:</span>
                    <br />0x4bE4F71Fa1eC64F9c4Bf321337229be7C6868B74
                    <br />(MIGRATION ADDRESS, SEND HERE)
                </p>
                <p>This means, send the tokens from whichever wallet you hold them, to the above address. Do NOT swap through Pancakeswap; SEND ALL $BFK TOKENS YOU HOLD TO THE ABOVE ADDRESS.</p>
                <p> <span class="primary-color">3. Token supply will remain the same on BSC.</span> All current holders will retain EXACT same 1:1 ratio and amount tokens supply at the time of trading paused (ONLY IF YOU SEND IN FOR MIGRATION). There will be a vesting period for all airdropped holders of V2 to V3. The estimated vesting schedule is as below: Exact dates will be announced once we have the official V3 launch date.</p>
                <p><span class="primary-color">VESTING PERIOD:</span>
                    <br />43% received at launch (100% OF CURRENT USD/BNB VALUE BEFORE TRADING IS PAUSED)
                    <br />10% after 30 days
                    <br />10% after 60 days
                    <br />10% after 90 days
                    <br />10% after 120 days
                    <br />10% after 150 days
                    <br />7% after 180 days
                </p>
                <p>Anybody that holds their tokens through the migration process will receive a total of 2.3X current token value, based on the new launch price over 180 days (6 month period). <span class="primary-color">Disclaimer: this may be less or more depending on the price at the time of receiving tokens.</span></p>
                <p><span class="primary-color">4. We are providing all members at least 72 hours (or more) to complete this process.</span> We will not abandon anyone who doesn't make the deadline, but it may take longer to receive tokens if you miss this deadline.</p>
                <p><span class="primary-color">BFK V3 BREAKDOWN:</span>
                    <br />The current price of $BFK is roughly $0.001 ($1 Million Market Capital). The presale price is estimated for $0.002 ($2 Million Market Capital) with a launch price of $0.0023 ($2.3 Million Market Capital). We will be working with several launchpads to raise a target of $500k - $1 Million for our V3 and ETH launch. 60% to Liquidity Pool, 20% to Marketing, 20% to Development. The MC-LP ratio will be higher which will allow for sustained growth and protect from heavy sell pressure. Update - we have turned off max transaction amount so you can send all your tokens in one transaction.
                </p>
                <p><span class="primary-color">$BFK V3 TOKENOMICS:</span>
                    <br />9% TOTAL Token Tax:
                </p>
                <p>1% P2E/Game Prizes (BFK)
                    <br />1% LP (AUTO OR MANUAL)
                    <br />3.5% Development (AUTOSWAP)
                    <br />3.5% Marketing (AUTOSWAP)
                </p>
                <p><span class="primary-color">NFT Marketplace Tax 3% (Each way Buy or Sell)</span>
                    <br />1% Ecosystem (BFK)
                    <br />1% Development (AUTOSWAP)
                    <br />1% Marketing (AUTOSWAP)
                </p>
                <p>Estimated launch date 4+ Weeks.</p>
                <p><span class="primary-color">NFT and Game:</span></p>
                <p>For NFT's Recovery / Account, Please make sure to visit the website and press on account recovery, fill in the information required, and submit the form.
                    <br />the information submitted will be used to provide the NEW minted NFT's with the new contract to the respective address at the same 1:1 value of floor price purchased.
                </p>
                <p>or use this link here: <a href="https://forms.gle/8LURhEHtH3RLfxmc9" target="_blank">https://forms.gle/8LURhEHtH3RLfxmc9</a></p>
                <p><span class="primary-color">Please make sure to type in the correct information so we can have a smooth recovery at the time of launch V3</span>
                    <br />the website holds most of the updates that will be coming in the near future for the game and Contract.
                </p>
                <p><span class="primary-color">Considering the Server-side and marketplace, Server access is down</span> as we are deploying things and performing needed access keys, also the game-log will start showing updates soon, starting Monday we roll back to updating the logs, also we have added the development statement.</p>
                <p>We're building the Future of 2D Shooter Games with BFK next Huge move. <img src="https://a.slack-edge.com/production-standard-emoji-assets/13.0/google-medium/2694-fe0f.png" /> <img src="https://a.slack-edge.com/production-standard-emoji-assets/13.0/google-medium/1f64c.png" /></p> -->
            </div>
        </div>
    </div>
</div>

<script>

    /**
     * Subscription Form
     */

    const subscriptionForm = document.querySelector('#subscriptionForm');
    const subscriptionForm2 = document.querySelector('#subscriptionForm2');

    subscriptionForm.addEventListener('submit', onSub);
    subscriptionForm2.addEventListener('submit', onSub);
    
    function onSub(ev) {

        console.log('In Subscription Form Submit Listener');

        const inputDOM = ev.target.querySelector('input');
        const btn = ev.target.querySelector('button');

        // Prevent Default
        ev.preventDefault();

        // Data
        const data = {
            email: inputDOM.value
        }

        // Validate
        console.log(data);

        // Disable Send Button
        btn.disabled = 'disabled';

        // Send
        $.ajax({
            url: 'sendSubscriptionEmail.php',
            async: true,
            cache: false,
            type: 'POST',
            dataType: "json",
            data: {
                data: JSON.stringify(data)
            },
            success: function(response) {
                if(response.status == 'success') {
                    console.log(response);
                    resetSubscriptionForm();
                    btn.innerHTML = '<i class="fa fa-check-circle"></i>';
                }
                else if(response.status == 'error') {
                    console.error(response);
                    btn.disabled = false;
                    btn.innerHTML = 'Something went wrong';
                }
                else {
                    console.info(response);
                    btn.disabled = false;
                }
            },
            error: function(err) {
                console.error(err);
                btn.disabled = false;
                btn.innerHTML = 'Something went wrong';
            }
        })

        function resetSubscriptionForm() {
            inputDOM.value = '';
        }

    }

</script>