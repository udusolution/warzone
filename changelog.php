<?php

    include 'env.php';

    ini_set('display_errors', 1);
    error_reporting(-1);

    $conn = null;
    
    try {
        // Create connection
        $conn = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        // Fetch Changelogs
        $sql = "SELECT * FROM changelog WHERE isDeleted = 0 ORDER BY id DESC";
        $result = $conn->query($sql);
        $logs = [];
    
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                
                $log = $row;
    
                $sql2 = "SELECT `file` FROM changelog_image WHERE changelogId = " . $row["id"];
                $result2 = $conn->query($sql2);
    
                if ($result2->num_rows > 0) {
                    // Do not use fetch_all since it is not provided without installing mysqlind
                    // $rows2 = $result2->fetch_all();
                    // $log["images"] = $rows2;

                    $logImages = [];
                    while ($rowImage = $result2->fetch_row()) {
                        array_push($logImages, $rowImage);
                    }
                    $log["images"] = $logImages;
                }
    
                array_push($logs, $log);
    
    
            }
        }

        $conn->close();
    }
    catch(mysqli_sql_exception $e) {
        echo $e->message;
        if($conn) $conn->close();
    }




?>


<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Head -->
    <?php include '_head.php'; ?>

</head>

<body>

        <!-- Preloader -->
        <?php include '_preloader.php'; ?>

        <!-- Header -->
        <?php include '_header.php'; ?>

        <!-- main-area -->
        <main>

            <!-- Announcements -->
            <?php include '_announcements.php'; ?>

            <!-- breadcrumb-area -->
            <section class="breadcrumb-area breadcrumb-bg team-breadcrumbs  gameversion-section">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb-content text-center">
                                <h2>GAMELOG <span class="primary-color">HISTORY</span></h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">GAMELOG</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="gameversion">
                        <div class="row gameversion-row">
                            <div class="col-12">
                                <div class="breadcrumb-content text-center">
                                    <h4>BFK Warzone Game Version</h4>
                                </div>
                                <div class="gameversion-span">
                                    <span>LIVE</span><h5>PUBLIC BETA V1 R1</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- breadcrumb-area-end -->

            <!-- Changelog -->
            <section id="changelog-section">

                <!-- Title -->
                <!-- <div class="block-title text-center">
                    <p>BFK Warzone Game Changelog</p>
                </div> -->

                

                <!-- List -->
                <div class="changelog__list" id="changelogList">
                    <?php if(isset($logs) && count($logs) == 0) { ?>
                        <p style="text-align: center;">No changelog yet</p>
                    <?php } ?>
                </div>

                <!-- Show More -->
                <div class="show-more-btn">
                    <button onclick="showMoreLogs()" class="btn-styled clickable">
                        <i class="fa fa-plus"></i> Show More
                    </button>
                </div>

            </section>

        </main>
        <!-- main-area-end -->

        <!-- Footer -->
        <?php include '_footer.php'; ?>

        <!-- Scripts -->
        <?php include '_scripts.php'; ?>
        
    </body>

</html>

<script>

    // State
    let allLogs = [];
    let unShownLogs = [];
    let shownLogs = [];

    // Config
    const imgExt = ['png', 'jpg', 'jpeg', 'gif'];
    const audioExt = ['mp3', 'aac', 'wav', 'flac', 'ogg', 'weba'];
    const videoExt = ['mp4', 'webm'];
    const pageSize = 5;

    // DOM
    const changelogList = document.querySelector('#changelogList');
    const showMoreBtn = document.querySelector('.show-more-btn');

    // Fill Logs from DB
    <?php if(isset($logs)) foreach($logs as $log) {
        echo 'allLogs.push('.json_encode($log).');';
    } ?>

    allLogs = allLogs.map(l => {
        l.images = l.images?.map(i => i[0] ?? null) ?? [];
        return l;
    })
    unShownLogs = allLogs;

    /**
     * Show more logs (pagination "Show More" button handler)
     */
    function showMoreLogs() {

        // New logs to show
        let toAdd = [];
        
        // If there are no more logs to show, return
        if(unShownLogs.length == 0) return;

        // If number of logs to show is less than the page size
        if(unShownLogs.length <= pageSize) {

            // Add remaining un-shown logs to shown logs
            toAdd = [...unShownLogs];
            shownLogs = [...shownLogs, ...toAdd];

            // Empty the un-shown logs array
            unShownLogs = [];

            // Hide the "show more" button
            showMoreBtn.style.display = 'none';
        }
        // Else if remaining un-shown logs are more than the page size
        else {

            // Pop 5 un-shown logs into the shown logs array
            toAdd = [...unShownLogs.splice(0, pageSize)];
            shownLogs = [...shownLogs, ...toAdd];
        }

        // Add new logs to DOM
        addLogsToDOM(toAdd);
    }

    /**
     * apply date filter to logs
     * show logs that are only X days old
     */
    function applyFilter(showDaysAgo) {

        // Get selected filter value
        const daysRangeToFilterBy = parseInt(showDaysAgo);

        // Filter the logs that are within the date range (all if "0")
        unShownLogs = daysRangeToFilterBy == 0
            ? [...allLogs]
            : allLogs.filter(log => Math.round(((new Date()) - Date.parse(log.createdOn)) / 86400000) < daysRangeToFilterBy);

        // Pop the first 5 un-shown logs to the shown logs array
        shownLogs = unShownLogs.splice(0, pageSize);

        // Clear the current shown logs from DOM
        changelogList.innerHTML = '';

        // Show the new logs
        addLogsToDOM(shownLogs);

        // If there are un-shown logs left, show the "show-more" button, else hide it
        if(unShownLogs.length > 0) {
            showMoreBtn.style.display = 'flex';
        }
        else {
            showMoreBtn.style.display = 'none';
        }
        
    }

    /**
     * Add logs to DOM
     * @param {array} logsToAdd logs to add
     */
    function addLogsToDOM(logsToAdd) {
        if(logsToAdd.length > 0) {
            changelogList.innerHTML = `<div class="changelog__filters">
                    <div class="changelog__filter">
                        <div class="changelog__filter__label"><i class="fa fa-calendar"></i> Filter</div>
                        <select class="changelog__filter__input--select" onchange="applyFilter(this.value)">
                            <option value="0" selected>All</option>
                            <option value="3">Last 3 days</option>
                            <option value="7">Last week</option>
                            <option value="14">Last 2 weeks</option>
                            <option value="30">Last month</option>
                        </select>
                    </div>
                </div>`;
            logsToAdd.forEach(l => {
                changelogList.innerHTML += `

                <div class="changelog" id="card-${l.id}">
                    <div class="changelog__top" onclick="toggleCard('card-${l.id}')">
                        <span>${l.version} - ${l.title} - ${l.date}</span>
                        <i class="fa fa-plus card-toggle"></i>    
                    </div>
                    <div class="changelog__bottom changelog__bottom--closed">
                        <div class="changelog__description">${l.description}</div>
                        ${l.images?.length > 0 ? (
                            `
                                <div class="changelog__audio">
                                    ${l.images.filter(i => audioExt.some(ext => ext == i.split('.').pop().toLowerCase())).map(i => `<audio controls src="${i}"></audio>`)}
                                </div>
                                <div class="changelog__images">
                                    ${l.images.filter(i => imgExt.some(ext => ext == i.split('.').pop().toLowerCase())).map(i => `<img src="${i}" loading="lazy" />`)}
                                </div>
                                <div class="changelog__videos">
                                    ${l.images.filter(i => videoExt.some(ext => ext == i.split('.').pop().toLowerCase())).map(i => `<video src="${i}" controls></video>`)}
                                </div>
                            `
                        ) : ''}
                    </div>
                </div>
                `;
            })
        }
    }
    
    /**
     * open/close a card (log)
     */
    function toggleCard(cardId) {

        console.log('TOGGLE')

        const cardDOM = document.querySelector(`#${cardId}`);
        const top = cardDOM.querySelector('.changelog__top');
        const bottom = cardDOM.querySelector('.changelog__bottom');
        const toggle = top.querySelector('.card-toggle');

        if(bottom.classList.contains('changelog__bottom--closed')) {
            bottom.classList.remove('changelog__bottom--closed');
            toggle.classList.remove('fa-minus');
            toggle.classList.remove('fa-plus');
            toggle.classList.add('fa-minus');
        }
        else {
            bottom.classList.add('changelog__bottom--closed');
            toggle.classList.remove('fa-minus');
            toggle.classList.remove('fa-plus');
            toggle.classList.add('fa-plus');
        }

    }
    
    // Init
    applyFilter(0);

    // Turn links to blank target
    const links = changelogList.querySelectorAll('a[href]');
    links.forEach(l => {
        l.target = '_blank';
    })

</script>