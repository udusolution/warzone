
<!doctype html>
<html class="no-js" lang="en">
    <head>
        
        <!-- Head -->
        <?php include '_head.php'; ?>

    </head>
    <body>

        <!-- Preloader -->
        <?php include '_preloader.php'; ?>

        <!-- Header -->
        <?php include '_header.php'; ?>

        <!-- main-area -->
        <main>

            <!-- Announcements -->
            <?php include '_announcements.php'; ?>

            <!-- slider-area -->
            <section class="slider-area home-four-slider">
                <div class="slider-active slider-wrapper-bg">

                    <div class="single-slider slider-bg slider-style-two" style="background-image: url('img/slider/featured4.png')">
                        <div class="container custom-container-two">
                            <div class="row">
                                <div class="col-xl-6 col-lg-7 col-md-11">
                                    <div class="slider-content">
                                        <h6 data-animation="fadeInUp" data-delay=".4s" class="">BFK WARZONE</h6>
                                        <h2 data-animation="fadeInUp" data-delay=".4s">ORIGINAL MERCH <br><span class="">STORE LIVE</span></h2>
                                        <p data-animation="fadeInUp" data-delay=".6s">BFK WARZONE original Merchandies Store, buy cool gadgets, Fortis Figurines, Shirts and more all in one place. Defend the fort and get cool products!</p>
                                        <a href="<?php echo $BFK_STORE_LINK; ?>" class="btn btn-style-two" data-animation="fadeInUp" data-delay=".8s">SHOP NOW</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="single-slider slider-bg slider-style-two" style="background-image: url('img/slider/featured3.png')">
                        <div class="container custom-container-two">
                            <div class="row">
                                <div class="col-xl-6 col-lg-7 col-md-11">
                                    <div class="slider-content">
                                        <h6 data-animation="fadeInUp" data-delay=".4s" class="">GAMING SERIES</h6>
                                        <h2 data-animation="fadeInUp" data-delay=".4s">10th EDITION GAMING <span class="">BLOCKCHAIN CONFERENCE</span></h2>
                                        <p data-animation="fadeInUp" data-delay=".6s">BFK WARZONE will be Sponsoring the 10th edition of the World Gaming Blockchain Conference online</p>
                                        <div class="main-slider-img-mobile img-loader">
                                            <img src="img/slider/CGCBFK.jpg" loading="lazy" alt="" data-animation="slideInRightS" data-delay=".8s" style="border-radius: 8px;">
                                        </div>
                                        <a href="https://www.cgc.one/" target="_blank" class="btn btn-style-two" data-animation="fadeInUp" data-delay=".8s">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-img" style="right: 15%">
                                <div class="img-loader" onclick="openInNewTab('<?php echo $BFK_CGC_LINK;?>')">
                                    <img src="img/slider/CGCBFK.jpg" loading="lazy" alt="" data-animation="slideInRightS" data-delay=".8s" style="width: 350px; border-radius: 8px;">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="single-slider slider-bg slider-style-two" style="background-image: url('img/slider/featured1.jpg')">
                        <div class="container custom-container-two">
                            <div class="row">
                                <div class="col-xl-6 col-lg-7 col-md-11">
                                    <div class="slider-content">
                                        <h6 data-animation="fadeInUp" data-delay=".4s" class="">GLOBAL SERIES</h6>
                                        <h2 data-animation="fadeInUp" data-delay=".4s">WORLD BLOCKCHAIN <span class="">SUMMIT 2022</span></h2>
                                        <p data-animation="fadeInUp" data-delay=".6s">BFK Warzone will be holding a 5 min pitch at the World Blockchain Summit showcasing the game for potential investors</p>
                                        <div class="main-slider-img-mobile img-loader" onclick="openInNewTab('<?php echo $BFK_SUMMITLOGO_LINK;?>')">
                                            <img src="img/slider/WBSsponsor.jpeg" loading="lazy" alt="" data-animation="slideInRightS" data-delay=".8s" style="border-radius: 8px;">
                                        </div>
                                        <a href="https://tresconglobal.com/conferences/blockchain/" target="_blank" class="btn btn-style-two" data-animation="fadeInUp" data-delay=".8s">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-img" style="right: 15%">
                                <div class="img-loader" onclick="openInNewTab('<?php echo $BFK_SUMMITLOGO_LINK;?>')">
                                    <img src="img/slider/WBSsponsor.jpeg" loading="lazy" alt="" data-animation="slideInRightS" data-delay=".8s" style="width: 350px; border-radius: 8px;">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="single-slider slider-bg slider-style-two" style="background-image: url('img/slider/featured2.jpg')">
                        <div class="container custom-container-two">
                            <div class="row">
                                <div class="col-xl-6 col-lg-7 col-md-11">
                                    <div class="slider-content">
                                        <h6 data-animation="fadeInUp" data-delay=".4s" class="">GLOBAL SERIES</h6>
                                        <h2 data-animation="fadeInUp" data-delay=".4s">CRYPTO EXPO <span class="">DUBAI 2022</span></h2>
                                        <p data-animation="fadeInUp" data-delay=".6s">BFK Warzone will be networking and expanding its connections in the gaming space</p>
                                        <div class="main-slider-img-mobile img-loader" onclick="openInNewTab('<?php echo $BFK_CRYPTOEXPO_LINK;?>')">
                                            <img src="img/slider/Cryptoexpodubai.png" loading="lazy" alt="" data-animation="slideInRightS" data-delay=".8s" style="border-radius: 8px;">
                                        </div>
                                        <a href="https://cryptoexpodubai.com/" target="_blank" class="btn btn-style-two" data-animation="fadeInUp" data-delay=".8s">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-img" style="right: 15%">
                                <div class="img-loader" onclick="openInNewTab('<?php echo $BFK_CRYPTOEXPO_LINK;?>')">
                                    <a href="https://cryptoexpodubai.com/" target="_blank"><img src="img/slider/Cryptoexpodubai.png" loading="lazy" alt="" data-animation="slideInRightS" data-delay=".8s" style="border-radius: 8px;"></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="single-slider slider-bg slider-style-two">
                        <div class="container custom-container-two">
                            <div class="row">
                                <div class="col-xl-6 col-lg-7 col-md-11">
                                    <div class="slider-content">
                                        <h6 data-animation="fadeInUp" data-delay=".4s">PLAY TO EARN</h6>
                                        <h2 data-animation="fadeInUp" data-delay=".4s">ENGAGE THE <span>WARZONE</span></h2>
                                        <p data-animation="fadeInUp" data-delay=".6s">Own, Rank and Skill your Forti's in a world of massive online multiplayer 2D battles, build your NFT's army, equip and engage in an never ending battlefield.</p>
                                        <div class="main-slider-img-mobile img-loader">
                                            <img src="img/slider/main-slider1.png" alt="" data-animation="slideInRightS" data-delay=".8s">
                                        </div>
                                        <a href="#fortiCarouselSection" class="btn btn-style-two" data-animation="fadeInUp" data-delay=".8s">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-img img-loader"><img src="img/slider/main-slider1.png" alt="" data-animation="slideInRightS" data-delay=".8s"></div>
                        </div>
                    </div>

                    <div class="single-slider slider-bg slider-style-two">
                        <div class="container custom-container-two">
                            <div class="row">
                                <div class="col-xl-6 col-lg-7 col-md-11">
                                    <div class="slider-content">
                                        <h6 data-animation="fadeInUp" data-delay=".4s">EARN AS A GAMER</h6>
                                        <h2 data-animation="fadeInUp" data-delay=".4s">GAMERS AT THE <span>CORE</span> OF BFK</h2>
                                        <p data-animation="fadeInUp" data-delay=".6s">BFK is place the gamers to shine, by holding $BFK you unlock the game, access our FREE ready to play Forti, fight, rank and achieve and build your profile as a gamer, your experience matters to advance in the warzone.</p>
                                        <div class="main-slider-img-mobile img-loader">
                                            <img src="img/slider/main-slider2.png" alt="" data-animation="slideInRightS" data-delay=".8s">
                                        </div>
                                        <a href="#fortiCarouselSection" class="btn btn-style-two" data-animation="fadeInUp" data-delay=".8s">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-img img-loader"><img src="img/slider/main-slider2.png" alt="" data-animation="slideInRightS" data-delay=".8s"></div>
                        </div>
                    </div>

                    <div class="single-slider slider-bg slider-style-two">
                        <div class="container custom-container-two">
                            <div class="row">
                                <div class="col-xl-6 col-lg-7 col-md-11">
                                    <div class="slider-content">
                                        <h6 data-animation="fadeInUp" data-delay=".4s">FIGHT THE ALIEN</h6>
                                        <h2 data-animation="fadeInUp" data-delay=".4s">DUNGEON PVE <span>FIGHTS</span></h2>
                                        <p data-animation="fadeInUp" data-delay=".6s">BFK is built with PVP and PVE game mode, that means you wont just fight players, but also AI's, our Featured Events section will hold several Dungeons to fight Alien Bosses and get loot and ETH rewards by winning and defending the fort. Fight as team Fight for the Fort!</p>
                                        <div class="main-slider-img-mobile img-loader">
                                            <img src="img/slider/main-slider3.png" alt="" data-animation="slideInRightS" data-delay=".8s">
                                        </div>
                                        <a href="#fortiCarouselSection" class="btn btn-style-two" data-animation="fadeInUp" data-delay=".8s">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-img img-loader"><img src="img/slider/main-slider3.png" alt="" data-animation="slideInRightS" data-delay=".8s"></div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- slider-area-end -->

            <!-- home-four-area-bg -->
            <div class="home-four-area-bg" id="fortiCarouselSection">
                <div class="video-bg">
                    <video autoplay muted playsinline loop>
                        <source src="./img/videos/video.mp4" type="video/mp4"></source>
                    </video>
                    <div class="bg"></div>
                </div>
                <!-- latest-games-area -->
                <section class="latest-games-area home-four-latest-games pt-120 rmp">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-6 col-md-8">
                                <div class="section-title home-four-title text-center mb-60">
                                    <h2>BFK WARZONE <span>FORTIS</span></h2>
                                    <p>Engage the Warzone with your Soldiers called Fortis that ranges from Navy Seals, Paratroopers, Snipers, Medics, Machine Gunners, Special Forces & more! Play Ranked to level up your Fortis and increase your NFT value reaching higher levels and value!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="game-gallery-area position-relative rmp">
                    <div class="game-gallery-bg"></div>
                    <div class="container-fluid p-0 fix">
                        <div class="row game-gallery-active">

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti1.jpg" alt="" <?php echo imgSize('img/fortis/forti1.jpg'); ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti2.jpg" alt="" <?php echo imgSize('img/fortis/forti2.jpg'); ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti3.jpg" alt="" <?php echo imgSize('img/fortis/forti3.jpg'); ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti4.jpg" alt="" <?php echo imgSize('img/fortis/forti4.jpg'); ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti5.jpg" alt="" <?php echo imgSize('img/fortis/forti5.jpg'); ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti6.jpg" alt="" <?php echo imgSize('img/fortis/forti6.jpg'); ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti7.jpg" alt="" <?php echo imgSize('img/fortis/forti7.jpg'); ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti8.jpg" alt="" <?php echo imgSize('img/fortis/forti8.jpg'); ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti9.jpg" alt="" <?php echo imgSize('img/fortis/forti9.jpg'); ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti10.jpg" alt="" <?php echo imgSize('img/fortis/forti10.jpg'); ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti11.jpg" alt="" <?php echo imgSize('img/fortis/forti11.jpg'); ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti12.jpg" alt="" <?php echo imgSize('img/fortis/forti12.jpg'); ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti13.jpg" alt="" <?php echo imgSize('img/fortis/forti13.jpg'); ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti14.jpg" alt="" <?php echo imgSize('img/fortis/forti14.jpg'); ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="game-gallery-item clickable img-loader" onclick="openFortiPopup(100)">
                                    <img loading="lazy" src="img/fortis/forti15.jpg" alt="" <?php echo imgSize('img/fortis/forti15.jpg'); ?>>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="slider-nav"></div>
                </div>
                <!-- latest-games-area-end -->

                <!-- Alien Poster -->
                <div class="alien-poster-wrapper img-loader">
                    <img src="img/images/alien-poster.jpg" <?php echo imgSize('img/images/alien-poster.jpg'); ?> />
                </div>

                <!-- live-match-area -->
                <section class="live-match-area pt-20 rmp">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-6 col-md-8">
                                <div class="section-title home-four-title text-center mb-60 black-title">
                                    <h2>BFK Warzone In <span>Action</span></h2>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-xl-12 col-lg-12">
                                <div class="videos-carousel">

                                    <div class="live-match-wrap">
                                        <img data-lazy="img/video-thumbs/thumb1.jpg" alt="">
                                        <a href="https://www.youtube.com/watch?v=cQXftW2qDiM" class="popup-video"><img data-lazy="img/icon/video_play_icon.png" alt=""></a>
                                    </div>

                                    <div class="live-match-wrap">
                                        <img data-lazy="img/video-thumbs/thumb2.jpg" alt="">
                                        <a href="https://www.youtube.com/watch?v=2-FtOsw5lpo" class="popup-video"><img data-lazy="img/icon/video_play_icon.png" alt=""></a>
                                    </div>

                                    <div class="live-match-wrap">
                                        <img data-lazy="img/video-thumbs/thumb3.jpg" alt="">
                                        <a href="https://www.youtube.com/watch?v=Cs2NjI_8U6M" class="popup-video"><img data-lazy="img/icon/video_play_icon.png" alt=""></a>
                                    </div>

                                    <div class="live-match-wrap">
                                        <img data-lazy="img/video-thumbs/thumb4.jpeg" alt="">
                                        <a href="https://www.youtube.com/watch?v=ruK3RLDTu84" class="popup-video"><img data-lazy="img/icon/video_play_icon.png" alt=""></a>
                                    </div>

                                    <div class="live-match-wrap">
                                        <img data-lazy="img/video-thumbs/thumb5.jpeg" alt="">
                                        <a href="https://www.youtube.com/watch?v=0J03qsY1-C8" class="popup-video"><img data-lazy="img/icon/video_play_icon.png" alt=""></a>
                                    </div>

                                    <div class="live-match-wrap">
                                        <img data-lazy="img/video-thumbs/thumb1.jpg" alt="">
                                        <a href="https://www.youtube.com/watch?v=cQXftW2qDiM" class="popup-video"><img data-lazy="img/icon/video_play_icon.png" alt=""></a>
                                    </div>

                                    <div class="live-match-wrap">
                                        <img data-lazy="img/video-thumbs/thumb2.jpg" alt="">
                                        <a href="https://www.youtube.com/watch?v=2-FtOsw5lpo" class="popup-video"><img data-lazy="img/icon/video_play_icon.png" alt=""></a>
                                    </div>

                                    <div class="live-match-wrap">
                                        <img data-lazy="img/video-thumbs/thumb3.jpg" alt="">
                                        <a href="https://www.youtube.com/watch?v=Cs2NjI_8U6M" class="popup-video"><img data-lazy="img/icon/video_play_icon.png" alt=""></a>
                                    </div>

                                    <div class="live-match-wrap">
                                        <img data-lazy="img/video-thumbs/thumb4.jpeg" alt="">
                                        <a href="https://www.youtube.com/watch?v=ruK3RLDTu84" class="popup-video"><img data-lazy="img/icon/video_play_icon.png" alt=""></a>
                                    </div>

                                    <div class="live-match-wrap">
                                        <img data-lazy="img/video-thumbs/thumb5.jpeg" alt="">
                                        <a href="https://www.youtube.com/watch?v=0J03qsY1-C8" class="popup-video"><img data-lazy="img/icon/video_play_icon.png" alt=""></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- live-match-area-end -->

            </div>
            <!-- home-four-area-bg-end -->

            <!-- Weapons -->
            <section class="weapons pt-20">
                <div class="">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-8">
                            <div class="section-title home-four-title text-center black-title mb-60">
                                <h2>FORTIS <span>WEAPONS SELECTION</span></h2>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-xl-12 col-lg-12">
                            <div class="weapons-carousel">

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=13"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/50 CAl Skin.png" <?php echo imgSize('img/weapons/50 CAl Skin.png'); ?> />
                                        <p>50 CAL Skin</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=1"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/50 CAL.png" <?php echo imgSize('img/weapons/50 CAL.png'); ?> />
                                        <p>50 CAL</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=2"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/awm_gun.png" <?php echo imgSize('img/weapons/awm_gun.png'); ?> />
                                        <p>AWM</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=3"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/sm/Gun 240.png" <?php echo imgSize('img/weapons/sm/Gun 240.png'); ?> />
                                        <p>M240</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=4"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/sm/gun m110.png" <?php echo imgSize('img/weapons/sm/gun m110.png'); ?> />
                                        <p>M110</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=5"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/sm/Gun M249.png" <?php echo imgSize('img/weapons/sm/Gun M249.png'); ?> />
                                        <p>M249</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=6"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/sm/HK416.png" <?php echo imgSize('img/weapons/sm/HK416.png'); ?> />
                                        <p>HK416</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=7"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/sm/MK 16 SCRAL.png" <?php echo imgSize('img/weapons/sm/MK 16 SCRAL.png'); ?> />
                                        <p>MK 16 SCAR-L</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=8"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/sm/MK 17 SCAR-H.png" <?php echo imgSize('img/weapons/sm/MK 17 SCAR-H.png'); ?> />
                                        <p>MK 17 SCAR-H</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=9"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/MK 18 MOD.png" <?php echo imgSize('img/weapons/MK 18 MOD.png'); ?> />
                                        <p>MK 18 MOD</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=10"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/sm/Pistol.png" <?php echo imgSize('img/weapons/sm/Pistol.png'); ?> />
                                        <p>Beretta M9</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=22"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/Shotgun.png" <?php echo imgSize('img/weapons/Shotgun.png'); ?> />
                                        <p>Mossberg</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=12"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/XM2010.png" <?php echo imgSize('img/weapons/XM2010.png'); ?> />
                                        <p>XM2010</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=23"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/sm/Boring_F-Thrower.png" <?php echo imgSize('img/weapons/sm/Boring_F-Thrower.png'); ?> />
                                        <p>Boring F-Thrower</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=24"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/sm/LRB_Pulse.png" <?php echo imgSize('img/weapons/sm/LRB_Pulse.png'); ?> />
                                        <p>LRB Pulse</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=25"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/sm/AKM.png" <?php echo imgSize('img/weapons/sm/AKM.png'); ?> />
                                        <p>AK47</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=26"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/sm/SG552.png" <?php echo imgSize('img/weapons/sm/SG552.png'); ?> />
                                        <p>Krieg 552</p>
                                    </div>
                                </div>

                                <div class="s-slide" onClick='window.location.href = "bootcamp.php?weaponeId=27"'>
                                    <div class="weapon-slide img-loader">
                                        <img loading="lazy" src="img/weapons/sm/ethenum.png" <?php echo imgSize('img/weapons/sm/ethenum.png'); ?> />
                                        <p>Ethenum XAR-S</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- gamers-area -->
            <section class="just-gamers-area just-gamers-bg pt-115 pb-120 rmp">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-5 col-lg-6 col-md-10">
                            <div class="section-title title-style-three white-title mb-70">
                                <h2>EMPOWER YOUR <span>FORTI'S</span></h2>
                                <p>Your Forti's Hold special ability's, Power-ups effects and Rank's that will unlock the limits and power of each!</p>
                            </div>
                            <div class="soldier-mobile img-loader">
                                <img loading="lazy" src="img/soldier.png" />
                            </div>
                            <div class="just-gamers-list">
                                <ul>
                                    <li>
                                        <div class="just-gamers-list-icon">
                                            <img loading="lazy" src="img/icon/arrow.png" alt="">
                                        </div>
                                        <div class="just-gamers-list-content fix">
                                            <h5>Special Abilities</h5>
                                            <p>Each Forti has a unique special ability that allows him to shine in battle, use it & fight to time your attacks or defense and have advantage in the Warzone!</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="just-gamers-list-icon">
                                            <img loading="lazy" src="img/icon/star.png" alt="">
                                        </div>
                                        <div class="just-gamers-list-content fix">
                                            <h5>Skill & Rank</h5>
                                            <p>Each Forti has a skill set of 20 levels, earn skills while fighting in ranked and non-ranked battles and unlock higher ranks and mint it to your Forti NFT.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="just-gamers-list-icon">
                                            <img loading="lazy" src="img/icon/bolt.png" alt="">
                                        </div>
                                        <div class="just-gamers-list-content fix">
                                            <h5>Power of the Knox</h5>
                                            <p>The alien power affects the forti's, the Infinity Knoxes and power ups can be activated to empower your Forti's and increase damage, health, speed and armor while skilling faster.</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 d-none d-lg-block">
                            <div class="just-gamers-img img-loader">
                                <img loading="lazy" src="img/soldier.png" alt="" class="just-gamers-character">
                                <div class="just-gamers-circle-shape">
                                    <img loading="lazy" src="img/images/gamers_circle_line.png" alt="">
                                    <img loading="lazy" src="img/images/gamers_circle_shape.png" alt="" class="rotateme">
                                </div>
                                <img loading="lazy" src="img/images/just_gamers_chart.png" alt="" class="gamers-chart-shape">
                                <div class="section-title title-style-three white-title mb-70 gamers-bottom-right-title">
                                    <h2>JUST FOR <span>GAMERS</span></h2>
                                    <p>Compete in a sophisticated matchmaking system and reach your limits while earning!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- gamers-area-end -->

            <!-- featured-game-area -->
            <section class="featured-game-area new-released-game-area pt-115 pb-90 rmp">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-8">
                            <div class="section-title home-four-title text-center black-title mb-60">
                                <h2>INFINITY KNOX <span>CARDS</span></h2>
                                <p>Unleash the Power of the Knox granting players permanent global stats boost on your armory. as long as you hold the knox its power will remain</p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Grouper -->
                <div class="grouper">
                    <button class="grouper__btn grouper__btn--gold" data-active="1" data-group="gold" onclick="showGold(true)">GOLD</button>
                    <button class="grouper__btn grouper__btn--emerald" data-active="0" data-group="emerald" onclick="showEmerald(true)">EMERALD</button>
                </div>

                <div class="container-fluid container-full">

                    <!-- GOLD -->
                    <div id="gold" class="group-show">
                        <div class="infinity-carousel-gold owl-carousel owl-theme">

                            <div class="">
                                <a href="img/infinity/sm/Comet Knox Infinity-min.png" class="popup-image">
                                    <div class="featured-game-item mb-30">
                                        <div class="featured-game-thumb img-loader">
                                            <img loading="lazy" src="img/infinity/sm/Comet Knox Infinity-min.png" alt="" <?php echo imgSize('img/infinity/sm/Comet Knox Infinity-min.png'); ?>>
                                        </div>
                                        <div class="featured-game-content">
                                            <h4>Comet Knox <span>Infinity</span></h4>
                                            <div class="featured-game-meta">
                                                <i class="fas fa-project-diagram"></i>
                                                <span>Gold</span>
                                            </div>
                                            <br>
                                            <span class="infinity-price">$BFK 350,000 <span class="global-effect" style="color: white !important;">Global Effect</span></span>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="">
                                <a href="img/infinity/sm/Health Knox Infinity-min.png" class="popup-image">
                                    <div class="featured-game-item mb-30">
                                        <div class="featured-game-thumb img-loader">
                                            <img loading="lazy" src="img/infinity/sm/Health Knox Infinity-min.png" alt="" <?php echo imgSize('img/infinity/sm/Health Knox Infinity-min.png'); ?>>
                                        </div>
                                        <div class="featured-game-content">
                                            <h4>Health Knox <span>Infinity</span></h4>
                                            <div class="featured-game-meta">
                                                <i class="fas fa-project-diagram"></i>
                                                <span>Gold</span>
                                            </div>
                                            <br>
                                            <span class="infinity-price">$BFK 350,000 <span class="global-effect" style="color: white !important;">Global Effect</span></span>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="">
                                <a href="img/infinity/sm/Knox Card Horizon-min.png" class="popup-image">
                                    <div class="featured-game-item mb-30">
                                        <div class="featured-game-thumb img-loader">
                                            <img loading="lazy" src="img/infinity/sm/Knox Card Horizon-min.png" alt="" <?php echo imgSize('img/infinity/sm/Knox Card Horizon-min.png'); ?>>
                                        </div>
                                        <div class="featured-game-content">
                                            <h4>Knox Card <span>Horizon</span></h4>
                                            <div class="featured-game-meta">
                                                <i class="fas fa-project-diagram"></i>
                                                <span>Gold</span>
                                            </div>
                                            <br>
                                            <span class="infinity-price">$BFK 350,000 <span class="global-effect" style="color: white !important;">Global Effect</span></span>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="">
                                <a href="img/infinity/sm/Knox Card Star-min.png" class="popup-image">
                                    <div class="featured-game-item mb-30">
                                        <div class="featured-game-thumb img-loader">
                                            <img loading="lazy" src="img/infinity/sm/Knox Card Star-min.png" alt="" <?php echo imgSize('img/infinity/sm/Knox Card Star-min.png'); ?>>
                                        </div>
                                        <div class="featured-game-content">
                                            <h4>Knox Card <span>Star</span></h4>
                                            <div class="featured-game-meta">
                                                <i class="fas fa-project-diagram"></i>
                                                <span>Gold</span>
                                            </div>
                                            <br>
                                            <span class="infinity-price">$BFK 350,000 <span class="global-effect" style="color: white !important;">Global Effect</span></span>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="">
                                <a href="img/infinity/sm/Speed Knox Infinity-min.png" class="popup-image">
                                    <div class="featured-game-item mb-30">
                                        <div class="featured-game-thumb img-loader">
                                            <img loading="lazy" src="img/infinity/sm/Speed Knox Infinity-min.png" alt="" <?php echo imgSize('img/infinity/sm/Speed Knox Infinity-min.png'); ?>>
                                        </div>
                                        <div class="featured-game-content">
                                            <h4>Speed Knox <span>Infinity</span></h4>
                                            <div class="featured-game-meta">
                                                <i class="fas fa-project-diagram"></i>
                                                <span>Gold</span>
                                            </div>
                                            <br>
                                            <span class="infinity-price">$BFK 350,000 <span class="global-effect" style="color: white !important;">Global Effect</span></span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            
                        </div>
                    </div>

                    <!-- EMERALD -->
                    <div id="emerald">
                        <div class="infinity-carousel-gold owl-carousel owl-theme">

                            <div class="">
                                <a href="img/infinity/sm/Knox-Card-Star-[Emerald]-min.png" class="popup-image">
                                    <div class="featured-game-item mb-30">
                                        <div class="featured-game-thumb img-loader">
                                            <img loading="lazy" src="img/infinity/sm/Knox-Card-Star-[Emerald]-min.png" alt="" <?php echo imgSize('img/infinity/sm/Knox-Card-Star-[Emerald]-min.png'); ?>>
                                        </div>
                                        <div class="featured-game-content">
                                            <h4>Knox Card <span>Star</span></h4>
                                            <div class="featured-game-meta">
                                                <i class="fas fa-project-diagram"></i>
                                                <span>Emerald</span>
                                            </div>
                                            <br>
                                            <span class="infinity-price">$BFK 350,000 <span class="global-effect" style="color: white !important;">Global Effect</span></span>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="">
                                <a href="img/infinity/sm/Knox-Comet-[Emerald]-min.png" class="popup-image">
                                    <div class="featured-game-item mb-30">
                                        <div class="featured-game-thumb img-loader">
                                            <img loading="lazy" src="img/infinity/sm/Knox-Comet-[Emerald]-min.png" alt="" <?php echo imgSize('img/infinity/sm/Knox-Comet-[Emerald]-min.png'); ?>>
                                        </div>
                                        <div class="featured-game-content">
                                            <h4>Knox <span>Comet</span></h4>
                                            <div class="featured-game-meta">
                                                <i class="fas fa-project-diagram"></i>
                                                <span>Emerald</span>
                                            </div>
                                            <br>
                                            <span class="infinity-price">$BFK 350,000 <span class="global-effect" style="color: white !important;">Global Effect</span></span>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="">
                                <a href="img/infinity/sm/Knox-Horizon-[Emerald]-min.png" class="popup-image">
                                    <div class="featured-game-item mb-30">
                                        <div class="featured-game-thumb img-loader">
                                            <img loading="lazy" src="img/infinity/sm/Knox-Horizon-[Emerald]-min.png" alt="" <?php echo imgSize('img/infinity/sm/Knox-Horizon-[Emerald]-min.png'); ?>>
                                        </div>
                                        <div class="featured-game-content">
                                            <h4>Knox <span>Horizon</span></h4>
                                            <div class="featured-game-meta">
                                                <i class="fas fa-project-diagram"></i>
                                                <span>Emerald</span>
                                            </div>
                                            <br>
                                            <span class="infinity-price">$BFK 350,000 <span class="global-effect" style="color: white !important;">Global Effect</span></span>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="">
                                <a href="img/infinity/sm/Knox-Quasar-[Emerald]-min.png" class="popup-image">
                                    <div class="featured-game-item mb-30">
                                        <div class="featured-game-thumb img-loader">
                                            <img loading="lazy" src="img/infinity/sm/Knox-Quasar-[Emerald]-min.png" alt="" <?php echo imgSize('img/infinity/sm/Knox-Quasar-[Emerald]-min.png'); ?>>
                                        </div>
                                        <div class="featured-game-content">
                                            <h4>Knox <span>Quasar</span></h4>
                                            <div class="featured-game-meta">
                                                <i class="fas fa-project-diagram"></i>
                                                <span>Emerald</span>
                                            </div>
                                            <br>
                                            <span class="infinity-price">$BFK 350,000 <span class="global-effect" style="color: white !important;">Global Effect</span></span>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="">
                                <a href="img/infinity/sm/Knox-Supernova-[Emerald]-min.png" class="popup-image">
                                    <div class="featured-game-item mb-30">
                                        <div class="featured-game-thumb img-loader">
                                            <img loading="lazy" src="img/infinity/sm/Knox-Supernova-[Emerald]-min.png" alt="" <?php echo imgSize('img/infinity/sm/Knox-Supernova-[Emerald]-min.png'); ?>>
                                        </div>
                                        <div class="featured-game-content">
                                            <h4>Knox <span>Supernova</span></h4>
                                            <div class="featured-game-meta">
                                                <i class="fas fa-project-diagram"></i>
                                                <span>Emerald</span>
                                            </div>
                                            <br>
                                            <span class="infinity-price">$BFK 350,000 <span class="global-effect" style="color: white !important;">Global Effect</span></span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            
                        </div>
                    </div>

                    <div class="infinity-link__wrapper">
                        <a href="<?php echo $BFK_MARKETPLACE_LINK; ?>" target="_blank" class="btn btn-style-two buy-now-btn">BUY NOW IN MARKETPLACE <i class="fa fa-arrow-right" style="margin-left: 10px;"></i></a>
                    </div>

                </div>
            </section>
            <!-- featured-game-area-end -->

            <!-- brand-area -->
            <div class="brand-area brand-bg home-four-brand rmp">
                <div class="container">
                    <?php include '_logos.php'; ?>
                </div>
            </div>
            <!-- brand-area-end -->

            <!-- shop-area -->
            <section class="shop-area home-four-shop-area pt-115 pb-90 rmp">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-6 col-lg-8">
                            <div class="section-title home-four-title text-center mb-35">
                                <h2>BFK Forti <span>Real Artifacts</span></h2>
                                <p>Our NFT collection reflected in the real world! own your Real Figurine Forti with any Supreme Rare NFT shipped to your door step free of charge, or buy and collect using our native token $BFK or via credit card.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row product-active">

                        <div class="col-xl-3">
                            <div class="shop-item">
                                <div class="product-thumb img-loader">
                                    <a href="img/artifacts/hd/armis.jpg" class="popup-image"><img data-lazy="img/artifacts/low-res/artifact1.jpg" alt="" <?php echo imgSize('img/artifacts/low-res/artifact1.jpg'); ?>></a>
                                </div>
                                <div class="product-content">
                                    <div class="product-tag"><a href="#">Sniper</a></div>
                                    <h4>
                                        <a href="#">Armis</a>
                                        <span class="product-rarity product-rarity--ultra-rare">Ultra Rare</span>
                                    </h4>
                                    <div class="product-meta">
                                        <div class="product-price">
                                            <h5>$BFK</h5>
                                        </div>
                                        <div class="product-cart-action">
                                            <a class="artifact-buy" href="https://www.pubgarab.me/products/bfk-warzone-forti-armis-supreme-rare-sniper" target="_blank">Pre-order</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3">
                            <div class="shop-item">
                                <div class="product-thumb img-loader">
                                    <a href="img/artifacts/hd/lesly.jpg" class="popup-image"><img data-lazy="img/artifacts/low-res/Lesly-assaultnavy.jpg" alt="" <?php echo imgSize('img/artifacts/low-res/Lesly-assaultnavy.jpg'); ?>></a>
                                </div>
                                <div class="product-content">
                                    <div class="product-tag"><a href="#">Assault Navy</a></div>
                                    <h4>
                                        <a href="#">Lesly</a>
                                        <span class="product-rarity product-rarity--ultimate">Ultimate</span>
                                    </h4>
                                    <div class="product-meta">
                                        <div class="product-price">
                                            <h5>$BFK</h5>
                                        </div>
                                        <div class="product-cart-action">
                                            <a class="artifact-coming-soon" href="<?php echo $BFK_STORE_LINK; ?>"  target="_blank">Coming Soon</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3">
                            <div class="shop-item">
                                <div class="product-thumb img-loader">
                                    <a href="img/artifacts/hd/rapto.jpg" class="popup-image"><img data-lazy="img/artifacts/low-res/rapto-paratrooper.jpg" alt="" <?php echo imgSize('img/artifacts/low-res/rapto-paratrooper.jpg'); ?>></a>
                                </div>
                                <div class="product-content">
                                    <div class="product-tag"><a href="#">Paratrooper</a></div>
                                    <h4>
                                        <a href="#">Rapto</a>
                                        <span class="product-rarity product-rarity--rare">Rare</span>
                                    </h4>
                                    <div class="product-meta">
                                        <div class="product-price">
                                            <h5>$BFK</h5>
                                        </div>
                                        <div class="product-cart-action">
                                            <a class="artifact-coming-soon" href="<?php echo $BFK_STORE_LINK; ?>"  target="_blank">Coming Soon</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3">
                            <div class="shop-item">
                                <div class="product-thumb img-loader">
                                    <a href="img/artifacts/hd/renkop.jpg" class="popup-image"><img data-lazy="img/artifacts/low-res/renkop-medic.jpg" alt="" <?php echo imgSize('img/artifacts/low-res/renkop-medic.jpg'); ?>></a>
                                </div>
                                <div class="product-content">
                                    <div class="product-tag"><a href="#">Medic</a></div>
                                    <h4>
                                        <a href="#">Renkop</a>
                                        <span class="product-rarity product-rarity--one-of-a-kind">One of a Kind</span>
                                    </h4>
                                    <div class="product-meta">
                                        <div class="product-price">
                                            <h5>$BFK</h5>
                                        </div>
                                        <div class="product-cart-action">
                                            <a class="artifact-coming-soon" href="<?php echo $BFK_STORE_LINK; ?>"  target="_blank">Coming Soon</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3">
                            <div class="shop-item">
                                <div class="product-thumb img-loader">
                                    <a href="img/artifacts/hd/scopik.jpg" class="popup-image"><img data-lazy="img/artifacts/low-res/scopik-sniper.jpg" alt="" <?php echo imgSize('img/artifacts/low-res/scopik-sniper.jpg'); ?>></a>
                                </div>
                                <div class="product-content">
                                    <div class="product-tag"><a href="#">Sniper</a></div>
                                    <h4>
                                        <a href="#">Scopik</a>
                                        <span class="product-rarity product-rarity--ultra-rare">Ultra Rare</span>
                                    </h4>
                                    <div class="product-meta">
                                        <div class="product-price">
                                            <h5>$BFK</h5>
                                        </div>
                                        <div class="product-cart-action">
                                            <a class="artifact-coming-soon" href="<?php echo $BFK_STORE_LINK; ?>"  target="_blank">Coming Soon</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3">
                            <div class="shop-item">
                                <div class="product-thumb img-loader">
                                    <a href="img/artifacts/hd/dongo.jpg" class="popup-image"><img data-lazy="img/artifacts/low-res/dongo-machinegunner.jpg" alt="" <?php echo imgSize('img/artifacts/low-res/dongo-machinegunner.jpg'); ?>></a>
                                </div>
                                <div class="product-content">
                                    <div class="product-tag"><a href="#">Machine Gunner</a></div>
                                    <h4>
                                        <a href="#">Dongo</a>
                                        <span class="product-rarity product-rarity--ultra-rare">Ultra Rare</span>
                                    </h4>
                                    <div class="product-meta">
                                        <div class="product-price">
                                            <h5>$BFK</h5>
                                        </div>
                                        <div class="product-cart-action">
                                            <a class="artifact-coming-soon" href="<?php echo $BFK_STORE_LINK; ?>"  target="_blank">Coming Soon</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3">
                            <div class="shop-item">
                                <div class="product-thumb img-loader">
                                    <a href="img/artifacts/hd/hercis.jpg" class="popup-image"><img data-lazy="img/artifacts/low-res/hercis-machine.jpg" alt="" <?php echo imgSize('img/artifacts/low-res/hercis-machine.jpg'); ?>></a>
                                </div>
                                <div class="product-content">
                                    <div class="product-tag"><a href="#">Machine Gunner</a></div>
                                    <h4>
                                        <a href="#">Hercis</a>
                                        <span class="product-rarity product-rarity--ultra-rare">Ultra Rare</span>
                                    </h4>
                                    <div class="product-meta">
                                        <div class="product-price">
                                            <h5>$BFK</h5>
                                        </div>
                                        <div class="product-cart-action">
                                            <a class="artifact-coming-soon" href="<?php echo $BFK_STORE_LINK; ?>"  target="_blank">Coming Soon</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3">
                            <div class="shop-item">
                                <div class="product-thumb img-loader">
                                    <a href="img/artifacts/hd/kocken.jpg" class="popup-image"><img data-lazy="img/artifacts/low-res/kocken-demolution.jpg" alt="" <?php echo imgSize('img/artifacts/low-res/kocken-demolution.jpg'); ?>></a>
                                </div>
                                <div class="product-content">
                                    <div class="product-tag"><a href="#">Demolition</a></div>
                                    <h4>
                                        <a href="#">Kocken</a>
                                        <span class="product-rarity product-rarity--rare">Rare</span>
                                    </h4>
                                    <div class="product-meta">
                                        <div class="product-price">
                                            <h5>$BFK</h5>
                                        </div>
                                        <div class="product-cart-action">
                                            <a class="artifact-coming-soon" href="<?php echo $BFK_STORE_LINK; ?>"  target="_blank">Coming Soon</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3">
                            <div class="shop-item">
                                <div class="product-thumb img-loader">
                                    <a href="img/artifacts/hd/doger.jpg" class="popup-image"><img data-lazy="img/artifacts/low-res/doger-assaultnavy.jpg" alt="" <?php echo imgSize('img/artifacts/low-res/doger-assaultnavy.jpg'); ?>></a>
                                </div>
                                <div class="product-content">
                                    <div class="product-tag"><a href="#">Assault Navy</a></div>
                                    <h4>
                                        <a href="#">Doger</a>
                                        <span class="product-rarity product-rarity--ultimate">Ultimate</span>
                                    </h4>
                                    <div class="product-meta">
                                        <div class="product-price">
                                            <h5>$BFK</h5>
                                        </div>
                                        <div class="product-cart-action">
                                            <a class="artifact-coming-soon" href="<?php echo $BFK_STORE_LINK; ?>"  target="_blank">Coming Soon</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3">
                            <div class="shop-item">
                                <div class="product-thumb img-loader">
                                    <a href="img/artifacts/hd/forton.jpg" class="popup-image"><img data-lazy="img/artifacts/low-res/Forton-demolution.jpg" alt="" <?php echo imgSize('img/artifacts/low-res/Forton-demolution.jpg'); ?>></a>
                                </div>
                                <div class="product-content">
                                    <div class="product-tag"><a href="#">Demolition</a></div>
                                    <h4>
                                        <a href="#">Forton</a>
                                        <span class="product-rarity product-rarity--supreme-rare">Supreme Rare</span>
                                    </h4>
                                    <div class="product-meta">
                                        <div class="product-price">
                                            <h5>$BFK</h5>
                                        </div>
                                        <div class="product-cart-action">
                                            <a class="artifact-coming-soon" href="<?php echo $BFK_STORE_LINK; ?>"  target="_blank">Coming Soon</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3">
                            <div class="shop-item">
                                <div class="product-thumb img-loader">
                                    <a href="img/artifacts/hd/comodo.jpg" class="popup-image"><img data-lazy="img/artifacts/low-res/comdo-specialassault.jpg" alt="" <?php echo imgSize('img/artifacts/low-res/comdo-specialassault.jpg'); ?>></a>
                                </div>
                                <div class="product-content">
                                    <div class="product-tag"><a href="#">Special Assault</a></div>
                                    <h4>
                                        <a href="#">Comodo</a>
                                        <span class="product-rarity product-rarity--ultra-rare">Ultra Rare</span>
                                    </h4>
                                    <div class="product-meta">
                                        <div class="product-price">
                                            <h5>$BFK</h5>
                                        </div>
                                        <div class="product-cart-action">
                                            <a class="artifact-coming-soon" href="<?php echo $BFK_STORE_LINK; ?>"  target="_blank">Coming Soon</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3">
                            <div class="shop-item">
                                <div class="product-thumb img-loader">
                                    <a href="img/artifacts/hd/draxxis.jpg" class="popup-image"><img data-lazy="img/artifacts/low-res/draxxis-legacy.jpg" alt="" <?php echo imgSize('img/artifacts/low-res/draxxis-legacy.jpg'); ?>></a>
                                </div>
                                <div class="product-content">
                                    <div class="product-tag"><a href="#">Special Assault</a></div>
                                    <h4>
                                        <a href="#">Draxxis</a>
                                        <span class="product-rarity product-rarity--legacy">Legacy</span>
                                    </h4>
                                    <div class="product-meta">
                                        <div class="product-price">
                                            <h5>$BFK</h5>
                                        </div>
                                        <div class="product-cart-action">
                                            <a class="artifact-coming-soon" href="<?php echo $BFK_STORE_LINK; ?>"  target="_blank">Coming Soon</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <!-- shop-area-end -->

            <!-- blog-area -->
            <section class="blog-area pt-115 pb-120 rmp">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-6 col-lg-8">
                            <div class="section-title home-four-title black-title text-center mb-65">
                                <h2>Latest News & <span>Updates</span></h2>
                                <p>Follow the development of the game and improvements updates that the team is constantly pushing to learn more about what's coming!</p>
                            </div>
                        </div>
                    </div>
                    <div class="news-carousel owl-carousel owl-theme">

                        <div class="">
                            <div class="blog-post home-four-blog-post mb-50">
                                <div class="blog-thumb mb-30 img-loader">
                                    <a href="#"><img loading="lazy" src="img/news/news1.jpg" alt="" <?php echo imgSize('img/news/news1.jpg'); ?>></a>
                                </div>
                                <div class="blog-post-content">
                                    <div class="blog-meta">
                                        <ul>
                                            <li><i class="far fa-user"></i>BFK</li>
                                            <li><i class="far fa-calendar-alt"></i>December 15, 2021</li>
                                        </ul>
                                    </div>
                                    <h4><a href="https://bfkwarzone.com/changelog.php" target="_blank">Update</a></h4>
                                    <p>Updates and Optimizations for the Game and Marketplace, check our changelog for details about each update.</p>
                                    <a href="https://bfkwarzone.com/changelog.php" target="_blank" class="read-more">Read More <i class="fas fa-caret-right"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <div class="blog-post home-four-blog-post mb-50">
                                <div class="blog-thumb mb-30 img-loader">
                                    <a href="#"><img loading="lazy" src="img/news/news2.jpg" alt="" <?php echo imgSize('img/news/news2.jpg'); ?>></a>
                                </div>
                                <div class="blog-post-content">
                                    <div class="blog-meta">
                                        <ul>
                                            <li><i class="far fa-user"></i>BFK</li>
                                            <li><i class="far fa-calendar-alt"></i>December 15, 2021</li>
                                        </ul>
                                    </div>
                                    <h4><a href="https://bfkwarzone.com/changelog.php" target="_blank">Update</a></h4>
                                    <p>Updates and Optimizations for the Game and Marketplace, check our changelog for details about each update.</p>
                                    <a href="https://bfkwarzone.com/changelog.php" target="_blank" class="read-more">Read More <i class="fas fa-caret-right"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <div class="blog-post home-four-blog-post mb-50">
                                <div class="blog-thumb mb-30 img-loader">
                                    <a href="#"><img loading="lazy" src="img/news/news3.jpg" alt="" <?php echo imgSize('img/news/news1.jpg'); ?>></a>
                                </div>
                                <div class="blog-post-content">
                                    <div class="blog-meta">
                                        <ul>
                                            <li><i class="far fa-user"></i>BFK</li>
                                            <li><i class="far fa-calendar-alt"></i>December 15, 2021</li>
                                        </ul>
                                    </div>
                                    <h4><a href="https://bfkwarzone.com/changelog.php" target="_blank">Update</a></h4>
                                    <p>Updates and Optimizations for the Game and Marketplace, check our changelog for details about each update.</p>
                                    <a href="https://bfkwarzone.com/changelog.php" target="_blank" class="read-more">Read More <i class="fas fa-caret-right"></i></a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <!-- blog-area-end -->

            <!-- Tutorial Video Popup Trigger (used with JQuery Magnific Popup) -->
            <div id="tutorialVideoPopup" style="display: none;">
                <div id="tutorialVideoOverlay" onclick="closeTutorialPopup()"></div>
                <img id="tutorialVideoImg" src="img/images/alien-poster.jpg" />
                <a href="https://www.youtube.com/watch?v=onTDpFAuMS0" id="tutorialVideoBtn" class="clickable primary-color"><i class="fa fa-play"></i></a>
            </div>

        </main>
        <!-- main-area-end -->

        <!-- Footer -->
        <?php include '_footer.php'; ?>

        <!-- Scripts -->
        <?php include '_scripts.php'; ?>
        
        <!-- Fortis Popup -->
        <?php include '_fortiPopup.php'; ?>

        <!-- Grouper -->
        <script>

            const gold = document.querySelector('#gold');
            const emerald = document.querySelector('#emerald');
            const goldBtn = document.querySelector('.grouper__btn[data-group="gold"]');
            const emeraldBtn = document.querySelector('.grouper__btn[data-group="emerald"]');

            function showGold(animate = false) {

                // If gold carousel is already showing, exit
                if(whichIsActive() == 'gold') return;

                emeraldBtn.setAttribute('data-active', '0');
                goldBtn.setAttribute('data-active', '1');
                
                if(animate) {
                    flushEffects();
                    emerald.classList.add('fade-out');

                    setTimeout(() => {
                        gold.classList.add('group-show');
                        gold.classList.add('fade-in');
                        emerald.classList.remove('group-show');
                        emerald.classList.remove('fade-out');
                    }, 500);
                }
                else {
                    gold.classList.add('group-show');
                    emerald.classList.remove('group-show');
                }

            }

            function showEmerald(animate = false) {

                // If emerald carousel is already showing, exit
                if(whichIsActive() == 'emerald') return;

                goldBtn.setAttribute('data-active', '0');
                emeraldBtn.setAttribute('data-active', '1');
                
                if(animate) {
                    flushEffects();
                    gold.classList.add('fade-out');

                    setTimeout(() => {
                        emerald.classList.add('group-show');
                        emerald.classList.add('fade-in');
                        gold.classList.remove('group-show');
                        gold.classList.remove('fade-out');
                    }, 500);
                }
                else {
                    emerald.classList.add('group-show');
                    gold.classList.remove('group-show');
                }

            }

            function whichIsActive() {
                return (
                    document.querySelector('.grouper [data-active="1"][data-group="gold"]')
                        ? 'gold'
                        : 'emerald'
                )
            }

            function flushEffects() {
                gold.classList.remove('fade-in');
                gold.classList.remove('fade-out');
                emerald.classList.remove('fade-in');
                emerald.classList.remove('fade-out');
            }

            // Game Release Countdown
            $('#gameReleaseCountdown').countdown({
                date: '02/18/2022 23:59:59',
                offset: 4
            }, function () {
                // alert('Merry Christmas!');
            });

            // Tutorial Video
            $('#tutorialVideoBtn').magnificPopup({
                type: 'iframe',
                closeMarkup: `<button title="don't show again" type="button" class="mfp-close tutorial-close-btn" onclick="doNotShowTutorial()">don't show again</button>`
            });
            const tutorialVideoBtn = document.querySelector('#tutorialVideoBtn');
            const tutorialVideoPopup = document.querySelector('#tutorialVideoPopup');

            function showTutorialPopup() {
                tutorialVideoPopup.style.display = 'block';
            }
            function closeTutorialPopup() {
                tutorialVideoPopup.remove();
            }
            if(localStorage.getItem('bfk-tutorial-show') == 'no') {
                closeTutorialPopup();
            }

            $(document).ready(function() {
                if(localStorage.getItem('bfk-tutorial-show') != 'no') {
                    showTutorialPopup();
                    tutorialVideoBtn.addEventListener('click', function() {
                        closeTutorialPopup();
                    })
                }

                // Auto Show Announcement popup (removed since announcements are dynamic now)
                // if(localStorage.getItem('bfk-announcement-show') != 'no') {
                //     openAnnouncementPopup();
                //     localStorage.setItem('bfk-announcement-show', 'no');
                // }
            });

            function doNotShowTutorial() {
                localStorage.setItem('bfk-tutorial-show', 'no');
            }
        </script>
        
    </body>
</html>
