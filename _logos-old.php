<div class="logos-carousel owl-carousel owl-theme">
    
    <div class="item clickable img-loader" onclick="openInNewTab('<?php echo $BFK_COIN_GECKO_LINK;?>')">
        <img loading="lazy" src="img/brand/logo1.png" alt="" <?php echo imgSize('img/brand/logo1.png'); ?>>
    </div>
    <div class="item clickable img-loader" onclick="openInNewTab('<?php echo $BFK_COIN_MARKET_CAP_LINK;?>')">
        <img loading="lazy" src="img/brand/logo2.png" alt="" <?php echo imgSize('img/brand/logo2.png'); ?>>
    </div>
    <div class="item clickable img-loader" onclick="openInNewTab('<?php echo $BFK_PANCAKESWAP_LINK;?>')">
        <img loading="lazy" src="img/brand/logo3.png" alt="" <?php echo imgSize('img/brand/logo3.png'); ?>>
    </div>
    <div class="item clickable img-loader" onclick="openInNewTab('<?php echo $BFK_BSC_SCAN_LINK;?>')">
        <img loading="lazy" src="img/brand/logo4.png" alt="" <?php echo imgSize('img/brand/logo4.png'); ?>>
    </div>
    <div class="item clickable img-loader" onclick="openInNewTab('<?php echo $BFK_DEXTOOLS_LINK;?>')">
        <img loading="lazy" src="img/brand/logo5.png" alt="" <?php echo imgSize('img/brand/logo5.png'); ?>>
    </div>
    <div class="item clickable img-loader" onclick="openInNewTab('<?php echo $BFK_COINBASE_LINK;?>')">
        <img loading="lazy" src="img/brand/coinbase.png" alt="" <?php echo imgSize('img/brand/coinbase.png'); ?> style="height: 28px; width: 112px; object-fit: cover; object-position: center center;">
    </div>
    <div class="item clickable img-loader" onclick="openInNewTab('<?php echo $BFK_CRYPTO_LINK;?>')">
        <img loading="lazy" src="img/brand/crypto.png" alt="" <?php echo imgSize('img/brand/crypto.png'); ?> style="height: 30px; width: 118px; object-fit: cover; object-position: center center;">
    </div>
    <div class="item clickable img-loader" onclick="openInNewTab('<?php echo $BFK_NOMICS_LINK;?>')">
        <img loading="lazy" src="img/brand/nomics.png" alt="" <?php echo imgSize('img/brand/nomics.png'); ?> style="height: 24px; width: 123px; object-fit: cover; object-position: center center;">
    </div>
    <div class="item clickable img-loader" onclick="openInNewTab('<?php echo $BFK_CERTIK_LINK;?>')">
        <img loading="lazy" src="img/brand/certik.png" alt="" <?php echo imgSize('img/brand/certik.png'); ?> style="height: 24px; width: 123px; object-fit: cover; object-position: center center;">
    </div>
    <!-- <div class="item clickable img-loader" onclick="openInNewTab('<?php echo $BFK_NARSUN_LINK;?>')">
        <img loading="lazy" src="img/logo/narsun_studios_black.png" alt="" <?php echo imgSize('img/brand/narsun.png'); ?> style="height: 70px; object-fit: contain; object-position: center center;">
    </div> -->
    <div class="item clickable img-loader" onclick="openInNewTab('<?php echo $BFK_BINANCE_LINK;?>')">
        <img loading="lazy" src="img/brand/binance.png" alt="" <?php echo imgSize('img/brand/binance.png'); ?>>
    </div>
    <div class="item clickable img-loader" onclick="openInNewTab('https://cryptoexpodubai.com/')">
        <img loading="lazy" src="img/brand/cryptoexpo.png" alt="" <?php echo imgSize('img/brand/cryptoexpo.png'); ?>>
    </div>
    <div class="item clickable img-loader" onclick="openInNewTab('https://tresconglobal.com/conferences/blockchain/')">
        <img loading="lazy" src="img/brand/wbs.png" alt="" <?php echo imgSize('img/brand/wbs.png'); ?>>
    </div>

</div>