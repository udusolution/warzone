<?php

$TEAM = [
    array(
        "id" => 1,
        "name" => "Elie Kattar",
        "position" => "Founder & CEO at BFK WARZONE",
        "avatar" => "img/team/elie.jpg",
        "description" => "<p>I have been working in the field for over ten years as a Community Manager and Gaming Specialist with multiple esports lounges, tournaments & platforms like Facebook Gaming in the Middle East & US. I have extensive experience in game design and development and am currently managing BFK Warzone Game and Marketplace Ecosystem, building the game into the blockchain. I started developing games privately in 2010 on Unity 3D Engine and engaged in several Professional projects also worked closely with Razer Gold Singapore for Facebook campaigns for PUBG MOBILE releases.</p>",
        "socials" => array(
            "linkedin" => "https://www.linkedin.com/in/ekattar/"
        )
    ),
    array(
        "id" => 3,
        "name" => "Christopher Gallant",
        "position" => "COO at BFK WARZONE",
        "avatar" => "img/team/chris.jpg",
        "description" => "<p>Chris comes from a business education background. Through hard work and dedication, he has excelled at each position he has worked at throughout his working life. He has taken his passion for learning and leading into the cryptocurrency space. His primary role in the BFK Warzone project is co-managing the overall resources and operations of the project. Involved in core team business decisions and deciding vote when decisions are tied. He is an excellent spokesperson for the project in the community, frequently participates in AMA’s, and all aspects of project planning and day-to-day responsibilities.</p>",
        "socials" => array(
            "linkedin" => "https://www.linkedin.com/mwlite/in/christopher-gallant-ba35b869"
        )
    ),
    array(
        "id" => 4,
        "name" => "Muhammad Shahab",
        "position" => "UI/UX 3D Game Designer",
        "avatar" => "img/team/muhammad.jpg",
        "description" => "<p>I intend to be in front of clients' requirements, I have deeply involved myself in always finding the ideal solutions from the initial phase to final delivery. I am providing services in the 3d domain for the last 10 years for the gaming and products industries and working on AAA games.</p>",
        "socials" => array(
            "linkedin" => "https://www.linkedin.com/in/syed-muhammad-shahab-34496773/"
        )
    ),
    array(
        "id" => 5,
        "name" => "Jayden Cummins",
        "position" => "Creative Director",
        "avatar" => "img/team/Jayden.jpg",
        "description" => "<p>Working in the entertainment industry for 30 years, and after studying film and television production (School of Visual Arts), sound engineering (School of Audio Engineering), and attending the Australian Film, Television and Radio School, Jayden initially carved out his career in the music industry. As a professional musician, composer and engineer, he worked with many top artists including Hunters and Collectors, Crowded House, 1927 and Margaret Urlich, before finally earning a reputation as one of Sydney's top cocktail pianists.</p>",
        "socials" => array(
            "linkedin" => "https://www.linkedin.com/in/jaydencummins/"
        )
    ),
]

?>

<script>

    const TEAM = <?php echo json_encode($TEAM); ?>;

</script>