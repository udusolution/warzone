<?php

// Fortis
$FORTIS = [
    array(
        "id" => 1,
        "name" => "PV2. Draxxis",
        "coverSM" => "img/fortis/sm/draxis.png",
        "cover" => "img/fortis/draxis.png",
        "cover2" => "img/fortis/draxis.png",
        "video" => "https://www.youtube.com/watch?v=rHKVeTiY8f8",
        "rarity" => "legacy",
        "cat" => "Battle Warrior",
        "faction" => "army",
        "weapon1Name" => "M4 Carbine",
        "weapon1Image" => "img/weapons/hd/M4A1.png",
        "weapon2Name" => "M4A16",
        "weapon2Image" => "img/weapons/hd/M16.png",
        "ability" => "Grenade Throw",
        "minSkill" => "1",
        "maxSkill" => "20",
        "price" => "Free To Play",
        "trade" => "Non-Tradable",
        "available" => "Unlimited",
        "insignia" => "img/ranks/army-1.png",
        "description" => "Draxxis is a Battle Warrior Soldier. Following the Army unit, Draxxis was the first Ever Soldier to Join Warzone as the Legacy Fighter; he will start at the Private level and be upgraded. Draxxis can Hold the M4 Carbine or the M4A16 Assualt rifle. His unique ability is Grenade Charge, allowing him to throw Explosive Grenades at his enemies, dealing radius damage delivery. Draxxis will be free to claim digital copy only in-game and is not tradable but only rankable. Draxxis can be Ranked with a maximum level of 10 and will start at level 1.",
        "stats" => [
            [
                array(
                    "title" => "DPR", // DPR, HP, Armor, Speed, Skill
                    "value" => "1.3",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5",
                    "icon" => "img/icon/pw3.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "25",
                    "icon" => "img/icon/pw4.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.11",
                    "icon" => "img/icon/pw5.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/pw6.png"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "0.8",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "25",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.085",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ]
        ],
        "specs" => ["Unlimited Supply", "Available for all profiles", "Rankable - Can play stake games", "Non - Tradable"]
    ),
    array(
        "id" => 2,
        "name" => "PV2. Boomer",
        "coverSM" => "img/fortis/sm/boomr.png",
        "cover" => "img/fortis/boomr.png",
        "cover2" => "img/fortis/boomr2.png",
        "video" => "https://www.youtube.com/watch?v=knKwyy-eTiE",
        "rarity" => "legacy",
        "cat" => "Battle Warrior",
        "faction" => "army",
        "weapon1Name" => "AK47",
        "weapon1Image" => "img/weapons/sm/AKM.png",
        "weapon2Name" => "Krieg 552 commando",
        "weapon2Image" => "img/weapons/sm/SG552.png",
        "ability" => "Cocktail Molotov",
        "minSkill" => "1",
        "maxSkill" => "20",
        "price" => "Free To Claim",
        "trade" => "Non-Tradable",
        "available" => "Unlimited",
        "insignia" => "img/ranks/army-1.png",
        "description" => "Boomer is a Light Assault Soldier. Following the Army unit, Boomer will start at the level of Private and be upgraded, and Boomer can Hold the AK47 or the Krieg 552 Commando Assault rifle. His unique ability is Cocktail Molotov that will allow him to throw flammable bottles on fire at his enemies dealing with AOE damage delivery. Boomer will be free to claim NFT and is free to trade & rankable following our minting rarity engine that will release and mint new NFTs for this Legacy unit. Boomer can be Ranked with a maximum level of 10 and will start at level 1.",
        "stats" => [
            [
                array(
                    "title" => "DPR",
                    "value" => "1.3",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5",
                    "icon" => "img/icon/pw3.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "25",
                    "icon" => "img/icon/pw4.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.11",
                    "icon" => "img/icon/pw5.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/pw6.png"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "0.8",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "25",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.085",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ]
        ],
        "specs" => ["Unlimited Supply", "Available for all profiles", "Rankable - Can play stake games", "Tradable"],
        "skins" => [
            array(
                "name" => "AK47 GOLD",
                "cover" => "img/weapons/sm/AKM_GOLD.png"
            )
        ]
    ),
    array(
        "id" => 3,
        "name" => "SN. Doger",
        "coverSM" => "img/fortis/sm/doger.png",
        "cover" => "img/fortis/doger.png",
        "cover2" => "img/fortis/doger2.png",
        "video" => "https://www.youtube.com/watch?v=k9OdDTvdrZ0",
        "rarity" => "ultimate",
        "cat" => "Navy Seal",
        "faction" => "navy",
        "weapon1Name" => "HK416",
        "weapon1Image" => "img/weapons/hd/HK416.png",
        "weapon2Name" => "MK 17 SCAR-H",
        "weapon2Image" => "img/weapons/hd/MK 17 SCAR-H.png",
        "ability" => "Speed Shots",
        "minSkill" => "2",
        "maxSkill" => "20",
        "price" => "50 USDT",
        "trade" => "Tradable",
        "available" => "100",
        "insignia" => "img/ranks/navy-2.png",
        "description" => "Doger is a Courageous Ultimate Specialist, Following the Navy Seal Army unit, Doger will start at the level of Seaman Apprentice and will be upgradable, Doger can Hold the HK416 Assault Rifle or MK17 SCAR, both of which will fire Speed Shots over his enemies, Doger also has a special Speed Shot ability that will allow him to fire Fast Rounds Consecutively causing excess damage on impact, Doger will be available for Early Access and will be free to trade afterwards following our minting rarity engine that will release and mint new NFT's for this ultimate Navy Seal, get ready to get your hands on it if you're Looking for close speed combats. Doger can be Ranked with a maximum level of 10.",
        "stats" => [
            [
                array(
                    "title" => "DPR",
                    "value" => "1.45",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.1",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "28",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.1",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "1.6",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.1",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "28",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.17",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ]
        ],
        "specs" => ["Mintable Supply", "100 mints EA", "Wait time minimum 7 days", "Available in Marketplace", "Seasonal mints possible", "No free toy available for purchase individually"]
    ),
    array(
        "id" => 4,
        "name" => "SN. Lesley",
        "coverSM" => "img/fortis/sm/lesley.png",
        "cover" => "img/fortis/lesley.png",
        "cover2" => "img/fortis/lesley2.png",
        "video" => "https://www.youtube.com/watch?v=BNqAIQDLE4M",
        "rarity" => "ultimate",
        "cat" => "Navy Seal",
        "faction" => "navy",
        "weapon1Name" => "AR 15 + Scope",
        "weapon1Image" => "img/weapons/hd/AR15 SCOPE.png",
        "weapon2Name" => "CZ Bren 2 MS Carbine",
        "weapon2Image" => "img/weapons/hd/CZ_Bren_2_MS_Carbine.png",
        "ability" => "Sticky Bomb",
        "minSkill" => "2",
        "maxSkill" => "20",
        "price" => "500 USDT",
        "trade" => "Tradable",
        "available" => "25",
        "insignia" => "img/ranks/navy-2.png",
        "description" => "Lesley is a Navy Seal Specialist. Following the Navy unit, Lesley assisted in several fights protecting the fort; she started at the Seaman level and can be upgraded. Lesley can Hold the AR 15 With Scope or the CZ Bren 2 MS Carbine. Her unique ability is Sticky Bomb, allowing her to throw Sticky Explosive Charges at enemies, dealing zone damage delivery. Lesley will be purchasable NFT in-game and is tradable and rankable. Lesley can be Ranked with a maximum level of 12 and will start at level 1.",
        "stats" => [
            [
                array(
                    "title" => "DPR",
                    "value" => "2",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.3",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "33",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.09",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "2.5",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.3",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "33",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.15",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ]
        ],
        "specs" => ["Limited Supply", "25 mints EA", "Available in Marketplace", "No free toy available for purchase individually", "Rankable - Can play stake games", "Tradable"]
    ),
    array(
        "id" => 5,
        "name" => "A1C. Rapto",
        "coverSM" => "img/fortis/sm/rapto.png",
        "cover" => "img/fortis/rapto.png",
        "cover2" => "img/fortis/rapto2.png",
        "video" => "https://www.youtube.com/watch?v=mauyI-rxXyY",
        "rarity" => "rare",
        "cat" => "Paratrooper",
        "faction" => "air_force",
        "weapon1Name" => "MP5K",
        "weapon1Image" => "img/weapons/hd/MP5K.png",
        "weapon2Name" => "M4A1",
        "weapon2Image" => "img/weapons/hd/M4A1.png",
        "ability" => "Smoke Grenade",
        "minSkill" => "2",
        "maxSkill" => "20",
        "price" => "75 USDT",
        "trade" => "Tradable",
        "available" => "40",
        "insignia" => "img/ranks/air-2.png",
        "description" => "Rapto is a Paratrooper Air Assault Specialist, Following the Airfoce Army unit, Rapto will start at the level of Airman First Class Paratrooper and will be upgradable, Rapto can Hold the M4A1 ACG Assault Rifle or MP5K Submachine gun , his special ability is Smoke Cloud Grenade that will allow him to Shadow hide or attack enemies with field advantage, Rapto will be available for Early Access and will be free to trade afterwards following our minting rarity engine that will release and mint new NFT's for this Rare Airforce Forti, get ready to get your hands on it if you're Looking for close speed combats. Rapto can be Ranked with a maximum level of 10 and will start at level 2.",
        "stats" => [
            [
                array(
                    "title" => "DPR",
                    "value" => "1.65",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.7",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "35",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.09",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "1.75",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.7",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "35",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.09",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ]
        ],
        "specs" => ["Mintable Supply", "50 mints EA", "Available in Marketplace", "No free toy available for purchase individually", "Seasonal mints possible", "Wait time minimum 90 days for next mint", "Rankable - Can play stake games", "Tradable"]
    ),
    array(
        "id" => 6,
        "name" => "PFC. Kocken",
        "coverSM" => "img/fortis/sm/kocken.png",
        "cover" => "img/fortis/kocken.png",
        "cover2" => "img/fortis/kocken2.png",
        "video" => "https://www.youtube.com/watch?v=tDbGmeOIVIg",
        "rarity" => "rare",
        "cat" => "Demolution",
        "faction" => "army",
        "weapon1Name" => "M136 AT4",
        "weapon1Image" => "img/weapons/hd/M136_AT4.png",
        "weapon2Name" => "M32",
        "weapon2Image" => "img/weapons/hd/M32.png",
        "ability" => "Double Rocket Launch",
        "minSkill" => "2",
        "maxSkill" => "20",
        "price" => "150 USDT",
        "trade" => "Tradable",
        "available" => "40",
        "insignia" => "img/ranks/army-2.png",
        "description" => "Kocken is a Rocket Launcher Master, Following the Demolution Army unit, Kocken will start at the level of Private Demolution and will be upgradable, Kocken can Hold the M136 AT4 Rocket Launcher or M32 Grenade Launcher , his special ability is Double Rocket Shots that will allow him to Fire at once 2 rockets towards enemies with massive damage delivery, Kocken will be available for Early Access and will be free to trade afterwards following our minting rarity engine that will release and mint new NFT's for this Rare Army Demolution Forti, get ready to get your hands on it if you're Looking for nuking the enemies hard! Kocken can be Ranked with a maximum level of 10 and will start at level 1.",
        "stats" => [
            [
                array(
                    "title" => "DPR",
                    "value" => "45",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.6",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "20",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.19",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "100",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "4.8",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.6",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "20",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.15",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ]
        ],
        "specs" => ["Mintable Supply", "40 mints EA", "Wait time minimum 120 days", "Available in Marketplace", "Seasonal mints possible", "No free toy available for purchase individually", "Rankable - Can play stake games", "Tradable"]
    ),
    array(
        "id" => 7,
        "name" => "PFC. Dongo",
        "coverSM" => "img/fortis/sm/dongo.png",
        "cover" => "img/fortis/dongo.png",
        "cover2" => "img/fortis/dongo.png",
        "video" => "https://www.youtube.com/watch?v=Fm2UGZ8WRdk",
        "rarity" => "ultra-rare",
        "cat" => "Saw Machine Gunner",
        "faction" => "army",
        "weapon1Name" => "SAW M249",
        "weapon1Image" => "img/weapons/hd/M249_SAW.png",
        "weapon2Name" => "M249",
        "weapon2Image" => "img/weapons/hd/Gun M249.png",
        "ability" => "Barrage",
        "minSkill" => "2",
        "maxSkill" => "20",
        "price" => "250 USDT",
        "trade" => "Tradable",
        "available" => "25",
        "insignia" => "img/ranks/army-2.png",
        "description" => "Dongo is a Saw Machine Gunner Specialist. Following the Army unit code, Dongo assisted in cover support and suppressive fire assistance for the fort; Dongo started at the Private First Class and can be upgraded. Dongo can Hold the SAW M249 and the  LA032: M249. His unique ability is Barrage fire, allowing him to fire multiple mags in different angles at enemies, dealing Closed zone damage delivery. Dongo will be purchasable NFT in-game and is tradable and rankable. Dongo can be Ranked with a maximum level of 10 and will start at level 2.",
        "stats" => [
            [
                array(
                    "title" => "DPR",
                    "value" => "1.8",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.5",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "35",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.08",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "1.9",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.5",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "35",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.07",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ]
        ],
        "specs" => ["Limited Supply", "25 mints EA", "Available in marketplace", "No free toy available for purchase individually", "Event mints possible", "Wait time minimum 120 days for next mints", "Rankable - Can play stake games", "Tradable"]
    ),
    array(
        "id" => 8,
        "name" => "Cpl. Hercis",
        "coverSM" => "img/fortis/sm/hercis.png",
        "cover" => "img/fortis/hercis.png",
        "cover2" => "img/fortis/hercis2.png",
        "video" => "https://www.youtube.com/watch?v=DfBOKcLwiSM",
        "rarity" => "ultra-rare",
        "cat" => "Machine Gunner",
        "faction" => "marine_corps",
        "weapon1Name" => "M249",
        "weapon1Image" => "img/weapons/hd/Gun M249.png",
        "weapon2Name" => "M240",
        "weapon2Image" => "img/weapons/hd/Gun 240.png",
        "ability" => "Dual MG",
        "minSkill" => "3",
        "maxSkill" => "20",
        "price" => "200 USDT",
        "trade" => "Tradable",
        "available" => "25",
        "insignia" => "img/ranks/marine-3.png",
        "description" => "Hercis is a HVM Machine Gunner Specialist. Following the Marine core unit code, Hercis assisted in cover support and Heavy suppressive fire assistance for the fort; Hercis started at the Corporal Rank and can be upgraded. Hercis can Hold the M249 or the M240. His unique ability is Dual MG, allowing him to fire double MG's simultaneously, dealing Heavy direct damage. Hercis will be purchasable NFT in-game and is tradable and rankable. Hercis can be Ranked with a maximum level of 10 and will start at level 3.",
        "stats" => [
            [
                array(
                    "title" => "DPR",
                    "value" => "1.7",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.8",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "36",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.09",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "1.8",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.8",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "36",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.08",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ]
        ],
        "specs" => ["Limited Supply", "30 mints EA", "Available in Marketplace", "No free toy available for purchase individually", "Event mints possible", "Wait time minimum 120 days for next mints", "Rankable - Can play stake games", "Tradable"]
    ),
    array(
        "id" => 9,
        "name" => "Sgt. Scopik",
        "coverSM" => "img/fortis/sm/scopik.png",
        "cover" => "img/fortis/scopik.png",
        "cover2" => "img/fortis/scopik2.png",
        "video" => "https://www.youtube.com/watch?v=i8LhpstPdyw",
        "rarity" => "ultra-rare",
        "cat" => "Sniperman",
        "faction" => "marine_corps",
        "weapon1Name" => "XM2010 Sniper",
        "weapon1Image" => "img/weapons/hd/XM2010.png",
        "weapon2Name" => "M110",
        "weapon2Image" => "img/weapons/hd/gun m110.png",
        "ability" => "Piercing Shot",
        "minSkill" => "4",
        "maxSkill" => "20",
        "price" => "850 USDT",
        "trade" => "Locked / Unlocks for sale at Player Rank Seargeant Major",
        "available" => "18",
        "insignia" => "img/ranks/marine-4.png",
        "description" => "Scopik is a professional Sniper Specialist. Following the Marine core unit code, Scopik assisted in accurate piercing kills over aliens in assistance for the fort; Scopik started at the Sergeant Rank and can be upgraded. Scopik can Hold the XM2010 Sniper or the M110. His unique ability is Piercing Bullet, allowing him to fire an unstoppable piercing round that penetrates any obstacle until hitting his target, dealing Heavy direct damage. Scopik will be purchasable NFT in-game and is tradable and rankable. Scopik can be Ranked with a maximum level of 10 and will start at level 3.",
        "stats" => [
            [
                array(
                    "title" => "DPR",
                    "value" => "25",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.1",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "45",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.17",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "20",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.1",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "45",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.1",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ]
        ],
        "specs" => ["Limited Supply", "20 mints EA", "No free toy available for purchase individually", "Rankable - Can play stake games", "Tradable"]
    ),
    array(
        "id" => 10,
        "name" => "SPC. Comodo",
        "coverSM" => "img/fortis/sm/comodo.png",
        "cover" => "img/fortis/comodo.png",
        "cover2" => "img/fortis/comodo2.png",
        "video" => "https://www.youtube.com/watch?v=swkxF15KIPo",
        "rarity" => "ultra-rare",
        "cat" => "Special Forces",
        "faction" => "army",
        "weapon1Name" => "Mk16 SCAR",
        "weapon1Image" => "img/weapons/hd/MK 16 SCRAL.png",
        "weapon2Name" => "Mk 18 Mod",
        "weapon2Image" => "img/weapons/hd/MK 18 MOD.png",
        "ability" => "Grenade Launcher",
        "minSkill" => "4",
        "maxSkill" => "20",
        "price" => "170 USDT",
        "trade" => "Tradable",
        "available" => "25",
        "insignia" => "img/ranks/army-4.png",
        "description" => "Comodo is a Special Forces Agent. Following the Army unit code, Comodo assisted in direct close fights defending the fort; Comodo started at the Specialist Rank and can be upgraded. Comodo can Hold the Mk16 SCAR or the Mk 18 Mod. His unique ability is the Grenade launcher, allowing him to fire multiple grenade shells, dealing Heavy AOE damage. Comodo will be purchasable NFT in-game and is tradable and rankable. Comodo can be Ranked with a maximum level of 10 and will start at level 4.",
        "stats" => [
            [
                array(
                    "title" => "DPR",
                    "value" => "2.3",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "6",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "32",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.09",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "2.6",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "6",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "32",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "5",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ]
        ],
        "specs" => ["Limited Supply", "25 mints EA", "Available in Marketplace", "No free toy available for purchase individually", "Rankable - Can play stake games", "Tradable"]
    ),
    array(
        "id" => 11,
        "name" => "SgtMaj. Armis",
        "coverSM" => "img/fortis/sm/armis.png",
        "cover" => "img/fortis/armis.png",
        "cover2" => "img/fortis/armis2.png",
        "video" => "https://www.youtube.com/watch?v=KX-uAsVViBM",
        "rarity" => "supreme-rare",
        "cat" => "Special Sniper",
        "faction" => "marine_corps",
        "weapon1Name" => "50 CAL",
        "weapon1Image" => "img/weapons/hd/50 CAL.png",
        "weapon2Name" => "AWM 338",
        "weapon2Image" => "img/weapons/hd/awm_gun.png",
        "ability" => "Double Auto Shot",
        "minSkill" => "6",
        "maxSkill" => "22",
        "price" => "1250 USDT",
        "trade" => "Locked / Unlocks for sale at Player Rank Seargeant Major",
        "available" => "13",
        "insignia" => "img/ranks/marine-6.png",
        "description" => "Armis is a legendary Supreme Rare Sniper, Following the Marine Corps Army unit, Armis will start at the level of Sergeant Major and will be upgradable, Armis can Hold the 50 CAL Sniper or the AWM 338, both of which will land heavy damage over his enemies, Armis also has a special Auto Double Shot ability that will allow him to fire 2 Rounds Consecutively causing massive damage on impact, Armis will be available for Early Access and will be locked afterwards following our minting rarity engine that will release and mint new NFT's for this legendary Sniper, get ready to get your hands on it if you're the sniper guy. keep in mind that Amis is one of the most powerful sniper units that will be ever minted in the game... LFG",
        "stats" => [
            [
                array(
                    "title" => "DPR",
                    "value" => "32",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.5",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "50",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.15",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "30",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "5.5",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "50",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.1",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ]
        ],
        "specs" => ["Limited Supply", "Available in Marketplace", "Free toy available for original purchaser or purchase individually", "Claimable toy for 30 days only after purchase", "Rankable - Can play stake games", "Tradable"]
    ),
    array(
        "id" => 12,
        "name" => "SGT. Forton",
        "coverSM" => "img/fortis/sm/forton.png",
        "cover" => "img/fortis/forton.png",
        "cover2" => "img/fortis/forton2.png",
        "video" => "https://www.youtube.com/watch?v=FU-1nP4QUWw",
        "rarity" => "supreme-rare",
        "cat" => "Armored Specialist",
        "faction" => "army",
        "weapon1Name" => "M1014 Shotgun",
        "weapon1Image" => "img/weapons/hd/Shotgun.png",
        "weapon2Name" => "Mossberg 590A1",
        "weapon2Image" => "img/weapons/hd/Mossberg 590A1.png",
        "ability" => "Ultimate Armor",
        "minSkill" => "5",
        "maxSkill" => "25",
        "price" => "1000 USDT",
        "trade" => "Locked / Unlocks for sale at Player Rank Seargeant Major",
        "available" => "15",
        "insignia" => "img/ranks/army-5.png",
        "description" => "Forton is an Armored Specialist Unit. Following the Army unit code, Forton assisted in Hard Tanking and Slowing down the Aliens at the fort frontlines; Forton started at the Sergeant Rank and can be upgraded. Forton can Hold M1014 SHOTGUN or the Mossberg 590A1. His unique ability is ultimate armor, allowing him to activate a magnetic shield and stop 99% of incoming damage for a time. Forton will be purchasable NFT in-game and is tradable and rankable. Forton can be Ranked with a maximum level of 12 and will start at level 6.",
        "stats" => [
            [
                array(
                    "title" => "DPR",
                    "value" => "20",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "150",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "7.5",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "18",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.1",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "15",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "150",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "7.5",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "18",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.15",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ]
        ],
        "specs" => ["Limited Supply", "Available in Marketplace", "Free toy available or purchase individually", "Claimable toy for 30 days only after purchase", "Rankable - Can play stake games", "Tradable"]
    ),
    array(
        "id" => 13,
        "name" => "MSG. Renkop",
        "coverSM" => "img/fortis/sm/renkop.png",
        "cover" => "img/fortis/renkop.png",
        "cover2" => "img/fortis/renkop.png",
        "video" => "https://www.youtube.com/watch?v=-wY0fS9NHGY",
        "rarity" => "one-of-a-kind",
        "cat" => "Medic",
        "faction" => "army",
        "weapon1Name" => "Beretta M9 Pistol",
        "weapon1Image" => "img/weapons/hd/Pistol.png",
        "weapon2Name" => null,
        "weapon2Image" => null,
        "ability" => "Speed Heal",
        "minSkill" => "8",
        "maxSkill" => "30",
        "price" => "1500 USDT",
        "trade" => "Locked / Unlocks for sale at Player Rank Seargeant Major",
        "available" => "5",
        "insignia" => "img/ranks/army-8.png",
        "description" => "Renkop is a Medic Specialist Unit. Following the Army unit code, Renkop assisted in several fights provided Auro heal and Medical support to the Fortis; Renkop started at the Master Sergeant Class and can be upgraded. Renkop can only Hold the Beretta M9 Pistol. His unique ability is Speed Heal, allowing him to Heal His Allies fast and provide HP instantly. Renkop will be purchasable NFT in-game and is tradable and rankable. Renkop can be Ranked with a maximum level of 15 and will start at level 6. Renkop is one of the RAREST Fortis in the warzone and with limited supply.",
        "stats" => [
            [
                array(
                    "title" => "DPR",
                    "value" => "8",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "6",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "35",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.09",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "6",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "100",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "6",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "35",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.09",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "1-20",
                    "icon" => "img/icon/map_marker.png"
                )
            ]
        ],
        "specs" => ["Limited Supply", "5 mints EA", "Available in Marketplace", "Free toy available for original purchaser or purchase individually", "Claimable toy for 30 days only after purchase", "Possible mints on events", "Rankable - Can play stake games", "Tradable"]
    ),
    array(
        "id" => 14,
        "name" => "Melon Nusk",
        "coverSM" => "img/fortis/sm/melon.png",
        "cover" => "img/fortis/melon.png",
        "cover2" => "img/fortis/melon2.png",
        "video" => "https://www.youtube.com/watch?v=rOvtC8zr5tc",
        "rarity" => "one-of-a-kind",
        "cat" => "Space Agent",
        "faction" => "space_force",
        "weapon1Name" => "Boring F-Thrower",
        "weapon1Image" => "img/weapons/sm/Boring_F-Thrower.png",
        "weapon2Name" => "LRB Pulse",
        "weapon2Image" => "img/weapons/sm/LRB_Pulse.png",
        "ability" => "Rocket Boost",
        "minSkill" => "8",
        "maxSkill" => "30",
        "price" => "Coming Soon",
        "trade" => null,
        "available" => null,
        "insignia" => "img/ranks/space-7.png",
        "rankName" => "Senior Master Sergeant (SMSgt)",
        "extraClasses" => "cards__card--labels-sm",
        "description" => "Melon Nusk is Spaceman's recreation of the famous Elon Musk. Only available in a minimal supply and one of the RAREST Fortis in the Warzone and limited supply. Following the Space Force unit code, Melon Nusk is Joining Warzone introducing the Space Force's Fame Units. Melon started at the Master Sergeant Class and can be upgraded. He can fire the MRB Pulse Gun electric Waves and the Flamethrower. His unique ability is Rocket Launch, allowing him to Fly over the Warzone for a limited time of 5 seconds. Melon Nusk will be purchasable for a limited time as an NFT in-game and is tradable and rankable. Melon can be Ranked with a maximum level of 15 and will start at level 3.",
        "stats" => [
            [
                array(
                    "title" => "DPR",
                    "value" => "2.2",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "130",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "6",
                    "icon" => "img/icon/pw3.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "40",
                    "icon" => "img/icon/pw4.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.1",
                    "icon" => "img/icon/pw5.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "3-15",
                    "icon" => "img/icon/pw6.png"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "2.2",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "130",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "6",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "40",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "0.1",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Skill",
                    "value" => "3-15",
                    "icon" => "img/icon/map_marker.png"
                )
            ]
        ],
        "specs" => ["Limited Edition 5 Only"]
    )
];

// Aliens
$ALIENS = [
    array(
        "id" => 1,
        "name" => "Ethenum AS-ST",
        "coverSM" => "img/aliens/sm/alien.png",
        "cover" => "img/aliens/alien.png",
        "cover2" => "img/aliens/alien.png",
        "video" => "https://www.youtube.com/watch?v=SBMYMVOjbeM",
        "rarity" => "eth",
        "cat" => "Battle Warrior",
        "faction" => "alien_chain",
        "weapon1Name" => "Ethenum XAR-S",
        "weapon1Image" => "img/weapons/ethenum.png",
        "weapon2Name" => null,
        "weapon2Image" => null,
        "ability" => "XAR Beam",
        "minSkill" => "1",
        "maxSkill" => "20",
        "price" => "Event Only",
        "trade" => "Non-Tradable",
        "available" => "Unlimited",
        "insignia" => "img/ranks/alien-1.png",
        "rankName" => "Ethenum Sentinel",
        "description" => "The Ethereum Sentinel is an Advanced Form of the ETH Chain that evolved technologically and built a fleet of AI Aliens with a main target of destroying the BSC smartchain as we know it. The Sentinels are powered by the Shard's the core power behind the Infinity Knox elemental power. The Alien is equipped with multiple Assault Laser Beams, Rocket Launcher's and  the main destructive weapon the XAR core Beam that is capable of destroying the fort. the alien will appear in event only battles and, if killed, will drop massive loots and one of a kind weapons.",
        "stats" => [
            [
                array(
                    "title" => "DPR",
                    "value" => "100",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "6000",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "50",
                    "icon" => "img/icon/pw3.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "75",
                    "icon" => "img/icon/pw4.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "Light Speed Beam",
                    "icon" => "img/icon/pw5.png"
                ),
                array(
                    "title" => "Level",
                    "value" => "Sentinel",
                    "icon" => "img/icon/pw6.png",
                    "small" => "yes"
                )
            ],
            [
                array(
                    "title" => "DPR",
                    "value" => "100",
                    "icon" => "img/icon/pw1.png"
                ),
                array(
                    "title" => "HP",
                    "value" => "6000",
                    "icon" => "img/icon/pw2.png"
                ),
                array(
                    "title" => "Armor",
                    "value" => "50",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Bullet Speed",
                    "value" => "75",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Fire Rate",
                    "value" => "Light Speed Beam",
                    "icon" => "img/icon/map_marker.png"
                ),
                array(
                    "title" => "Level",
                    "value" => "Sentinel",
                    "icon" => "img/icon/map_marker.png",
                    "small" => true
                )
            ]
        ],
        "specs" => ["Limited to Event Only", "Not tradable or rankable"]
    )
];

// Weapons
$WEAPONS = [
    array(
        "id" => 1,
        "name" => "50 CAL",
        "cover" => "img/weapons/50 CAL.png",
        "coverHD" => "img/weapons/hd/50 CAL.png",
        "description" => "The M2010 Enhanced Sniper Rifle (ESR), formerly known as the XM2010 and M24 Reconfigured Sniper Weapon System, is a sniper rifle developed by PEO Soldier for the United States Army.[3] It is derived from and replaced the M24 Sniper Weapon System, and was designed to give snipers longer range in the mountainous and desert terrain.",
        "category" => "heavy-sniper",
        "price" => "Listing Soon",
        "efficiency" => "High Velocity Bullet, Fast impact without Lead, High damage.",
        "rarity" => "ultimate",
        "skin" => "img/weapons/hd/50 CAl Skin.png",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Auto / Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "7s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "2s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "50",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "1",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Heavy Sniper",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Main Weapon",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 2,
        "name" => "AWM .338",
        "cover" => "img/weapons/awm_gun.png",
        "coverHD" => "img/weapons/hd/awm_gun.png",
        "description" => "The AWM in the .338 Lapua Magnum (8.6×70mm) calibre was designed as a dedicated long-range sniper rifle. The rifle is fitted with stainless steel, fluted, 686 mm (27.0 in) barrel, which research has found to be the best compromise between muzzle velocity, weight, and length.",
        "category" => "heavy-sniper",
        "price" => "Listing Soon",
        "efficiency" => "High Velocity Bullet, Quick Shots + Piercing.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "17",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "6s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "2s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "50",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "1",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Heavy Sniper",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Secondary Weapon (Coming Soon)",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 3,
        "name" => "M240",
        "cover" => "img/weapons/Gun 240.png",
        "coverHD" => "img/weapons/hd/Gun 240.png",
        "description" => "The M240 is adapted as a coaxial machine gun for tanks and 7.62 mm firepower on light armoured vehicles.The M240 is part of the secondary armament on the U.S. Army M1 series Abrams tank, M2/M3 series Bradley Fighting Vehicle, and the U.S. Marine Corps LAV-25.",
        "category" => "hmg",
        "price" => "Listing Soon",
        "efficiency" => "Supressive Fast fire, Good at long range.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Full Auto",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "1",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "75",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "8s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "2s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "16",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.05",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Heavy Machine Gun",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Secondary Weapon (Coming Soon)",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 4,
        "name" => "M110",
        "cover" => "img/weapons/gun m110.png",
        "coverHD" => "img/weapons/hd/gun m110.png",
        "description" => "The M110 Semi-Automatic Sniper System (M110 SASS) is an American semi-automatic precision rifle that is chambered for the 7.62×51mm NATO round. It was adopted by the U.S. military following the 2005 US Army Semi-Automatic Sniper Rifle (XM110 SASR) competition.",
        "category" => "sniper",
        "price" => "Listing Soon",
        "efficiency" => "Fast Reload and Effective at Medium range snipes.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Semi Auto / Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "9.5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "10",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "16s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "3s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "35",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.35",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Sniper",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Secondary Weapon (Coming Soon)",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 5,
        "name" => "M249",
        "cover" => "img/weapons/Gun M249.png",
        "coverHD" => "img/weapons/hd/Gun M249.png",
        "description" => "The M249 light machine gun (LMG), also known as the M249 Squad Automatic Weapon (SAW), continues to be the manufacturer's designation. M249, is the American adaptation of the Belgian FN Minimi, a light machine gun manufactured by the Belgian company FN Herstal (FN).",
        "category" => "lmg",
        "price" => "Listing Soon",
        "efficiency" => "Faster Reload then most LMG's, good at short range fights.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Full Auto",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "1.1",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "75",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "7s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "2s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "17",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.06",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "LMG",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Main Weapon",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 6,
        "name" => "HK416",
        "cover" => "img/weapons/HK416.png",
        "coverHD" => "img/weapons/hd/HK416.png",
        "description" => "The Heckler & Koch HK416 is a gas-operated assault rifle chambered for the 5.56x45mm NATO cartridge. It is designed and manufactured by the German company Heckler & Koch. It uses a proprietary short-stroke, gas piston system from Heckler & Koch's earlier G36 family of rifles.",
        "category" => "automatic-rifle",
        "price" => "Listing Soon",
        "efficiency" => "Medium Range Assault Rifle with Good damage output.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Auto / Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "0.9",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "30",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "4s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "1s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "17",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.08",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Assault Rifle",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Main Weapon",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 7,
        "name" => "MK 16 SCAR-L",
        "cover" => "img/weapons/MK 16 SCRAL.png",
        "coverHD" => "img/weapons/hd/MK 16 SCRAL.png",
        "description" => "The FN SCAR (Special Operations Forces Combat Assault Rifle) is a family of gas-operated (short-stroke gas piston) automatic rifles developed by Belgian manufacturer FN Herstal (FN) in 2004. The SCAR-L, for \"light\", is chambered in 5.56x45mm NATO ",
        "category" => "automatic-rifle",
        "price" => "Listing Soon",
        "efficiency" => "Long Range and Above Average Damage output.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Auto / Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "1",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "4s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "1s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "18.7",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.057",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Assault Rifle",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Main Weapon",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 8,
        "name" => "MK 17 SCAR-H",
        "cover" => "img/weapons/MK 17 SCAR-H.png",
        "coverHD" => "img/weapons/hd/MK 17 SCAR-H.png",
        "description" => "The FN SCAR (Special Operations Forces Combat Assault Rifle) is a family of gas-operated (short-stroke gas piston) automatic rifles developed by Belgian manufacturer FN Herstal (FN) in 2004.",
        "category" => "automatic-rifle",
        "price" => "Listing Soon",
        "efficiency" => "Slower Bullet Speed and Greater Damage and Medium Range.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Auto / Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "0.92",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "40",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "5s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "1s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "17",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.08",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Assault Rifle",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Secondary Weapon (Coming Soon)",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 9,
        "name" => "MK 18 MOD",
        "cover" => "img/weapons/MK 18 MOD.png",
        "coverHD" => "img/weapons/hd/MK 18 MOD.png",
        "description" => "The CQBR features a 10.3 in (262 mm) length barrel (similar to the Colt Commando short-barreled M16 variants of the past) which makes the weapon significantly more compact, thus making it easier to use in, and around, vehicles and in tight, confined spaces.",
        "category" => "automatic-rifle",
        "price" => "Listing Soon",
        "efficiency" => "Quick Fire output with improved reload speed.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Auto / Burst / Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "1.1",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "35",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "3s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "1s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "18.7",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.057",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Assault Rifle",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Secondary Weapon (Coming Soon)",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 10,
        "name" => "Beretta M9",
        "cover" => "img/weapons/Pistol.png",
        "coverHD" => "img/weapons/hd/Pistol.png",
        "description" => "The Beretta M9, officially the Pistol, Semiautomatic, 9mm, M9, is the designation for the Beretta 92FS semi-automatic pistol used by the United States Armed Forces. The M9 was adopted by the United States military as their service pistol in 1985.",
        "category" => "hand-gun",
        "price" => "Listing Soon",
        "efficiency" => "Very Fast Fire Mechanism and good damage.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Single / Semi Auto",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "1",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "10",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "3s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "1s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "18",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.05",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Hand Gun",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Main Weapon",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 11,
        "name" => "M1014",
        "cover" => "img/weapons/Shotgun.png",
        "coverHD" => "img/weapons/hd/Shotgun.png",
        "description" => "is a semi-automatic shotgun produced by Italian firearm manufacturer Benelli Armi SpA, and the last of the \"Benelli Super 90\" series of semi-automatic shotguns. The M4 uses a proprietary action design called the \"auto-regulating gas-operated\" (ARGO) system, which was created specifically for the weapon.",
        "category" => "shotgun",
        "price" => "Listing Soon",
        "efficiency" => "Deadly at Short Range, Heavy damage.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Single Manual Action",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "8",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "8",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "5s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "3s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "19",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.1",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Shotgun",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Main Weapon",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 12,
        "name" => "XM2010",
        "cover" => "img/weapons/XM2010.png",
        "coverHD" => "img/weapons/hd/XM2010.png",
        "description" => "The M2010 Enhanced Sniper Rifle (ESR), formerly known as the XM2010 and M24 Reconfigured Sniper Weapon System, is a sniper rifle developed by PEO Soldier for the United States Army.[3] It is derived from and replaced the M24 Sniper Weapon System.",
        "category" => "sniper",
        "price" => "Listing Soon",
        "efficiency" => "Quick Reloads and Piercing power, very good at long range.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Bolt Action Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "11",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "10s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "3s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "35",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.35",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Sniper",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Main Weapon",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 13,
        "name" => "50 CAL CAMO",
        "cover" => "img/weapons/50 CAl Skin.png",
        "coverHD" => "img/weapons/hd/50 CAl Skin.png",
        "description" => "The M2010 Enhanced Sniper Rifle (ESR), formerly known as the XM2010 and M24 Reconfigured Sniper Weapon System, is a sniper rifle developed by PEO Soldier for the United States Army.[3] It is derived from and replaced the M24 Sniper Weapon System, and was designed to give snipers longer range in the mountainous and desert terrain of the War in Afghanistan.",
        "category" => "heavy-sniper",
        "price" => "Listing Soon",
        "efficiency" => "Improved version of the 50 CAL with reduced recoil and skin.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Auto / Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "7s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "2s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "50",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "1",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Heavy Sniper",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Main Weapon",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 14,
        "name" => "MP5K",
        "cover" => "img/weapons/MP5K.png",
        "coverHD" => "img/weapons/hd/MP5K.png",
        "description" => "he MP5 (German: Maschinenpistole 5) is a 9x19mm Parabellum submachine gun, developed in the 1960s by a team of engineers from the German small arms manufacturer Heckler & Koch GmbH (H&K) of Oberndorf am Neckar.",
        "category" => "smg",
        "price" => "Listing Soon",
        "efficiency" => "Very Fast Bullet Output, good at close range and fast reload.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Full Auto",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "0.75",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "40",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "2.5s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "1s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "18.5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.06",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "SMG",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Main Weapon",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 15,
        "name" => "M4A1",
        "cover" => "img/weapons/M4A1.png",
        "coverHD" => "img/weapons/hd/M4A1.png",
        "description" => "The M4 carbine is a 5.56×45mm NATO, air-cooled, gas-operated,[b] magazine-fed, carbine, assault rifle developed in the United States during the 1980s. It is a shortened version of the M16A2 assault rifle.",
        "category" => "automatic-rifle",
        "price" => "Listing Soon",
        "efficiency" => "Close to Medium Range Assault Rifle with Very good output damage.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Auto / Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "0.87",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "40",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "4s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "1s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "18.5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.06",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Assault Rifle",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Secondary Weapon (Coming Soon)",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 16,
        "name" => "M4A16",
        "cover" => "img/weapons/M16.png",
        "coverHD" => "img/weapons/hd/M16.png",
        "description" => "The M4 carbine is a 5.56×45mm NATO, air-cooled, gas-operated,[b] magazine-fed, carbine, assault rifle developed in the United States during the 1980s. It is a shortened version of the M16A2 assault rifle.",
        "category" => "automatic-rifle",
        "price" => "Listing Soon",
        "efficiency" => "Medium to Long Range Assault Rifle with Advanced Damage and burst firing speed.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Single / Burst 3 Shots",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "0.8",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "30",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "5s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "1s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "16",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.085",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Assault Rifle",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Secondary Weapon (Coming Soon)",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 17,
        "name" => "SAW M249",
        "cover" => "img/weapons/M249_SAW.png",
        "coverHD" => "img/weapons/hd/M249_SAW.png",
        "description" => "he M249 light machine gun (LMG), also known as the M249 Squad Automatic Weapon (SAW), which continues to be the manufacturer's designation,[3] and formally written as Light Machine Gun, 5.56 mm, M249, is the American adaptation of the Belgian FN Minimi.",
        "category" => "lmg",
        "price" => "Listing Soon",
        "efficiency" => "Faster Reload then most LMG's, good at short range fights, fast output saw edition.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Full Auto",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "0.9",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "120",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "8s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "2s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "17",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.06",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "LMG",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Main Weapon",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 18,
        "name" => "CZ Bren 2 MS Carbine",
        "cover" => "img/weapons/CZ_Bren_2_MS_Carbine.png",
        "coverHD" => "img/weapons/hd/CZ_Bren_2_MS_Carbine.png",
        "description" => "is a gas-operated modular assault rifle designed and manufactured by Česká zbrojovka Uherský Brod.[1] The modular design enables users to change the calibre of the weapon to 5.56×45mm NATO or 7.62×39mm intermediate cartridges by quick change of barrel with gas tubes, breech block, magazine bay and magazine.",
        "category" => "automatic-rifle",
        "price" => "Listing Soon",
        "efficiency" => "Best at medium to long range, fast delivery and strong output damage.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Auto / Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "1.1",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "35",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "3s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "1s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "18",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.065",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Assault Rifle",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Secondary Weapon (Coming Soon)",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 19,
        "name" => "AR15 SCOPE",
        "cover" => "img/weapons/AR15 SCOPE.png",
        "coverHD" => "img/weapons/hd/AR15 SCOPE.png",
        "description" => "The AR-15 is closely related to the military M16 and M4 Carbine rifles, which all share the same core design, first patented for use in the AR-10, featuring a gas-operated, rotating bolt (combined with an integral piston) instead of conventional direct impingement.",
        "category" => "automatic-rifle",
        "price" => "Listing Soon",
        "efficiency" => "Medium to Long Range Assault Rifle with Moderate damage and recoil.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Auto / Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "1",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "40",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "4s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "1s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "18",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.065",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Assault Rifle",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Main Weapon",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 20,
        "name" => "M136 AT4",
        "cover" => "img/weapons/M136_AT4.png",
        "coverHD" => "img/weapons/hd/M136_AT4.png",
        "description" => "The AT4[a] is a Swedish 84 mm (3.31 in) unguided, man-portable, single-shot, disposable, recoilless smoothbore anti-tank weapon built by Saab Bofors Dynamics (previously Bofors Anti-Armour Systems)  making it one of the most common light anti-tank weapons in the world.",
        "category" => "rocket-launcher",
        "price" => "Listing Soon",
        "efficiency" => "High Damage RPG with AOE effect on impact.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "Unlimited",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "1 Rocket Every 3s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "3s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "15",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Rocket Launcher",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Main Weapon",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 21,
        "name" => "M32",
        "cover" => "img/weapons/M32.png",
        "coverHD" => "img/weapons/hd/M32.png",
        "description" => "is a lightweight 40 mm six-shot revolver-type grenade launcher (variations also fire 37/38mm) developed and manufactured in South Africa by Milkor (Pty) Ltd. The MGL was demonstrated as a concept to the South African Defence Force (SADF) in 1981.",
        "category" => "grenade-launcher",
        "price" => "Listing Soon",
        "efficiency" => "High Damage GRENADE's with Fast-Fire ability.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "6",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "6s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "2s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "15",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Grenade Launcher",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Secondary Weapon (Coming Soon)",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 22,
        "name" => "Mossberg 590A1",
        "cover" => "img/weapons/Mossberg 590A1.png",
        "coverHD" => "img/weapons/hd/Mossberg 590A1.png",
        "description" => "The Mossberg 500 (M500) is a series of pump-action shotguns manufactured by O.F. Mossberg & Sons.[1] The 500 series comprises widely varying models of hammerless repeaters, all of which share the same basic receiver and action, but differ in bore size, barrel length, choke options, magazine capacity, stock and forearm materials.",
        "category" => "shotgun",
        "price" => "Listing Soon",
        "efficiency" => "Quick Release Shotgun with auto firing up to 5 rounds, effective at close range.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Semi Auto / Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "7.5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "7s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "2s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "19",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.1",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Shotgun",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Secondary Weapon (Coming Soon)",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 23,
        "name" => "Boring F-Thrower",
        "cover" => "img/weapons/Boring_F-Thrower.png",
        "coverHD" => "img/weapons/hd/Boring_F-Thrower.png",
        "description" => "The Boring Flame Thrower is a ranged incendiary device designed to project a controllable jet of fire with high pressure, and it has been designed to fire in the medium range.",
        "category" => "flame-thrower",
        "price" => "Listing Soon",
        "efficiency" => "Quick Fire output and AOE effect with Lasting effect of 3 seconds.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Category",
                "value" => "Flame Thrower",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Main Weapon",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 24,
        "name" => "LRB Pulse",
        "cover" => "img/weapons/LRB_Pulse.png",
        "coverHD" => "img/weapons/hd/LRB_Pulse.png",
        "description" => "The Pulse Gun is powered by the Pulse Wall generator Hub, which charges over time and provides tremendous power concentrated and fired in sequence.",
        "category" => "pulse",
        "price" => "Listing Soon",
        "efficiency" => "Very Fast Long Range Electric Energy Pulses with quick reload.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Category",
                "value" => "Pulse",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Main Weapon",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 25,
        "name" => "AK47",
        "cover" => "img/weapons/AKM.png",
        "coverHD" => "img/weapons/hd/AKM.png",
        "description" => "The AK-47, officially known as the Avtomat Kalashnikov (Russian: Автомат Калашникова, lit. 'Kalashnikov's automatic [rifle]'; also known as the Kalashnikov or just AK), is a gas-operated assault rifle that is chambered for the 7.62×39mm cartridge. After more than seven decades, the AK-47 model and its variants remain the most popular and widely used rifles in the world.",
        "category" => "automatic-rifle",
        "price" => "Listing Soon",
        "efficiency" => "High Damage Output with slower Bullet speed, very effective at medium range.",
        "rarity" => "ultimate",
        "skin" => "img/weapons/hd/AKM_GOLD.png",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Auto / Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "0.87",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "40",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "4s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "1s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "18.5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.06",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Assault Rifle",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Primary Weapon",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 26,
        "name" => "Krieg 552",
        "cover" => "img/weapons/SG552.png",
        "coverHD" => "img/weapons/hd/SG552.png",
        "description" => "The Krieg 552 is a well-rounded assault rifle. Accurate and has a good unscoped rate of fire with moderate damage. It is light and easy to control.",
        "category" => "automatic-rifle",
        "price" => "Listing Soon",
        "efficiency" => "Medium Range Assault Rifle with High damage output, slower reload time.",
        "rarity" => "ultimate",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Auto / Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "0.87",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "40",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "4s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "1s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "18.5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.06",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Assault Rifle",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Secondary Weapon (Coming Soon)",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 27,
        "name" => "Ethenum XAR-S",
        "cover" => "img/weapons/ethenum.png",
        "coverHD" => "img/weapons/hd/ethenum.png",
        "description" => "The XAR-S is a powerful element's generation weapon that projects beams sourced from the shard's energy with Lightspeed projection. The delivery is instant & lethal.",
        "category" => "pulse",
        "price" => "Event Only",
        "efficiency" => "A super Powerful Beam Array with Elemental power outup, deals heavy damage on impact.",
        "rarity" => "one-of-a-kind",
        "stats" => [
            array(
                "title" => "Firing Mode",
                "value" => "Auto / Single",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "DPR",
                "value" => "0.87",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "RPM",
                "value" => "40",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Full Fire Delivery Speed",
                "value" => "4s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Reload Speed",
                "value" => "1s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Bullet Speed",
                "value" => "18.5",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Fire Rate",
                "value" => "0.06",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Recoil",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rank",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Supply",
                "value" => "1",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Category",
                "value" => "Assault Rifle",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Status",
                "value" => "Primary Weapon",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    )
];

// Infinity Knox
$I_KNOX = [
    array(
        "id" => 1,
        "name" => "Gold Quasar",
        "cover" => "img/infinity/Speed Knox Infinity-min.png",
        "coverSM" => "img/infinity/sm/Speed Knox Infinity-min.png",
        "description" => "The infinity Knoxe's are Elemental Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time or even permanently, the power of the QUASAR version is Absorbed from the Core Shards that are harvested from the Ethunum Aliens Sentinels and their Motherships, the Power Factor from the Quasar is Speed allowing the Forti's to Move Faster in the warzone.",
        "category" => "quasar",
        "price" => "Community",
        "rarity" => "one-of-a-kind",
        "group" => "gold",
        "video" => "https://www.youtube.com/watch?v=MdHL7gcZ-WM",
        "stats" => [
            array(
                "title" => "Model",
                "value" => "Gold",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Series",
                "value" => "Quasar",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "1% + Global Perma",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Speed",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Starting Value",
                "value" => "3 BNB",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Unlocked Only EA",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rarity",
                "value" => "One of a kind",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Mints",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Badge",
                "value" => "Gold + Effect",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 2,
        "name" => "Gold Supernova",
        "cover" => "img/infinity/Health Knox Infinity-min.png",
        "coverSM" => "img/infinity/sm/Health Knox Infinity-min.png",
        "description" => "The infinity Knoxe's are Elemental Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time or even permanently, the power of the SUPERNOVA version is Absorbed from the Core Shards that are harvested from the Ethunum Aliens Sentinels and their Motherships, the Power Factor from the Supernova is Health allowing the Forti's to Acquire HP & Heal in the warzone.",
        "category" => "supernova",
        "price" => "Community",
        "rarity" => "one-of-a-kind",
        "group" => "gold",
        "video" => "https://www.youtube.com/watch?v=AHDdsCt-wXM",
        "stats" => [
            array(
                "title" => "Model",
                "value" => "Gold",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Series",
                "value" => "Supernova",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "1% + Global Perma",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Health",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Starting Value",
                "value" => "3 BNB",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Unlocked Only EA",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rarity",
                "value" => "One of a kind",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Mints",
                "value" => "20",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Badge",
                "value" => "Gold + Effect",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 3,
        "name" => "Gold Star",
        "cover" => "img/infinity/Knox Card Star-min.png",
        "coverSM" => "img/infinity/sm/Knox Card Star-min.png",
        "description" => "The infinity Knoxe's are Elemental Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time or even permanently, the power of the STAR version is Absorbed from the Core Shards that are harvested from the Ethunum Aliens Sentinels and their Motherships, the Power Factor from the Star is Armor allowing the Forti's to Sustain Heavy Damage in the warzone.",
        "category" => "star",
        "price" => "Community",
        "rarity" => "one-of-a-kind",
        "group" => "gold",
        "video" => "https://www.youtube.com/watch?v=3rOoIKRQFNs",
        "stats" => [
            array(
                "title" => "Model",
                "value" => "Gold",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Series",
                "value" => "Star",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "1% + Global Perma",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Armor",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Starting Value",
                "value" => "3 BNB",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Unlocked Only EA",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rarity",
                "value" => "One of a kind",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Mints",
                "value" => "21",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Badge",
                "value" => "Gold + Effect",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 4,
        "name" => "Gold Horizon",
        "cover" => "img/infinity/Knox Card Horizon-min.png",
        "coverSM" => "img/infinity/sm/Knox Card Horizon-min.png",
        "description" => "The infinity Knoxe's are Elemental Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time or even permanently, the power of the HORIZON version is Absorbed from the Core Shards that are harvested from the Ethunum Aliens Sentinels and their Motherships, the Power Factor from the Horizon is Damage allowing the Forti's to Deal Higher Damage delivery in the warzone.",
        "category" => "horizon",
        "price" => "Community",
        "rarity" => "one-of-a-kind",
        "group" => "gold",
        "video" => "https://www.youtube.com/watch?v=-RwOnCMH4P4",
        "stats" => [
            array(
                "title" => "Model",
                "value" => "Gold",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Series",
                "value" => "Horizon",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "1% + Global Perma",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Power",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Starting Value",
                "value" => "3 BNB",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Unlocked Only EA",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rarity",
                "value" => "One of a kind",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Mints",
                "value" => "21",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Badge",
                "value" => "Gold + Effect",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 5,
        "name" => "Gold Comet",
        "cover" => "img/infinity/Comet Knox Infinity-min.png",
        "coverSM" => "img/infinity/sm/Comet Knox Infinity-min.png",
        "description" => "The infinity Knoxe's are Elemental Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time or even permanently, the power of the COMET version is Absorbed from the Core Shards that are harvested from the Ethunum Aliens Sentinels and their Motherships, the Power Factor from the Comet is Skill allowing the Forti's to Raise in Rank Faster in the warzone. ",
        "category" => "comet",
        "price" => "Community",
        "rarity" => "one-of-a-kind",
        "group" => "gold",
        "video" => "https://www.youtube.com/watch?v=U-YpQLKYvTw",
        "stats" => [
            array(
                "title" => "Model",
                "value" => "Gold",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Series",
                "value" => "Comet",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "1% + Global Perma",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Skill",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Starting Value",
                "value" => "3 BNB",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Unlocked Only EA",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rarity",
                "value" => "One of a kind",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Mints",
                "value" => "21",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Badge",
                "value" => "Gold + Effect",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 6,
        "name" => "Emerald Quasar",
        "cover" => "img/infinity/Knox-Quasar-[Emerald]-min.png",
        "coverSM" => "img/infinity/sm/Knox-Quasar-[Emerald]-min.png",
        "description" => "The infinity Knoxe's are Elemental Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time or even permanently, the power of the QUASAR version is Absorbed from the Core Shards that are harvested from the Ethunum Aliens Sentinels and their Motherships, the Power Factor from the Quasar is Speed allowing the Forti's to Move Faster in the warzone.",
        "category" => "quasar",
        "price" => "350K BFK",
        "rarity" => "supreme-rare",
        "group" => "emerald",
        "video" => "https://www.youtube.com/watch?v=ghVTYRzllRM",
        "stats" => [
            array(
                "title" => "Model",
                "value" => "Emerald",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Series",
                "value" => "Quasar",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "0.85% + Global Perma",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Speed",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Starting Value",
                "value" => "2.5 BNB",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "At Launch",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rarity",
                "value" => "Supereme Rare",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Mints",
                "value" => "9",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Badge",
                "value" => "Emerald + Effect",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 7,
        "name" => "Emerald Supernova",
        "cover" => "img/infinity/Knox-Supernova-[Emerald]-min.png",
        "coverSM" => "img/infinity/sm/Knox-Supernova-[Emerald]-min.png",
        "description" => "The infinity Knoxe's are Elemental Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time or even permanently, the power of the SUPERNOVA version is Absorbed from the Core Shards that are harvested from the Ethunum Aliens Sentinels and their Motherships, the Power Factor from the Supernova is Health allowing the Forti's to Acquire HP & Heal in the warzone.",
        "category" => "supernova",
        "price" => "350K BFK",
        "rarity" => "supreme-rare",
        "group" => "emerald",
        "video" => "https://www.youtube.com/watch?v=JCEx6FjNQr4",
        "stats" => [
            array(
                "title" => "Model",
                "value" => "Emerald",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Series",
                "value" => "Supernova",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "0.85% + Global Perma",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Health",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Starting Value",
                "value" => "2.5 BNB",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "At Launch",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rarity",
                "value" => "Supereme Rare",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Mints",
                "value" => "9",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Badge",
                "value" => "Emerald + Effect",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 8,
        "name" => "Emerald Star",
        "cover" => "img/infinity/Knox-Card-Star-[Emerald]-min.png",
        "coverSM" => "img/infinity/sm/Knox-Card-Star-[Emerald]-min.png",
        "description" => "The infinity Knoxe's are Elemental Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time or even permanently, the power of the STAR version is Absorbed from the Core Shards that are harvested from the Ethunum Aliens Sentinels and their Motherships, the Power Factor from the Star is Armor allowing the Forti's to Sustain Heavy Damage in the warzone.",
        "category" => "star",
        "price" => "350K BFK",
        "rarity" => "supreme-rare",
        "group" => "emerald",
        "video" => "https://www.youtube.com/watch?v=ADwMb1ZZsi0",
        "stats" => [
            array(
                "title" => "Model",
                "value" => "Emerald",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Series",
                "value" => "Star",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "0.85% + Global Perma",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Armor",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Starting Value",
                "value" => "2.5 BNB",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "At Launch",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rarity",
                "value" => "Supereme Rare",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Mints",
                "value" => "9",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Badge",
                "value" => "Emerald + Effect",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 9,
        "name" => "Emerald Horizon",
        "cover" => "img/infinity/Knox-Horizon-[Emerald]-min.png",
        "coverSM" => "img/infinity/sm/Knox-Horizon-[Emerald]-min.png",
        "description" => "The infinity Knoxe's are Elemental Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time or even permanently, the power of the HORIZON version is Absorbed from the Core Shards that are harvested from the Ethunum Aliens Sentinels and their Motherships, the Power Factor from the Horizon is Damage allowing the Forti's to Deal Higher Damage delivery in the warzone.",
        "category" => "horizon",
        "price" => "350K BFK",
        "rarity" => "supreme-rare",
        "group" => "emerald",
        "video" => "https://www.youtube.com/watch?v=Q02PEwznonc",
        "stats" => [
            array(
                "title" => "Model",
                "value" => "Emerald",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Series",
                "value" => "Horizon",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "0.85% + Global Perma",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Power",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Starting Value",
                "value" => "2.5 BNB",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "At Launch",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rarity",
                "value" => "Supereme Rare",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Mints",
                "value" => "10",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Badge",
                "value" => "Emerald + Effect",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 10,
        "name" => "Emerald Comet",
        "cover" => "img/infinity/Knox-Comet-[Emerald]-min.png",
        "coverSM" => "img/infinity/sm/Knox-Comet-[Emerald]-min.png",
        "description" => "The infinity Knoxe's are Elemental Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time or even permanently, the power of the COMET version is Absorbed from the Core Shards that are harvested from the Ethunum Aliens Sentinels and their Motherships, the Power Factor from the Comet is Skill allowing the Forti's to Raise in Rank Faster in the warzone. ",
        "category" => "comet",
        "price" => "350K BFK",
        "rarity" => "supreme-rare",
        "group" => "emerald",
        "video" => "https://www.youtube.com/watch?v=-CARvTE6uks",
        "stats" => [
            array(
                "title" => "Model",
                "value" => "Emerald",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Series",
                "value" => "Comet",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "0.85% + Global Perma",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Skill",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Starting Value",
                "value" => "2.5 BNB",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "At Launch",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Rarity",
                "value" => "Supereme Rare",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Mints",
                "value" => "10",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Badge",
                "value" => "Emerald + Effect",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
];

// Power Up Knox
$P_KNOX = [
    array(
        "id" => 1,
        "name" => "H-Pulse",
        "cover" => "img/infinity/Knox Card Horizon-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Horizon is Damage allowing the Forti's to Deal More DPS Faster in the warzone.",
        "category" => "horizon",
        "price" => "2500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Horizon",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "10% Buff on damage",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Power",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "6s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--legacy\">H</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 2,
        "name" => "G-Pulse",
        "cover" => "img/infinity/Knox Card Horizon-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Horizon is Damage allowing the Forti's to Deal More DPS Faster in the warzone.",
        "category" => "horizon",
        "price" => "4500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Horizon",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "30% Buff on damage",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Power",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "4s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--rare\">G</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 3,
        "name" => "M-Pulse",
        "cover" => "img/infinity/Knox Card Horizon-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Horizon is Damage allowing the Forti's to Deal More DPS Faster in the warzone.",
        "category" => "horizon",
        "price" => "7500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Horizon",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "5% Buff on damage",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Power",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "15s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--ultra-rare\">M</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 4,
        "name" => "X-Pulse",
        "cover" => "img/infinity/Knox Card Horizon-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Horizon is Damage allowing the Forti's to Deal More DPS Faster in the warzone.",
        "category" => "horizon",
        "price" => "10000 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Horizon",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "10% Buff on damage",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Power",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "30s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--ultimate\">X</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 5,
        "name" => "I-Pulse",
        "cover" => "img/infinity/Knox Card Horizon-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Horizon is Damage allowing the Forti's to Deal More DPS Faster in the warzone.",
        "category" => "horizon",
        "price" => "15000 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Horizon",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "10% Buff on damage - Player and Team",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Power",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "10s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--supreme-rare\">I</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 6,
        "name" => "H-Giga",
        "cover" => "img/infinity/Knox Card Star-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Star is Armor allowing the Forti's to Sustain Heavy Damage in the warzone.",
        "category" => "star",
        "price" => "2500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Star",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+5 Armor",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Armor",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "6s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--legacy\">H</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 7,
        "name" => "G-Giga",
        "cover" => "img/infinity/Knox Card Star-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Star is Armor allowing the Forti's to Sustain Heavy Damage in the warzone.",
        "category" => "star",
        "price" => "4500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Star",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+10 Armor",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Armor",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "4s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--rare\">G</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 8,
        "name" => "M-Giga",
        "cover" => "img/infinity/Knox Card Star-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Star is Armor allowing the Forti's to Sustain Heavy Damage in the warzone.",
        "category" => "star",
        "price" => "7500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Star",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+5 Armor",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Armor",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "15s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--ultra-rare\">M</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 9,
        "name" => "X-Giga",
        "cover" => "img/infinity/Knox Card Star-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Star is Armor allowing the Forti's to Sustain Heavy Damage in the warzone.",
        "category" => "star",
        "price" => "10000 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Star",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+10 Armor",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Armor",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "30s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--ultimate\">X</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 10,
        "name" => "I-Giga",
        "cover" => "img/infinity/Knox Card Star-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Star is Armor allowing the Forti's to Sustain Heavy Damage in the warzone.",
        "category" => "star",
        "price" => "15000 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Star",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+5 Armor Team Base",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Armor",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "10s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--supreme-rare\">I</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 11,
        "name" => "H-Nova",
        "cover" => "img/infinity/Health Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Supernova is Health allowing the Forti's to Acquire HP & Heal in the warzone.",
        "category" => "supernova",
        "price" => "2500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Supernova",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+10 HP",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Health",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "Instant effect",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--legacy\">H</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 12,
        "name" => "G-Nova",
        "cover" => "img/infinity/Health Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Supernova is Health allowing the Forti's to Acquire HP & Heal in the warzone.",
        "category" => "supernova",
        "price" => "4500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Supernova",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+30 HP",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Health",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "Instant effect",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--rare\">G</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 13,
        "name" => "M-Nova",
        "cover" => "img/infinity/Health Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Supernova is Health allowing the Forti's to Acquire HP & Heal in the warzone.",
        "category" => "supernova",
        "price" => "7500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Supernova",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+50 HP",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Health",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "Start with 10 HP fills to 50 HP in 5 Seconds",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--ultra-rare\">M</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 14,
        "name" => "X-Nova",
        "cover" => "img/infinity/Health Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Supernova is Health allowing the Forti's to Acquire HP & Heal in the warzone.",
        "category" => "supernova",
        "price" => "10000 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Supernova",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+10 HP/s, 100 HP Max",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Health",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "Each Second add 10 HP for 10 Seconds",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--ultimate\">X</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 15,
        "name" => "I-Nova",
        "cover" => "img/infinity/Health Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Supernova is Health allowing the Forti's to Acquire HP & Heal in the warzone.",
        "category" => "supernova",
        "price" => "15000 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Supernova",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+5 HP/s, for Self & Team",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Health",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "Each Second add 5 HP for 30 Seconds",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--supreme-rare\">I</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 16,
        "name" => "H-Vortex",
        "cover" => "img/infinity/Speed Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Quasar is Speed allowing the Forti's to Move Faster in the warzone.",
        "category" => "quasar",
        "price" => "2500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Quasar",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+15% Speed",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Speed",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "6s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--legacy\">H</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 17,
        "name" => "G-Vortex",
        "cover" => "img/infinity/Speed Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Quasar is Speed allowing the Forti's to Move Faster in the warzone.",
        "category" => "quasar",
        "price" => "4500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Quasar",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+25% Speed +5% JUMP",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Speed",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "4s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--rare\">G</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 18,
        "name" => "M-Vortex",
        "cover" => "img/infinity/Speed Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Quasar is Speed allowing the Forti's to Move Faster in the warzone.",
        "category" => "quasar",
        "price" => "7500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Quasar",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+15% Speed +15% Higher Jump",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Speed",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "5s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--ultra-rare\">M</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 19,
        "name" => "X-Vortex",
        "cover" => "img/infinity/Speed Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Quasar is Speed allowing the Forti's to Move Faster in the warzone.",
        "category" => "quasar",
        "price" => "10000 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Quasar",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+20% Speed +20% Higher Jump",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Speed",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "10s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--ultimate\">X</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 20,
        "name" => "I-Vortex",
        "cover" => "img/infinity/Speed Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Quasar is Speed allowing the Forti's to Move Faster in the warzone.",
        "category" => "quasar",
        "price" => "15000 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Quasar",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+10% Speed +10% Higher Jump / Team",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Speed",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "15s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--supreme-rare\">I</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 21,
        "name" => "H-Unity",
        "cover" => "img/infinity/Comet Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Comet is Skill allowing the Forti's to Raise in Rank Faster in the warzone.",
        "category" => "comet",
        "price" => "2500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Comet",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+5 SKill Points on kill",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Skill",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "10s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--legacy\">H</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 22,
        "name" => "G-Unity",
        "cover" => "img/infinity/Comet Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Comet is Skill allowing the Forti's to Raise in Rank Faster in the warzone.",
        "category" => "comet",
        "price" => "4500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Comet",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+15 Skill Point on Kill",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Skill",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "15s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--rare\">G</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 23,
        "name" => "M-Unity",
        "cover" => "img/infinity/Comet Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Comet is Skill allowing the Forti's to Raise in Rank Faster in the warzone.",
        "category" => "comet",
        "price" => "7500 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Comet",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "Skill Sync / Gets 5 Skill Points extra on any enemy kill by team",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Skill",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "10s",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--ultra-rare\">M</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 24,
        "name" => "X-Unity",
        "cover" => "img/infinity/Comet Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Comet is Skill allowing the Forti's to Raise in Rank Faster in the warzone.",
        "category" => "comet",
        "price" => "10000 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Comet",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+20 Skill point for Game Win / Team effect",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Skill",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "Applies to game if rule is met",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--ultimate\">X</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
    array(
        "id" => 25,
        "name" => "I-Unity",
        "cover" => "img/infinity/Comet Knox Infinity-min.png",
        "description" => "Powerful Energy Boosters that empowers its bearer with unmatched powers over a period of time. the Power Factor from the Comet is Skill allowing the Forti's to Raise in Rank Faster in the warzone.",
        "category" => "comet",
        "price" => "15000 BFK",
        "stats" => [
            array(
                "title" => "Series",
                "value" => "Comet",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Power",
                "value" => "+1 Skill Points/s, +5 Skill points on kill, +10 Skill Points on Lose, + 25 Skill Points on win, Team effect",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Effect",
                "value" => "Skill",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Timer of Action",
                "value" => "20s, Applies to the game when rule is met",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Availability",
                "value" => "Sold in Marketplace",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Utility",
                "value" => "in game consumable",
                "icon" => "img/icon/map_marker.png"
            ),
            array(
                "title" => "Edition",
                "value" => "<div class=\"cards__knox__letter-tag cards__knox__letter--supreme-rare\">I</div>",
                "icon" => "img/icon/map_marker.png"
            )
        ]
    ),
];

// Forti Rankings
$RANKINGS = [

    // ARMY
    array(
        "id" => 1,
        "skill" => "Starting Stage or follow Card Rarity Starting Level",
        "level" => 1,
        "category" => "army",
        "name" => "Private (PV2)",
        "cover" => "img/ranks/army-1.png"
    ),
    array(
        "id" => 2,
        "skill" => "20",
        "level" => 2,
        "category" => "army",
        "name" => "Private First Class (PFC)",
        "cover" => "img/ranks/army-2.png"
    ),
    array(
        "id" => 3,
        "skill" => "40",
        "level" => 3,
        "category" => "army",
        "name" => "Corporal (CPL)",
        "cover" => "img/ranks/army-3.png"
    ),
    array(
        "id" => 4,
        "skill" => "60",
        "level" => 4,
        "category" => "army",
        "name" => "Specialist (SPC)",
        "cover" => "img/ranks/army-4.png"
    ),
    array(
        "id" => 5,
        "skill" => "85",
        "level" => 5,
        "category" => "army",
        "name" => "Seargeant (SGT)",
        "cover" => "img/ranks/army-5.png"
    ),
    array(
        "id" => 6,
        "skill" => "110",
        "level" => 6,
        "category" => "army",
        "name" => "Staff Seargeant (SSG)",
        "cover" => "img/ranks/army-6.png"
    ),
    array(
        "id" => 7,
        "skill" => "135",
        "level" => 7,
        "category" => "army",
        "name" => "Seargeant First Class (SFC)",
        "cover" => "img/ranks/army-7.png"
    ),
    array(
        "id" => 8,
        "skill" => "165",
        "level" => 8,
        "category" => "army",
        "name" => "Master Seargeant (MSG)",
        "cover" => "img/ranks/army-8.png"
    ),
    array(
        "id" => 9,
        "skill" => "195",
        "level" => 9,
        "category" => "army",
        "name" => "First Seargeant (1SG)",
        "cover" => "img/ranks/army-9.png"
    ),
    array(
        "id" => 10,
        "skill" => "235",
        "level" => 10,
        "category" => "army",
        "name" => "Seargeant Major (SGM)",
        "cover" => "img/ranks/army-10.png"
    ),

    // NAVY
    array(
        "id" => 11,
        "skill" => "Starting Stage",
        "level" => 1,
        "category" => "navy",
        "name" => "Seaman Apprentice (SA)",
        "cover" => "img/ranks/navy-1.png"
    ),
    array(
        "id" => 12,
        "skill" => "20",
        "level" => 2,
        "category" => "navy",
        "name" => "Seaman (SN)",
        "cover" => "img/ranks/navy-2.png"
    ),
    array(
        "id" => 13,
        "skill" => "40",
        "level" => 3,
        "category" => "navy",
        "name" => "Petty Officer Third Class (PO3)",
        "cover" => "img/ranks/navy-3.png"
    ),
    array(
        "id" => 14,
        "skill" => "60",
        "level" => 4,
        "category" => "navy",
        "name" => "Petty Officer Second Class (PO2)",
        "cover" => "img/ranks/navy-4.png"
    ),
    array(
        "id" => 15,
        "skill" => "85",
        "level" => 5,
        "category" => "navy",
        "name" => "Petty Officer First Class (PO1)",
        "cover" => "img/ranks/navy-5.png"
    ),
    array(
        "id" => 16,
        "skill" => "110",
        "level" => 6,
        "category" => "navy",
        "name" => "Chief Petty Officer (CPO)",
        "cover" => "img/ranks/navy-6.png"
    ),
    array(
        "id" => 17,
        "skill" => "135",
        "level" => 7,
        "category" => "navy",
        "name" => "Senior Chief Petty Officer (SCPO)",
        "cover" => "img/ranks/navy-7.png"
    ),
    array(
        "id" => 18,
        "skill" => "165",
        "level" => 8,
        "category" => "navy",
        "name" => "Master Chief Petty Officer (MCPO)",
        "cover" => "img/ranks/navy-8.png"
    ),
    array(
        "id" => 19,
        "skill" => "195",
        "level" => 9,
        "category" => "navy",
        "name" => "Lieutenant (LT)",
        "cover" => "img/ranks/navy-9.png"
    ),
    array(
        "id" => 20,
        "skill" => "235",
        "level" => 10,
        "category" => "navy",
        "name" => "Lieutenant Commander (LCDR)",
        "cover" => "img/ranks/navy-10.png"
    ),

    // AIR FORCE
    array(
        "id" => 21,
        "skill" => "Starting Stage",
        "level" => 1,
        "category" => "air_force",
        "name" => "Airman (Amn)",
        "cover" => "img/ranks/air-1.png"
    ),
    array(
        "id" => 22,
        "skill" => "20",
        "level" => 2,
        "category" => "air_force",
        "name" => "Airman First Class (A1C)",
        "cover" => "img/ranks/air-2.png"
    ),
    array(
        "id" => 23,
        "skill" => "40",
        "level" => 3,
        "category" => "air_force",
        "name" => "Senior Airman (SrA)",
        "cover" => "img/ranks/air-3.png"
    ),
    array(
        "id" => 24,
        "skill" => "60",
        "level" => 4,
        "category" => "air_force",
        "name" => "First Seargeant",
        "cover" => "img/ranks/air-4.png"
    ),
    array(
        "id" => 25,
        "skill" => "85",
        "level" => 5,
        "category" => "air_force",
        "name" => "Chief Master Seargeant (CMSgt)",
        "cover" => "img/ranks/air-5.png"
    ),
    array(
        "id" => 26,
        "skill" => "110",
        "level" => 6,
        "category" => "air_force",
        "name" => "Second Lieutenant (2d Lt)",
        "cover" => "img/ranks/air-6.png"
    ),
    array(
        "id" => 27,
        "skill" => "135",
        "level" => 7,
        "category" => "air_force",
        "name" => "First Lieutenant (1st Lt)",
        "cover" => "img/ranks/air-7.png"
    ),
    array(
        "id" => 28,
        "skill" => "165",
        "level" => 8,
        "category" => "air_force",
        "name" => "Captain (Capt)",
        "cover" => "img/ranks/air-8.png"
    ),
    array(
        "id" => 29,
        "skill" => "195",
        "level" => 9,
        "category" => "air_force",
        "name" => "Major (Maj)",
        "cover" => "img/ranks/air-9.png"
    ),
    array(
        "id" => 30,
        "skill" => "235",
        "level" => 10,
        "category" => "air_force",
        "name" => "Lieutenant Colonel (Lt Col)",
        "cover" => "img/ranks/air-10.png"
    ),

    // MARINE CORPS
    array(
        "id" => 31,
        "skill" => "Starting Stage",
        "level" => 1,
        "category" => "marine_corps",
        "name" => "Private First Class (PFC)",
        "cover" => "img/ranks/marine-1.png"
    ),
    array(
        "id" => 32,
        "skill" => "20",
        "level" => 2,
        "category" => "marine_corps",
        "name" => "Lance Corporal (LCpl)",
        "cover" => "img/ranks/marine-2.png"
    ),
    array(
        "id" => 33,
        "skill" => "40",
        "level" => 3,
        "category" => "marine_corps",
        "name" => "Corporal (Cpl)",
        "cover" => "img/ranks/marine-3.png"
    ),
    array(
        "id" => 34,
        "skill" => "60",
        "level" => 4,
        "category" => "marine_corps",
        "name" => "Seargeant (Sgt)",
        "cover" => "img/ranks/marine-4.png"
    ),
    array(
        "id" => 35,
        "skill" => "85",
        "level" => 5,
        "category" => "marine_corps",
        "name" => "First Seargeant",
        "cover" => "img/ranks/marine-5.png"
    ),
    array(
        "id" => 36,
        "skill" => "110",
        "level" => 6,
        "category" => "marine_corps",
        "name" => "Seargeant Major (SgtMaj)",
        "cover" => "img/ranks/marine-6.png"
    ),
    array(
        "id" => 37,
        "skill" => "135",
        "level" => 7,
        "category" => "marine_corps",
        "name" => "Second Lieutenant (2ndLt)",
        "cover" => "img/ranks/marine-7.png"
    ),
    array(
        "id" => 38,
        "skill" => "165",
        "level" => 8,
        "category" => "marine_corps",
        "name" => "First Lieutenant (1stLt)",
        "cover" => "img/ranks/marine-8.png"
    ),
    array(
        "id" => 39,
        "skill" => "195",
        "level" => 9,
        "category" => "marine_corps",
        "name" => "Captain (Capt)",
        "cover" => "img/ranks/marine-9.png"
    ),
    array(
        "id" => 40,
        "skill" => "235",
        "level" => 10,
        "category" => "marine_corps",
        "name" => "Major (Maj)",
        "cover" => "img/ranks/marine-10.png"
    ),

];

// PLAYER RANKINGS
$P_RANKINGS = [

    array(
        "id" => 1,
        "name" => "Private",
        "level" => 1,
        "cover" => "img/ranks/profile-1.png",
        "description" => "Your Profile ranking is a summary of your activity in the marketplace and game, each rank provides more access and features that empower your role as a commander of the Warzone, to read more about each rank perks please refer to the Rank details Section on each Rank.",
        "startingRank" => "All New Players",
        "startingRankNote" => "Ranks in the game are affected by the activity of the player.",
        "specs" => ["lvl.1 Private", "7500 max points", "5 NFT purchase limit", "1 Infinity Knox"]
    ),
    array(
        "id" => 2,
        "name" => "Corporal",
        "level" => 2,
        "cover" => "img/ranks/profile-2.png",
        "description" => "Your Profile ranking is a summary of your activity in the marketplace and game, each rank provides more access and features that empower your role as a commander of the Warzone, to read more about each rank perks please refer to the Rank details Section on each Rank.",
        "startingRank" => "7500 Points",
        "startingRankNote" => "Ranks in the game are affected by the activity of the player.",
        "specs" => ["lvl.2 Corporal", "20000 max points", "Around 50 games played", "15 NFT purchase limit", "2 Infinity Knox"]
    ),
    array(
        "id" => 3,
        "name" => "Seargeant",
        "level" => 3,
        "cover" => "img/ranks/profile-3.png",
        "description" => "Your Profile ranking is a summary of your activity in the marketplace and game, each rank provides more access and features that empower your role as a commander of the Warzone, to read more about each rank perks please refer to the Rank details Section on each Rank.",
        "startingRank" => "20000 Points",
        "startingRankNote" => "Ranks in the game are affected by the activity of the player.",
        "specs" => ["lvl.3 Seargeant", "35000 max points", "Around 125 games played", "25 NFT purchase limit", "5 Infinity Knox"]
    ),
    array(
        "id" => 4,
        "name" => "Seargeant Major",
        "level" => 4,
        "cover" => "img/ranks/profile-4.png",
        "description" => "Your Profile ranking is a summary of your activity in the marketplace and game, each rank provides more access and features that empower your role as a commander of the Warzone, to read more about each rank perks please refer to the Rank details Section on each Rank.",
        "startingRank" => "35000 Points",
        "startingRankNote" => "Ranks in the game are affected by the activity of the player.",
        "specs" => ["lvl.4 Seargeant Major", "55000 max points", "Around 250 games played", "25 NFT purchase limit", "No limit on Knox"]
    ),
    array(
        "id" => 5,
        "name" => "Second Lieutenant",
        "level" => 5,
        "cover" => "img/ranks/profile-5.png",
        "description" => "Your Profile ranking is a summary of your activity in the marketplace and game, each rank provides more access and features that empower your role as a commander of the Warzone, to read more about each rank perks please refer to the Rank details Section on each Rank.",
        "startingRank" => "55000 Points",
        "startingRankNote" => "Ranks in the game are affected by the activity of the player.",
        "specs" => ["lvl.5 Second Lieutenant", "80000 max points", "Around 400 games played", "35 NFT purchase limit", "No limit on Knox"]
    ),
    array(
        "id" => 6,
        "name" => "First Lieutenant",
        "level" => 6,
        "cover" => "img/ranks/profile-6.png",
        "description" => "Your Profile ranking is a summary of your activity in the marketplace and game, each rank provides more access and features that empower your role as a commander of the Warzone, to read more about each rank perks please refer to the Rank details Section on each Rank.",
        "startingRank" => "80000 Points",
        "startingRankNote" => "Ranks in the game are affected by the activity of the player.",
        "specs" => ["lvl.6 First Lieutenant", "100000 max points", "Around 625 games played", "50 NFT purchase limit", "No limit on Knox"]
    ),
    array(
        "id" => 7,
        "name" => "Captain",
        "level" => 7,
        "cover" => "img/ranks/profile-7.png",
        "description" => "Your Profile ranking is a summary of your activity in the marketplace and game, each rank provides more access and features that empower your role as a commander of the Warzone, to read more about each rank perks please refer to the Rank details Section on each Rank.",
        "startingRank" => "100000 Points",
        "startingRankNote" => "Ranks in the game are affected by the activity of the player.",
        "specs" => ["lvl.7 Captain", "150000 max points", "Around 765 games played", "NFT no purchase limit", "No limit on Knox", "Special Access"]
    ),
    array(
        "id" => 8,
        "name" => "Major",
        "level" => 8,
        "cover" => "img/ranks/profile-8.png",
        "description" => "Your Profile ranking is a summary of your activity in the marketplace and game, each rank provides more access and features that empower your role as a commander of the Warzone, to read more about each rank perks please refer to the Rank details Section on each Rank.",
        "startingRank" => "150000 Points",
        "startingRankNote" => "Ranks in the game are affected by the activity of the player.",
        "specs" => ["lvl.8 Major", "300000 max points", "Around 1000 games played", "NFT no purchase limit", "No limit on Knox", "Special Access", "Limited Artifact Access", "Golden Key Events", "Star Badge on Profile"]
    ),
    array(
        "id" => 9,
        "name" => "Major General",
        "level" => 9,
        "cover" => "img/ranks/profile-9.png",
        "description" => "Your Profile ranking is a summary of your activity in the marketplace and game, each rank provides more access and features that empower your role as a commander of the Warzone, to read more about each rank perks please refer to the Rank details Section on each Rank.",
        "startingRank" => "300000 Points",
        "startingRankNote" => "Ranks in the game are affected by the activity of the player.",
        "specs" => ["lvl.9 Major General", "500000 max points", "Around 2000 games played", "NFT no purchase limit", "No limit on Knox", "Special Access", "Limited Artifact Access", "Free Gifts", "Golden Key Events", "Special Club Access", "Star Badge on Profile"]
    ),
    array(
        "id" => 10,
        "name" => "General of the Army",
        "level" => 10,
        "cover" => "img/ranks/profile-10.png",
        "description" => "Your Profile ranking is a summary of your activity in the marketplace and game, each rank provides more access and features that empower your role as a commander of the Warzone, to read more about each rank perks please refer to the Rank details Section on each Rank.",
        "startingRank" => "500000 Points",
        "startingRankNote" => "Ranks in the game are affected by the activity of the player.",
        "specs" => ["lvl.10 General of the Army", "Around 4000 games played", "NFT no purchase limit", "No limit on Knox", "Special Access", "Limited Artifact Access", "Game Development", "Golden Key Events", "Special Club Access", "Team Core Access", "Star Badge on Profile", "Telegram Title"]
    ),

]

?>

<script>
    const FORTIS = <?php echo json_encode($FORTIS); ?>;
    const ALIENS = <?php echo json_encode($ALIENS); ?>;
    const WEAPONS = <?php echo json_encode($WEAPONS); ?>;
    const I_KNOX = <?php echo json_encode($I_KNOX); ?>;
    const P_KNOX = <?php echo json_encode($P_KNOX); ?>;
    const RANKINGS = <?php echo json_encode($RANKINGS); ?>;
    const P_RANKINGS = <?php echo json_encode($P_RANKINGS); ?>;
</script>