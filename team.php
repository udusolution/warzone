<?php include '_members.php'; ?>

<!doctype html>
<html class="no-js" lang="">
    <head>

        <!-- Head -->
        <?php include '_head.php'; ?>

    </head>
    <body>

        <!-- Preloader -->
        <?php include '_preloader.php'; ?>

        <!-- Header -->
        <?php include '_header.php'; ?>

        <!-- main-area -->
        <main>

            <!-- Announcements -->
            <?php include '_announcements.php'; ?>

            <!-- breadcrumb-area -->
            <section class="breadcrumb-area breadcrumb-bg team-breadcrumbs">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb-content text-center">
                                <h2>TEAM</h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Team</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- breadcrumb-area-end -->

            <!-- inner-about-area -->
            <section class="inner-about-area fix">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-7 col-lg-6 order-0 order-lg-2">
                            <div class="inner-about-img">
                                <img src="img/images/studio1.jpg" class="wow fadeInRight" data-wow-delay=".3s" alt="">
                                <img src="img/images/studio2_new.jpg" class="wow fadeInLeft" data-wow-delay=".6s" alt="">
                                <img src="img/images/studio3.jpg" class="wow fadeInUp" data-wow-delay=".6s" alt="">
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-6">
                            <div class="section-title title-style-three mb-25">
                                <h2>Gaming <span>Studios</span></h2>
                            </div>
                            <div class="inner-about-content">
                                <h5>BFK WARZONE DESIGN & DEVELOPMENT</h5>
                                <p>TGC International Studios has Developed BFK WARZONE with a dedicated in-house team of 6+ developers who have tremendous experience developing games on UNITY 3D & Blockchain ecosystem.</p>
                                <p>BFK Warzone is managed by TGC Studios Licensed in the United Arab emirates expanding internationally.</p>
                                <a href="mailto:gaming@tgcinternational.net" class="btn btn-style-two">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="inner-about-shape"><img src="img/images/medale_shape.png" alt=""></div>
            </section>
            <!-- inner-about-area-end -->

            <!-- Logos -->
            <div class="team-logos">
                <div class="container">
                    <div class="team-logos-carousel owl-carousel owl-theme">
            
                        <div class="item clickable" onclick="openInNewTab('<?php echo $TGC_LINK;?>')">
                            <img loading="lazy" src="img/brand/tgc-international.png" alt="" <?php echo imgSize('img/brand/tgc-international.png'); ?>>
                        </div>
                        <div class="item clickable" onclick="openInNewTab('<?php echo $BFK_MAIN_LINK;?>')">
                            <img loading="lazy" src="img/logo/logo_secondary.png" alt="" <?php echo imgSize('img/logo/logo_secondary.png'); ?>>
                        </div>
                        <div class="item clickable" onclick="openAlertPopup(`<img class='copyright-img' src='img/copyright/copyright.png' alt='' height='100%' width='100%'>`)">
                            <img loading="lazy" src="img/logo/ministry.png" alt="" <?php echo imgSize('img/logo/ministry.png'); ?>>
                        </div>
                        <!-- <div class="item clickable" onclick="openInNewTab('<?php echo $BFK_NARSUN_LINK;?>')">
                            <img loading="lazy" src="img/logo/narsun_studios.png" alt="" <?php echo imgSize('img/logo/narsun_studios.png'); ?> style="height: 70px; object-fit: contain; object-position: center center;">
                        </div> -->

                    </div>
                </div>
            </div>

            <!-- team-member -->
            <section class="team-member-area pt-115 pb-125">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-6 col-lg-8">
                            <div class="section-title title-style-three text-center mb-70">
                                <h2>Our team <span>members</span></h2>
                                <p>Our Dedicated Core Team and our Task Force Making the impossible Possible</p>
                            </div>
                        </div>
                    </div>

                    <div class="row product-active">

                        <?php foreach($TEAM as $key => $val) { ?>
                            <div class="col-xl-3">
                                <div class="team-member-box text-center mb-50">
                                    <div class="team-member-thumb clickable" onclick="showMember(<?php echo $val['id']; ?>)">
                                        <img src="<?php echo $val['avatar']; ?>" alt="" class="team-member-avatar">
                                        <div class="team-member-social">
                                            <ul>
                                                <?php if(isset($val['socials']['facebook'])) { ?>
                                                    <li><a href="<?php echo $val['socials']['facebook']; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                <?php } ?>
                                                <?php if(isset($val['socials']['twitter'])) { ?>
                                                    <li><a href="<?php echo $val['socials']['twitter']; ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                <?php } ?>
                                                <?php if(isset($val['socials']['pinterest'])) { ?>
                                                    <li><a href="<?php echo $val['socials']['pinterest']; ?>" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
                                                <?php } ?>
                                                <?php if(isset($val['socials']['linkedin'])) { ?>
                                                    <li><a href="<?php echo $val['socials']['linkedin']; ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="team-member-content clickable" onclick="showMember(<?php echo $val['id']; ?>)">
                                        <h4><a><?php echo $val['name']; ?></a></h4>
                                        <span><?php echo $val['position']; ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                    </div>

                    <!-- Member Details -->
                    <div class="team-member-details" id="teamMemberDetails">
                        <img class="team-member-details__avatar team-member-avatar" src="img/team/team_member03.jpg" alt="" id="memberAvatar" />
                        <div class="team-member-details__info">
                            <div class="team-member-details__name" id="memberName">Emily Doe</div>
                            <div class="team-member-details__position" id="memberPosition">Position</div>
                            <div class="team-member-details__description" id="memberDescription">
                                <p>Magna aute eiusmod enim cupidatat dolore enim irure in reprehenderit id. Do sit eiusmod dolor commodo commodo reprehenderit. Labore ipsum do cupidatat ullamco voluptate sunt minim do Lorem consectetur excepteur sunt.</p>
                                <p>Amet eiusmod mollit in tempor et laborum irure sit ex nostrud minim irure in. Sint id Lorem cupidatat minim duis voluptate pariatur quis voluptate quis dolore. Ea ipsum voluptate non exercitation pariatur cillum dolor sit magna. Eiusmod quis sit tempor fugiat adipisicing magna mollit.</p>
                            </div>
                            <div class="team-member-details__socials-wrapper">
                                <span>Social networks: </span>
                                <ul id="memberSocials" class="team-member-details__socials"></ul>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- team-member-end -->

        </main>
        <!-- main-area-end -->

        <!-- Footer -->
        <?php include '_footer.php'; ?>

        <!-- Scripts -->
        <?php include '_scripts.php'; ?>

        <script>

            const memberAvatar = document.querySelector('#memberAvatar');
            const memberName = document.querySelector('#memberName');
            const memberPosition = document.querySelector('#memberPosition');
            const memberDescription = document.querySelector('#memberDescription');
            const memberSocials = document.querySelector('#memberSocials');
            
            function showMember(id, scroll = true) {
                const member = TEAM.filter(t => t.id == id)[0];

                memberAvatar.src = member.avatar;
                memberName.innerHTML = member.name;
                memberPosition.innerHTML = member.position;
                memberDescription.innerHTML = member.description;

                memberSocials.innerHTML = '';
                if(member.socials['facebook']) memberSocials.innerHTML += `<li><a href="${member.socials['facebook']}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>`;
                if(member.socials['twitter']) memberSocials.innerHTML += `<li><a href="${member.socials['twitter']}" target="_blank"><i class="fab fa-twitter"></i></a></li>`;
                if(member.socials['pinterest']) memberSocials.innerHTML += `<li><a href="${member.socials['pinterest']}" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>`;
                if(member.socials['linkedin']) memberSocials.innerHTML += `<li><a href="${member.socials['linkedin']}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>`;

                if(scroll) {
                    $('html, body').animate({
                        scrollTop: $("#teamMemberDetails").offset().top - (window.innerHeight / 2)
                    }, 500);
                }
            }

            showMember(1, false);

            // Listen to autoplay to show active member
            $('.product-active').on('beforeChange', function(event, slick, currentSlide, nextSlide){
                showMember(nextSlide + 1, false);
            });

            // If member details is mostly in port, pause the slick slider
            function testInView($el){
                const windowTop = $(window).scrollTop();
                const windowBottom = windowTop + window.innerHeight;
                const elTop = $el.offset().top;
                const elBottom = elTop + $el.height();

                // If element's center is above the window bottom
                return ((elTop + elBottom) / 2) < windowBottom;
            }
            function setInView(){
                $("#teamMemberDetails").each(function(){
                    if(testInView($(this))){
                        $('.product-active').slick('slickPause');
                    }
                    else {
                        $('.product-active').slick('slickPlay');
                    }
                });
            }
            $(document).scroll(function(){
                setInView();
            });
            $(document).resize(function(){
                setInView();
            });
            $(document).ready(function(){
                setInView();
            });

        </script>
        
    </body>
</html>
