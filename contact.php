<!doctype html>
<html class="no-js" lang="">
    <head>
        
        <!-- HEAD -->
        <?php include '_head.php'; ?>

        <!-- ReCAPTCHA -->
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    </head>
    <body>

        <!-- Preloader -->
        <?php include '_preloader.php'; ?>

        <!-- HEADER -->
        <?php include '_header.php'; ?>

        <!-- main-area -->
        <main>

            <!-- Announcements -->
            <?php include '_announcements.php'; ?>

            <!-- breadcrumb-area -->
            <section class="breadcrumb-area breadcrumb-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="breadcrumb-content text-center">
                                <h2>contact <span>info</span></h2>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.php">Warzone</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">contact</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- breadcrumb-area-end -->

            <!-- contact-area -->
            <section class="contact-area pt-120 pb-120">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">

                            <!-- Contact Map Section -->
                            <section class="contact-map-section">
                                <div class="map-outer">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14368.32439828512!2d55.9683029!3d25.8008984!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd3b5ffc06b24ab09!2sRas%20Al%20Khaimah%20Economic%20Zone%20-%20RAKEZ!5e0!3m2!1sen!2sae!4v1639041635232!5m2!1sen!2sae" height="450" style="border:0; width:100%;"></iframe>
                                </div>
                            </section>
                            <!-- End Contact Map Section -->

                        </div>
                        <div class="col-lg-6 pl-45">
                            <div class="section-title title-style-three mb-20">
                                <h2>CONTACT US ABOUT <span>BFK WARZONE</span></h2>
                            </div>
                            <div class="contact-info-list mb-40">
                                <ul>
                                    <li><i class="fas fa-map-marker-alt"></i><?php echo $ADDRESS; ?></li>
                                    <li><i class="fas fa-envelope"></i><a href="mailto:<?php echo $EMAIL; ?>"><?php echo $EMAIL; ?></a></li>
                                    <li><i class="fas fa-phone"></i><a href="tel:<?php echo $PHONE; ?>"><?php echo $PHONE; ?></a></li>
                                    <li><i class="fas fa-building"></i><span>TGC International FZ-LLC Studios</li>
                                </ul>
                            </div>
                            <div class="contact-form">
                                <form id="contactForm">
                                    <textarea name="message" id="message" placeholder="Write Message..." required></textarea>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" name="name" placeholder="Your Name" required>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="email" name="email" placeholder="Your Mail" required>
                                        </div>
                                    </div>
                                    <div class="g-recaptcha" data-sitekey="6LcIxXweAAAAALNeburC_5gaKfY6Z1HgkUD8io7v"></div>
                                    <button type="submit" id="submitBtn">SUBMIT MESSAGE</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- contact-area-end -->


            <!-- brand-area -->
            <div class="brand-area lite-gray-bg pb-170 pt-120">
                <div class="container">
                    <?php include '_logos.php'; ?>
                </div>
            </div>
            <!-- brand-area-end -->



        </main>
        <!-- main-area-end -->

        <!-- FOOTER -->
        <?php include '_footer.php'; ?>

		<!-- SCRIPTS -->
        <?php include '_scripts.php'; ?>

        <script>
            function basicmap() {
                    // Basic options for a simple Google Map
                    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                    var mapOptions = {
                        // How zoomed in you want the map to start at (always required)
                        zoom: 11,
                        scrollwheel: false,
                        // The latitude and longitude to center the map (always required)
                        center: new google.maps.LatLng(40.6700, -73.9400), // New York
                        // This is where you would paste any style found on Snazzy Maps.
                        styles: [{ "featureType": "all", "elementType": "geometry.fill", "stylers": [{ "weight": "2.00" }] }, { "featureType": "all", "elementType": "geometry.stroke", "stylers": [{ "color": "#9c9c9c" }] }, { "featureType": "all", "elementType": "labels.text", "stylers": [{ "visibility": "on" }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#f2f2f2" }] }, { "featureType": "landscape", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }] }, { "featureType": "landscape.man_made", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "saturation": -100 }, { "lightness": 45 }] }, { "featureType": "road", "elementType": "geometry.fill", "stylers": [{ "color": "#eeeeee" }] }, { "featureType": "road", "elementType": "labels.text.fill", "stylers": [{ "color": "#7b7b7b" }] }, { "featureType": "road", "elementType": "labels.text.stroke", "stylers": [{ "color": "#ffffff" }] }, { "featureType": "road.highway", "elementType": "all", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "color": "#46bcec" }, { "visibility": "on" }] }, { "featureType": "water", "elementType": "geometry.fill", "stylers": [{ "color": "#c8d7d4" }] }, { "featureType": "water", "elementType": "labels.text.fill", "stylers": [{ "color": "#070707" }] }, { "featureType": "water", "elementType": "labels.text.stroke", "stylers": [{ "color": "#ffffff" }] }]
                    };
                    // Get the HTML DOM element that will contain your map
                    // We are using a div with id="map" seen below in the <body>
                    var mapElement = document.getElementById('contact-map');

                    // Create the Google Map using our element and options defined above
                    var map = new google.maps.Map(mapElement, mapOptions);

                    // Let's also add a marker while we're at it
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(40.6700, -73.9400),
                        map: map,
                        icon: 'img/icon/map_marker.png',
                        title: 'Geco'
                    });
                }
                if ($('#contact-map').length != 0) {
                    google.maps.event.addDomListener(window, 'load', basicmap);
                }
        </script>

        <script>
            /**
             * Contact Form
             */

            const contactForm = document.querySelector('#contactForm');
            const name = contactForm.querySelector('input[name="name"]');
            const email = contactForm.querySelector('input[name="email"]');
            const message = contactForm.querySelector('textarea[name="message"]');
            const submitBtn = contactForm.querySelector('#submitBtn');

            contactForm.addEventListener('submit', function(ev) {

                // Prevent Default
                ev.preventDefault();

                // Data
                const data = {
                    name: name.value,
                    email: email.value,
                    message: message.value,
                    recaptcha: grecaptcha.getResponse()
                }

                // Validate
                console.log(data);
                if(grecaptcha.getResponse() == "") {
                    submitBtn.innerHTML = 'Invalid Captcha';
                    return;
                }

                // Disable Send Button
                submitBtn.disabled = 'disabled';

                // Send
                $.ajax({
                    url: 'sendContactEmail.php',
                    async: true,
                    cache: false,
                    type: 'POST',
                    dataType: "json",
                    data: {
                        data: JSON.stringify(data)
                    },
                    success: function(response) {
                        if(response.status == 'success') {
                            console.log(response);
                            resetContactForm();
                            submitBtn.innerHTML = 'Thank you!';
                        }
                        else if(response.status == 'error') {
                            console.error(response);
                            submitBtn.disabled = false;
                            submitBtn.innerHTML = 'Something went wrong!';
                        }
                        else {
                            console.info(response);
                            submitBtn.disabled = false;
                        }
                    },
                    error: function(err) {
                        console.error(err);
                        submitBtn.disabled = false;
                        submitBtn.innerHTML = 'Something went wrong!!';
                    }
                })

                function resetContactForm() {
                    name.value = '';
                    email.value = '';
                    message.value = '';
                }

            })

        </script>

    </body>
</html>
