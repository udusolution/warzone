<?php

    include 'env.php';

    $error = '';
    $success = '';

    $title = '';
    $description = '';
    $date = date("Y-m-d");
    $version = '';

    $alertMsg = '';
    $pw = '';
    if(isset($_GET['pw'])) $pw = $_GET['pw'];
    if(isset($_GET['msg'])) {
        $alertMsg = $_GET['msg'];
        $error = $alertMsg;
    }

    // Create connection
    $conn = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Get Changelogs
    // requires mysqlind, do not use
    // $logs = $conn->query("SELECT * FROM changelog ORDER BY id DESC")->fetch_all(MYSQLI_ASSOC);

    $res3 = $conn->query("SELECT * FROM changelog ORDER BY id DESC");
    $logs = [];
    while ($row3 = $res3->fetch_assoc()) {
        array_push($logs, $row3);
    }

    // Get form values
    if(isset($_POST['title'])) {

        // Upload Vars
        $targetDir = "uploads/";
        $total = count($_FILES['upload']['name']);
        $uploadedFiles = [];

        for( $i=0 ; $i < $total ; $i++ ) {

            //Get the temp file path
            $tmpFilePath = $_FILES['upload']['tmp_name'][$i];
          
            //Make sure we have a file path
            if ($tmpFilePath != ""){
              //Setup our new file path
              $newFilePath = $targetDir . uniqid() . '-' . $_FILES['upload']['name'][$i];
          
              //Upload the file into the temp dir
              if(move_uploaded_file($tmpFilePath, $newFilePath)) {
          
                //Handle other code here
                array_push($uploadedFiles, $newFilePath);
              }
            }
        }

        // Form Data
        $title = $_POST['title'];
        $description = $_POST['description'];
        $date = $_POST['date'];
        $version = $_POST['version'];
        $usrPassword = $_POST['password'];

        if($usrPassword != 'BFK!99') {
            $error = 'Incorrect password';
        }
        else {

            $sql = "INSERT INTO changelog (`title`, `description`, `date`, `version`) VALUES (?,?,?,?)";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param("ssss", $title, $description, $date, $version);
    
            try {
                $stmt->execute();
    
                $changelogId = mysqli_insert_id($conn);
    
                foreach($uploadedFiles as $file) {
                    $conn->query("INSERT INTO changelog_image (`changelogId`, `file`) VALUES (". $changelogId .", '". $file ."')");
                }
            }
            catch(Error $e) {
                echo "Issue inserting into database";
            }

            $success = 'Successfully added changelog!';
        }

        
    }
    
    $conn->close();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Battle Fort Knox | BFK Warzone | NFT Marketplace</title>

    <script src="https://cdn.ckeditor.com/ckeditor5/29.2.0/classic/ckeditor.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/changelog-admin.css">

    <!-- Media Note Style -->
    <style>
        .media-note {
            padding: 5px 10px;
            background: #ffd37eab;
            color: black;
            font-size: 12px;
            display: block;
            border-radius: 4px;
            margin-bottom: 10px;
        }
    </style>

</head>
<body>
    
    <div class="form-container">

        <h1>BFK Warzone Changelog Admin Page</h1>

        <!-- Password -->
        <div class="section password-section">
            <!-- Error & Success Messages -->
            <?php if(isset($error) && $error != "") { ?>
                <div class="alert alert-danger" role="alert">
                    <i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
                </div>
            <?php } ?>
            <?php if(isset($success) && $success != "") { ?>
                <div class="alert alert-success" role="alert">
                    <i class="fa fa-check-circle"></i> <?php echo $success; ?>
                </div>
            <?php } ?>

            <h2><i class="fa fa-lock"></i> Password</h2>
            <input type="text" class="form-control" placeholder="Enter password" id="pagePassword" <?php if(isset($pw)) echo 'value="'.$pw.'"'; ?> />
        </div>

        <!-- Table -->
        <div class="section">

            <h2>Changelogs</h2>

            <div class="table-responsive">
                <table class="table table-striped changelogs-table">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Version</th>
                            <th>Date</th>
                            <!-- <th>Description</th> -->
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($logs)) { foreach($logs as $log) { ?>
                            <tr>
                                <td><?php echo $log['title']; ?></td>
                                <td><?php echo $log['version']; ?></td>
                                <td><?php echo $log['date']; ?></td>
                                <!-- <td class="ellipsis"><?php echo $log['description']; ?></td> -->
                                <td class="actions-row">
                                    <a class="btn btn-primary btn-sm locked" href="editChangelog.php?editId=<?php echo $log['id']; ?>">Edit</a>
                                    <?php if($log['isDeleted'] == 1) { ?>
                                        <form method="POST" action="operations/restoreChangelog.php" class="actionForm">
                                            <input type="hidden" name="password" class="passwordField" />
                                            <input type="hidden" name="id" value="<?php echo $log['id']; ?>" />
                                            <button class="btn btn-success btn-sm locked">Restore</button>
                                        </form>
                                    <?php } else { ?>
                                        <form method="POST" action="operations/deleteChangelog.php" class="actionForm">
                                            <input type="hidden" name="password" class="passwordField" />
                                            <input type="hidden" name="id" value="<?php echo $log['id']; ?>" />
                                            <button class="btn btn-danger btn-sm locked">Delete</button>
                                        </form>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>

        </div>

        <!-- Form -->
        <form class="add-changelog section" method="POST" action="changelog-admin.php" enctype="multipart/form-data" id="theForm">
    
            <h2>Add Changelog Entry</h2>
    
            <!-- Title -->
            <div class="form-group">
                <label>* Title</label>
                <input class="form-control" placeholder="Enter title" name="title" required min="3" max="30" value="<?php echo $title; ?>" />
                <span class="form-error"></span>
            </div>

            <!-- Version -->
            <div class="form-group">
                <label>* Version</label>
                <input class="form-control"  placeholder="Enter version" type="text" name="version" required value="<?php echo $version; ?>" />
                <span class="form-error"></span>
            </div>

            <!-- Date -->
            <div class="form-group">
                <label>* Date</label>
                <input class="form-control" type="date" name="date" required value="<?php echo $date; ?>" />
                <span class="form-error"></span>
            </div>

            <!-- Description -->
            <div class="form-group">
                <label>* Description</label>
                <textarea id="editor" name="description" placeholder="Enter description"><?php echo $description; ?></textarea>
                <span class="form-error"></span>
            </div>

            <!-- Images -->
            <div class="form-group">
                <label>Images, audio, and/or videos (optional)</label>
                <span class="media-note">Avoid large file sizes. Make sure media is properly compressed before uploading. Recommended formats for video are "webm" and "mp4". Recommended formats for audio are "weba" and "mp3".</span>
                <input class="form-control" type="file" name="upload[]" accept="image/*,audio/*,video/mp4,video/webm" multiple="multiple" />
                <span class="form-error"></span>
            </div>

            <input type="hidden" name="password" class="passwordField" />
    
            <!-- Submit -->
            <button type="submit" class="btn btn-primary" id="saveBtn">Add</button>

        </form>

    </div>

    <!-- Loader -->
    <div class="loader-wrapper">
        <div class="loader">
            <i class="fa fa-spinner loader-icon"></i>
        </div>
    </div>

</body>
</html>

<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }

    // Password
    const pagePassword = document.querySelector('#pagePassword');

    // document.querySelectorAll('.locked').forEach(locked => {
    //     locked.disabled = 'disabled';
    // })

    document.querySelectorAll('.passwordField').forEach(field => {
        field.value = pagePassword.value;
    })

    pagePassword.addEventListener('change', function(ev) {
        const enteredPassword = ev.target.value;
        const isCorrect = (enteredPassword == 'BFK!99');

        document.querySelectorAll('.passwordField').forEach(field => {
            field.value = enteredPassword;
        })
    })

    // FUNCTIONS
    function deleteChangelog(id) {

        const requestBody = {
            id: id,
            password: pagePassword.value
        }

        fetch('operations/deleteChangelog.php', { 
            method: 'POST',
            body: JSON.stringify(requestBody),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => {
                console.log(res);
                res.text().then(console.log);
            })
            .catch(er => {
                alert(er);
            });
    }

    // On form submit, disable the button and show a loader
    const theForm = document.querySelector('#theForm');
    const saveBtn = document.querySelector('#saveBtn');
    const loader = document.querySelector('.loader-wrapper');
    loader.classList.remove('loader-wrapper--show');
    theForm.addEventListener('submit', function() {
        saveBtn.disabled = 'disabled';
        loader.classList.add('loader-wrapper--show');
    })

    // On Actions Clicked, disable the buttons
    const actionForms = document.querySelectorAll('.actionForm');
    const actionFormBtns = document.querySelectorAll('.locked');
    actionForms.forEach(actionForm => {
        actionForm.addEventListener('submit', function() {
            actionFormBtns.forEach(actionFromBtn => {
                actionFromBtn.disabled = 'disabled';
            })
        })
    })

</script>

<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ), {
            removePlugins: ['EasyImage', 'ImageCaption', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed', 'Table']
        })
        .catch( error => {
            console.error( error );
        } );
</script>
