<?php

$FAQ = [
    ["Overview", "Battle Fort Knox is now rebranded as a gaming focused ecosystem. We have heard our community loud and clear and we are proud to deliver a purely gaming specific brand. We have shed the features that have been slowing us down, and we are doubling down on the features that made us great. No more tokenomics that hurt the chart, and no more identity crisis! We are Battle Fort Knox and we are presenting BFK Warzone, the ultimate P2E 2D shooter that user's can play and earn \$BFK. Load up soldier, the war rages on..."],
    ["Past and Present", "\$BFK was once Baby Fort Knox, but now the baby is fully grown, and known as Battle Fort Knox. We have finished our teething stage and we know which shoes fit us best. We have shed the tokenomics that were keeping investors away, and focusing on what we do best. The BFK team is now strictly focused on presenting a gaming ecosystem in which users can easily trade, buy, sell, play, and upgrade NFTs under simple conditions. We have also heard the community and will be delivering some prizes to a certain special group of users who hold a limited edition BFK NFT."],
    ["Plan", "BFK believes decentralized NFT use case scenarios will be the future of the NFT industry. Our mission, as always, is to push the limits of use case NFTs and design a marketplace in which the users have total control in earning cryptocurrency in an entertaining manner. Due to the economical gas fees, BSC tokens can be the groundbreaking space for the NFT industry, and NFTs can be so much more than JPEGs; BFK aims to prove this."],
    ["Audit", "We are proud to be onboarded with Certik for an audit, and for their Skynet active monitoring to keep the BFK Warzone ecosystem safe. You can find this <a href='https://www.certik.org/projects/bfkwarzone' target='_blank'>here</a>"],
    ["What are the new tokenomics?", "We are creating simple and focused tokenomics. Upon relaunch, we will have five percent on buys and sells alike and this will remain static throughout the project. No more complex features where you need an entire dashboard to keep track. Simple and focused is our strategy, you can find the breakdown below.<br>1.5% Game Development<br>1.5% Marketing<br>1% LP<br>1% Game Prizes"],
    ["What are your marketing plans?", "We have a very experienced marketing crew with extensive contacts throughout the space. As we have proven already, we have been very quick to provide maximum exposure and effective marketing for the token as well as the ecosystem. We will be applying the same foundational approach, along with gaming specific outreach for our relaunch marketing strategy. We will focus on the gaming ecosystem and infiltrate the associated global markets in which this industry is most popular."],
    ["How can I purchase \$BFK tokens?", "You can purchase \$BFK tokens <a href='https://pancakeswap.finance/swap?outputCurrency=0xed44623b06616bccec876617c124f5461bd5f79b' target='_blank'>here</a>"],
    ["What are your plans for the interactive NFT game?", "<p>\$BFK is a specialized gaming ecosystem on the Binance Smart Chain which will be powered by our native token, and used as the currency for our P2E shooter and NFT Marketplace.</p><p>BFK Warzone will have an interactive marketplace where our NFTs will feature as in-game characters called 'Fortis' that can be used to earn tokens in multiple game modes. These soldiers range from navy seals, paratroopers, snipers, medics, machine gunners, and special forces. You can challenge your gaming buddies to 1v1 or join forces to take down bosses in dungeon style gameplay.</p><p>The more you play and level up your Forti, the stronger and more valuable it becomes. Users can sell their old weapons and gear they are no longer using on our 'black market' for BFK tokens.</p><p>Gear up soldiers, it's time to hold the fort!</p>"]
]

?>

<!doctype html>
<html class="no-js" lang="">
    <head>
        
        <!-- HEAD -->
        <?php include '_head.php'; ?>

        <!-- ReCAPTCHA -->
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

        <style>
            #submitBtn:disabled {
                cursor: not-allowed;
            }
            .form-img {
                width: 100%;
                height: auto;
            }
        </style>

    </head>
    <body>

        <!-- Preloader -->
        <?php include '_preloader.php'; ?>

        <!-- HEADER -->
        <?php include '_header.php'; ?>

        <!-- main-area -->
        <main class="tokenomics-page">

            <!-- Announcement -->
            <section class="countdown-section">
                <div class="countdown-section__title"><span class="primary-color">BFK Warzone</span> is introducing a new improved smart contract v3 for its future global expansion</div>
                <div class="countdown-section__subtitle">More information will be available here soon. More updates on <a href="<?php echo $BFK_TELEGRAM_OFFICIAL_LINK; ?>" target="_blank">Telegram</a></div>
                <!-- <div class="countdown-section__subtitle">Beta R1 will stay Live meanwhile</div> -->
            </section>

            <!-- Hero Section -->
            <section class="slider-area slider-bg">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-9">
                            <div class="slider-content text-center">
                                <h6 class="wow fadeInDown" data-wow-delay=".2s">WARZONE</h6>
                                <h2 class="wow fadeInDown" data-wow-delay=".2s"><span>Battle Fort Knox</span> presents <span>BFK WARZONE</span> P2E 2D Shooter and NFT Marketplace</h2>
                                <p class="wow fadeInUp" data-wow-delay=".2s">$BFK is presenting a simple yet specialized gaming ecosystem in which our native token will be used as the currency for our P2E shooter and our NFT Marketplace. We are designing an interactive marketplace where our NFTs will feature as in-game playable characters that can be used to earn tokens in multiple game modes. Challenge your gaming buddies to 1v1, or join forces to take down bosses in dungeon style gameplay!</p>
                                <!-- <a href="#" class="btn wow fadeInUp" data-wow-delay="2.2s">READ MORE</a> -->

                                <div onclick="copyToClipboard('<?php echo $CONTRACT_ADDRESS; ?>')" class="contract-address" title="click to copy">
                                    <h4>Contract Address V2</h4>
                                    <p><?php echo $CONTRACT_ADDRESS; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Project Highlights Section -->
            <section class="features-area features-bg pt-120 pb-70">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-6 col-lg-8">
                            <div class="section-title title-style-two text-center mb-60">
                                <!-- <span>what we give players</span> -->
                                <h2>V2 Project <span>Highlights</span></h2>
                            </div>
                        </div>
                    </div>
                    <div class="highlights">

                        <div class="features-item mb-30">
                            <div class="features-head mb-35">
                                <img src="img/icon/highlight1.jpg" />
                                <h4>NFT MARKETPLACE</h4>
                                <p>Buy, sell, and upgrade your NFTs</p>
                            </div>
                        </div>


                        <div class="features-item mb-30">
                            <div class="features-head mb-35">
                                <img src="img/icon/percent.png" />
                                <h4>SIMPLE and FOCUSED TOKENOMICS</h4>
                                <p>5% BUY 5% SELL</p>
                            </div>
                        </div>


                        <div class="features-item mb-30">
                            <div class="features-head mb-35">
                                <img src="img/icon/highlight3.jpg" />
                                <h4>"USE CASE" NFTs</h4>
                                <p>NFTs function as playable characters</p>
                            </div>
                        </div>


                        <div class="features-item mb-30">
                            <div class="features-head mb-35">
                                <img src="img/icon/highlight4.png" />
                                <h4>ANTI-BOT</h4>
                                <p>Integrated Test Contract Release and Blacklisting</p>
                            </div>
                        </div>

                        <div class="features-item mb-30">
                            <div class="features-head mb-35">
                                <img src="img/icon/highlight5.png" />
                                <h4>NFT GAME</h4>
                                <p>P2E 2D shooter coming to iOS, Android, and Windows</p>
                            </div>
                        </div>

                    </div>

                    <!-- Song -->
                    <div class="bfk-song-wrapper">
                        <audio src="files/song.mp3" id="bfkSong" preload="none"></audio>
                        <div class="bfk-song-btn" id="songBtn">
                            <i class="fa fa-play" id="songIcon"></i>
                            <span>&nbsp;BFK Warzone Song</span>
                        </div>
                    </div>

                </div>
            </section>

            <!-- Road Map -->
            <section class="upcoming-games-area pt-120 pb-80 container" id="road-map-section">

                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-8">
                        <div class="section-title title-style-two text-center mb-60">
                            <h2 style="color: black;"><span>V2 ROADMAP</span></h2>
                        </div>
                    </div>
                </div>
                
                <div class="road-map">

                    <div class="road-map__top">

                        <!-- Phase 1 -->
                        <div class="road-map__step">
                            <span class="road-map__step__title">Phase 1</span>
                            <ul class="road-map__step__list">
                                <li>Contract Creation and Testnet</li>
                                <li>Website Creation</li>
                                <li>Socials Creation</li>
                                <li>Whitelist Contest</li>
                                <li>Audit Obtained</li>
                                <li>Memes and Stickers</li>
                                <li>Promotional/Logo Reveal Videos</li>
                                <li>Battle Fort Knox Dashboard</li>
                                <li>Icon updates - Trustwallet, BSCscan, Dextools</li>
                                <li>PCS Listing</li>
                                <li>BFK Minigame</li>
                                <li>Animated NFT Sample</li>
                            </ul>
                        </div>

                        <!-- Filler -->
                        <div class="road-map__step--filler"></div>

                        <!-- Phase 3 -->
                        <div class="road-map__step">
                            <span class="road-map__step__title">Phase 3</span>
                            <ul class="road-map__step__list">
                                <li>Coinmarketcap Listing</li>
                                <li>Listing on Major CEX</li>
                                <li>25k+ Holders</li>
                                <li>Big “Public Space” Marketing</li>
                                <li>Big Giveaway @ 100M MC - limited edition playable NFT</li>
                                <li>NFT / NFT Marketplace Creation</li>
                                <li>Battle Fort Knox merchandise and prizes!</li>
                                <li>Battle Fort Knox Staking</li>
                                <li>BFK Demo/Trailer Release</li>
                                <li>Entire NFT Drop Animated Release</li>
                            </ul>
                        </div>

                        <!-- Filler -->
                        <div class="road-map__step--filler"></div>

                    </div>
                    <div class="road-map__line">
                        <div class="road-map__line__step__wrapper">
                            <div class="road-map__line__step">
                                <i class="fas fa-check"></i>
                            </div>
                        </div>
                        <div class="road-map__line__step__wrapper">
                            <div class="road-map__line__step">
                                <!-- <i class="fas fa-check"></i> -->
                            </div>
                        </div>
                        <div class="road-map__line__step__wrapper">
                            <div class="road-map__line__step">
                                <!-- <i class="fas fa-check"></i> -->
                            </div>
                        </div>
                        <div class="road-map__line__step__wrapper">
                            <div class="road-map__line__step">
                                <!-- <i class="fas fa-check"></i> -->
                            </div>
                        </div>
                        <div class="road-map__line__step__wrapper--no-flex">
                            <div class="road-map__line__step">
                                <!-- <i class="fas fa-check"></i> -->
                                <img class="rocket-icon" src="img/icon/rocket-icon.png" />
                            </div>
                        </div>

                        <!-- Finish -->
                        <!-- <div class="road-map__line__step__wrapper--no-flex">
                            <div class="road-map__finish">Finish</div>
                        </div> -->
                        
                    </div>
                    <div class="road-map__bottom">

                        <!-- Filler -->
                        <div class="road-map__step--filler"></div>

                        <!-- Phase 2 -->
                        <div class="road-map__step">
                            <span class="road-map__step__title">Phase 2</span>
                            <ul class="road-map__step__list">
                                <li>Listing on top 15 Cointrackers</li>
                                <li>Partnership Explorations</li>
                                <li>Listing on CoinGecko</li>
                                <li>5000+ Holders</li>
                                <li>Expand Alternate Language Communities</li>
                                <li>Trending Strategies Implemented</li>
                                <li>First Vote to Donate to Charities</li>
                                <li>Weekly AMAs With Community - On Demand</li>
                                <li>Weekly Competitions</li>
                                <li>Impressive Manual Buyback</li>
                                <li>Influencer Partnerships</li>
                                <li>Big Marketing Push (i.e BTOK Ads)</li>
                                <li>First NFT Character Drop (static)</li>
                            </ul>
                        </div>

                        <!-- Filler -->
                        <div class="road-map__step--filler"></div>

                        <!-- Phase 4 -->
                        <div class="road-map__step">
                            <span class="road-map__step__title">Phase 4</span>
                            <ul class="road-map__step__list">
                                <li>NFT Interactive Battle Game 1V1</li>
                                <li>First Multiplayer Event</li>
                                <li>Android, iOS, and Windows Release</li>
                                <li>Market Cap Stable Above 100 million</li>
                                <li>50k+ Holders</li>
                                <li>Create Your Own NFT via Marketplace</li>
                                <li>More to come stay tuned!</li>
                            </ul>
                        </div>

                    </div>

                </div>

            </section>

            <!-- About & FAQ -->
            <section class="upcoming-games-area upcoming-games-bg pt-120 pb-80">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-6 col-lg-8">
                            <div class="section-title title-style-two text-center mb-60">
                                <!-- <span>what we give players</span> -->
                                <h2 style="color: black;">About & <span>FAQ</span></h2>
                            </div>
                        </div>
                    </div>
                    <div class="row faq-row">

                        <?php foreach($FAQ as $k => $v) { ?>
                            <div class="col-lg-3 col-md-6">
                                <div class="upcoming-game-item mb-40 clickable" onclick="openAlertPopup(`<?php echo $v[1]; ?>`)">
                                    <div class="upcoming-game-head">
                                        <div class="uc-game-head-title">
                                            <h4><?php echo $v[0]; ?></h4>
                                        </div>
                                    </div>
                                    <p>
                                        <?php echo substr($v[1], 0, 250); ?>
                                        <?php
                                            if(strlen($v[1]) > 250) {
                                                echo '...<span class="read-more-btn">read more</span>';
                                            }
                                        ?>
                                    </p>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
            </section>

        </main>
        <!-- main-area-end -->

        <!-- FOOTER -->
        <?php include '_footer.php'; ?>

		<!-- SCRIPTS -->
        <?php include '_scripts.php'; ?>

        <!-- Song Player -->
        <script>
            const bfkSong = document.querySelector('#bfkSong');
            const songBtn = document.querySelector('#songBtn');
            const songIcon = document.querySelector('#songIcon');

            songBtn.addEventListener('click', function() {
                if(bfkSong.paused) {
                    bfkSong.play();
                    songIcon.classList.remove('fa-play');
                    songIcon.classList.add('fa-pause');
                }
                else {
                    bfkSong.pause();
                    songIcon.classList.remove('fa-pause');
                    songIcon.classList.add('fa-play');
                }
            })
        </script>

    </body>
</html>
